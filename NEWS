Anjuta 0.1.7:   (21st August 2001)              -- Stephane Demurget

   => Release of Anjuta-0.1.7 (stable) - bug fixes release

Fixed: Two nasty bugs preventing to use the wizards (macros dir copy and
       src/source.c).
Fixed: Interface clean up.
Updated: HACKING, doc, and others
Updated: french, spanish, turk and japan translations fully up-to-date.

--------------------------------------------------------------------

Anjuta 0.1.6:	(31st July 2001)		-- Nabakumar. 

   => Release of Anjuta-0.1.6 (stable).

New: Class browser and file browser added in the project manager.
New: GDP standardized docbook documentations.
New: Anjuta faqs documentation.
New: New gnomish icon set and splashscreen.
New: Context sensitive help and gnome api browser
     (need Devhelp and the dev books to be installed).

Added: Preferences for setting caret and selection colors.
Fix for gdb version 5.
Closes all files when project is closed.
And many big fixes and minor feature additions.
Translations updated.

Note : the major part of the new project manager has been written but
it needs time to be integrated and tested. That's why it's not present in
this release. 

--------------------------------------------------------------------

Anjuta 0.1.4:	(25nd Mar 2001)		-- Nabakumar. 

   => Release of Anjuta-0.1.4 (stable).

Calltips provided for Gnome function prototypes (and standard libraries functions).   
Fixed project dir creation bug.
Fixed double accelerators bug.
Fixed source generation bug.
Fixed pixmap creation with NULL window.
i18n bug fix.
Debugger now takes program args.
Japanese translation.
Other minor  bug fixes.

--------------------------------------------------------------------

Anjuta 0.1.3:	(4th Mar 2001)		-- Nabakumar. 

   => Release of Third  version (Alpha)  with lots of bugs.
   
New Features in Anjuta 0.1.3:
-----------------------------

   	=> Anjuta now integrates Scintilla editing component:
	----------------------------------------------------
		* Automatic syntax hilighting (using lexers).
		* Code folding/hiding .
		* Linenumbers/markers display.
		* Text zooming.
		* Text autocompletion
		* Automatic indentation and indentation guides
		* ... and much more.

	=> Dynamic Project Management:
	-----------------------------
		* Anjuta no longer uses the template based project management.
		* Provides project configuration.
		* Added two more modules in project management:
			(Help module and Translation module).
		* Added BONOBO support and BONOBO application wizard.
		
	=> Java style Properties management:
	-----------------------------------
		Now uses Java style properties management. Which makes
		it highly configurable, both from distributor's and user's side.
	
	=> Supports for Other languages added:
	-------------------------------------
		* Java, Perl, Python, LaTeX, 
		... and others can be user configurable.
		
	=> On-Board command editor:
	--------------------------
		Facilitates customization of commands used
		for compiling, building and execution.
	
	=> Provides Hilite style editor:
	-------------------------------
		Used for customizing Syntax hiliting style.
	
	=> More intelligent messaging system:
	-----------------------------------
		* Now error messages (or messages in general)
		can be browsed using go to Prev/Next message,
		goto start/end of block and goto matching braces.
	
	=> Bookmarks management:
	-----------------------
		Anjuta has now supported Bookmarks
		management.
	
	=> Debugger:
	-----------
		* Now breakpoints can be toggled Onboard.
		* Added Run to cursor.
		* Fixed some earlier bugs.
	
	=> Added Two more toolbars:
	--------------------------
		* Browser toolbar for browsing messages,
		bookmarks and code blocks.
		* Format toolbar for easy formatting
		of code statements.
		
	=> Application GUI:
	------------------
		* Smarter and more informative status bar.
		* Improved memory to remember Application
		GUI configuration between sessions.

Thing that I forgot to change:
------------------------------
	=> Anjuta Online manual. It's still the old one.
	so, new users may find it a bit annoying to learn
	the controls, especially those who are young
	in programming.

--------------------------------------------------------------------
Anjuta 0.1.2-beta_2:	(24th Feb 2001)		-- Nabakumar. 

   => Release of 0.1.2 beta2  version  with lesser bugs.

--------------------------------------------------------------------
Anjuta 0.1.2:	(03rd Nov 2000)		-- Nabakumar. 

   => Release of second version (Beta) with lesser bugs.

   => Minor fixes in Doc files.
   => Fixed to ignore non-existent dir for blank prj.
   => More checking in project creation, especially to confirm
	that the prj file already does not exist.
   => Now Menu item "Format/Detach current document" is inactive
        when there is no document opened.
   => Improved Library settings GUI.
   => Can now enable/disable added libraries.
   => Stock libs provided for ease of entry.

--------------------------------------------------------------------
Anjuta 0.1.1: (01rd Nov 2000)	-- Nabakumar. 

  => Release of first version (Beta) with lots of bugs.
	
--------------------------------------------------------------------

