/*
 * anjuta-app.c Copyright (C) 2000  Kh. Naba Kumar Singh
 * 
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free 
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc., 59 
 * Temple Place, Suite 330, Boston, MA  02111-1307  USA 
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <sys/stat.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>
#include <sys/wait.h>

#include <gnome.h>
#include <liboaf/liboaf.h>
#include <bonobo.h>
#include <bonobo/bonobo-ui-util.h>
#include <bonobo/bonobo-listener.h>
#include <gal/e-paned/e-paned.h>
#include <gal/e-paned/e-vpaned.h>
#include <gal/e-paned/e-hpaned.h>

#include <libanjuta/libanjuta.h>

#include "about.h"
#include "anjuta-app.h"
#include "dnd.h"

#define ANJUTA_XML_FILE "anjuta.xml"

struct _AnjutaAppPriv 
{
	GtkWidget *hpane;
	GtkWidget *vpane;
	
	GtkWidget *content;
	GtkWidget *the_client;
	
	GtkWidget *hpaned_client;
	
	GtkWidget *sidebar_win_container;
	GtkWidget *sidebar_client;
	GtkWidget *sidebar_notebook;

	GtkWidget *bottombar_win_container;
	GtkWidget *bottombar_client;
	
	GtkWidget *document_manager;
};

/* Prototypes */
static void anjuta_app_class_init (AnjutaAppClass *class);
static void anjuta_app_init (AnjutaApp *tool);
static void anjuta_app_drag_recv(gchar* filename, gpointer data);
static void anjuta_app_destroy( GtkWidget *widget, gpointer data );

static void anjuta_app_about (GtkWidget* widget, gpointer data);

guint
anjuta_app_get_type()
{
	static guint window_type = 0;

	if (!window_type) {
           GtkTypeInfo window_info = {
               "AnjutaApp",
               sizeof (AnjutaApp),
               sizeof (AnjutaAppClass),
               (GtkClassInitFunc) anjuta_app_class_init,
               (GtkObjectInitFunc) anjuta_app_init,
               (GtkArgSetFunc) NULL,
               (GtkArgGetFunc) NULL
             };
             window_type = gtk_type_unique (bonobo_window_get_type (),
					    &window_info);
           }
		
         return window_type;
}

static void
anjuta_app_class_init( AnjutaAppClass *class )
{
	GtkObjectClass *object_class;
	
	object_class = (GtkObjectClass*) class;
}

/* Menu verbs */
static BonoboUIVerb verbs [] = {
	BONOBO_UI_UNSAFE_VERB ("FileExit", anjuta_app_destroy),
	BONOBO_UI_UNSAFE_VERB ("HelpAbout", anjuta_app_about),
	BONOBO_UI_VERB_END
};

static void
anjuta_app_init(AnjutaApp *window)
{
	BonoboUIContainer *ui_container;
	
	GtkWidget *vbox1;
	GtkWidget *hbox3;
	GtkWidget *button1;
	GtkWidget *frame1;
	GtkWidget *vbox3;
	GtkWidget *pixmap;
	GtkWidget *hseparator1;
	GtkWidget *hseparator2;
	GtkWidget *hseparator3;
	GtkWidget *button3;
	GtkWidget *hbox2;
	GtkWidget *vbox2;
	GtkWidget *button4;
	GtkWidget *frame2;
	GtkWidget *hbox4;
	GtkWidget *vseparator3;
	GtkWidget *vseparator2;
	GtkWidget *vseparator1;
	GtkWidget *button2;
	GtkWidget *label1;
	
	GtkTooltips *tooltips;

	tooltips = gtk_tooltips_new ();


	window->priv = g_new0 (AnjutaAppPriv, 1);

	/* Initialize Application */
	bonobo_window_construct (BONOBO_WINDOW (window), "anjuta", "anjuta");
	
	/* Set some default window's properties */
	gtk_widget_set_uposition (GTK_WIDGET(window), 0, 0);
	gtk_widget_set_usize (GTK_WIDGET(window), 500, 116);
	gtk_window_set_default_size (GTK_WINDOW (window), 600, 400);
	gtk_window_set_policy (GTK_WINDOW (window), FALSE, TRUE, FALSE);
	gtk_window_set_wmclass (GTK_WINDOW (window), "mainide", "Anjuta");
	gtk_window_set_policy(GTK_WINDOW(window), TRUE, TRUE, TRUE);

	window->priv->content = gtk_event_box_new();
	gtk_widget_show(window->priv->content);
	bonobo_window_set_contents (BONOBO_WINDOW (window), window->priv->content);

	/* Too many toolbar items, disabling the text */
	gnome_preferences_set_toolbar_labels(FALSE);

	/* Menus and toolbars */
	window->uic = bonobo_ui_component_new_default ();

	ui_container = bonobo_ui_container_new ();
	bonobo_ui_container_set_win (ui_container, BONOBO_WINDOW (window));
	
	bonobo_ui_component_set_container (
		window->uic, bonobo_object_corba_objref (BONOBO_OBJECT (ui_container)));

	bonobo_ui_component_add_verb_list_with_data (window->uic, verbs, window);

	bonobo_ui_util_set_ui (window->uic, PACKAGE_PREFIX_DIR, ANJUTA_XML_FILE, "anjuta");
	
	window->ui_container = ui_container;

	window->priv->vpane = e_vpaned_new ();
	gtk_widget_show(window->priv->vpane);
	gtk_container_add (GTK_CONTAINER (window->priv->content), window->priv->vpane);
	
	window->priv->hpane = e_hpaned_new();
	gtk_widget_show(window->priv->hpane);
	e_paned_set_position (E_PANED (window->priv->hpane), 200);
	e_paned_add1 (E_PANED (window->priv->vpane), window->priv->hpane);
	
	bonobo_ui_engine_config_set_path (bonobo_window_get_ui_engine (BONOBO_WINDOW (window)), "/anjuta/UIConf/kvps");

	gtk_widget_queue_draw( GTK_WIDGET(window) );
	gtk_widget_queue_resize( GTK_WIDGET(window) );
	
	/* Add file drag and drop support */
	dnd_drop_init(GTK_WIDGET(window), anjuta_app_drag_recv, window,
		"text/plain", "text/html", "text/source", NULL);

	vbox1 = gtk_vbox_new (FALSE, 0);
	gtk_widget_show (vbox1);
	e_paned_set_position (E_PANED (window->priv->vpane), 400);
	e_paned_add1 (E_PANED (window->priv->hpane), vbox1);

	hbox3 = gtk_hbox_new (FALSE, 0);
	gtk_widget_show (hbox3);
	gtk_box_pack_start (GTK_BOX (vbox1), hbox3, FALSE, FALSE, 0);

	button1 = gtk_button_new ();
	gtk_widget_show (button1);
	gtk_box_pack_start (GTK_BOX (hbox3), button1, FALSE, TRUE, 0);
	gtk_container_set_border_width (GTK_CONTAINER (button1), 1);
	gtk_tooltips_set_tip (tooltips, button1,
			      _("Click here to undock this window"), NULL);

	pixmap =
		anjuta_res_get_pixmap_widget (GTK_WIDGET(window), "handle_undock.xpm", FALSE);
	gtk_widget_show (pixmap);
	gtk_container_add (GTK_CONTAINER (button1), pixmap);

	frame1 = gtk_frame_new (NULL);
	gtk_widget_show (frame1);
	gtk_box_pack_start (GTK_BOX (hbox3), frame1, TRUE, TRUE, 1);
	gtk_container_set_border_width (GTK_CONTAINER (frame1), 1);
	gtk_frame_set_shadow_type (GTK_FRAME (frame1), GTK_SHADOW_NONE);

	vbox3 = gtk_vbox_new (TRUE, 1);
	gtk_widget_show (vbox3);
	gtk_container_add (GTK_CONTAINER (frame1), vbox3);

	hseparator1 = gtk_hseparator_new ();
	gtk_widget_show (hseparator1);
	gtk_box_pack_start (GTK_BOX (vbox3), hseparator1, TRUE, TRUE, 0);

	hseparator2 = gtk_hseparator_new ();
	gtk_widget_show (hseparator2);
	gtk_box_pack_start (GTK_BOX (vbox3), hseparator2, TRUE, TRUE, 0);

	hseparator3 = gtk_hseparator_new ();
	gtk_widget_show (hseparator3);
	gtk_box_pack_start (GTK_BOX (vbox3), hseparator3, TRUE, TRUE, 0);

	button3 = gtk_button_new ();
	gtk_widget_show (button3);
	gtk_box_pack_start (GTK_BOX (hbox3), button3, FALSE, TRUE, 0);
	gtk_container_set_border_width (GTK_CONTAINER (button3), 1);
	gtk_tooltips_set_tip (tooltips, button3,
			      _("Click here to hide this window"), NULL);


	pixmap = anjuta_res_get_pixmap_widget (GTK_WIDGET(window), "handle_hide.xpm", FALSE);
	gtk_widget_show (pixmap);
	gtk_container_add (GTK_CONTAINER (button3), pixmap);

	hbox2 = gtk_hbox_new (FALSE, 0);
	gtk_widget_ref (hbox2);
	gtk_widget_show (hbox2);
	e_paned_add2 (E_PANED (window->priv->vpane), hbox2);

	vbox2 = gtk_vbox_new (FALSE, 0);
	gtk_widget_show (vbox2);
	gtk_box_pack_start (GTK_BOX (hbox2), vbox2, FALSE, FALSE, 0);

	button4 = gtk_button_new ();
	gtk_widget_show (button4);

	gtk_box_pack_start (GTK_BOX (vbox2), button4, FALSE, TRUE, 0);
	gtk_container_set_border_width (GTK_CONTAINER (button4), 1);
	gtk_tooltips_set_tip (tooltips, button4,
			      _("Click here to hide this window"), NULL);

	pixmap = anjuta_res_get_pixmap_widget (GTK_WIDGET(window), "handle_hide.xpm", FALSE);
	gtk_widget_show (pixmap);
	gtk_container_add (GTK_CONTAINER (button4), pixmap);

	window->priv->sidebar_notebook = gtk_notebook_new ();
	gtk_widget_show (window->priv->sidebar_notebook);
	gtk_container_add (GTK_CONTAINER (vbox1), window->priv->sidebar_notebook);
	gtk_notebook_set_show_border (GTK_NOTEBOOK (window->priv->sidebar_notebook), TRUE);
	gtk_notebook_set_scrollable (GTK_NOTEBOOK (window->priv->sidebar_notebook), TRUE);
	
	frame2 = gtk_frame_new (NULL);
	gtk_widget_show (frame2);
	gtk_box_pack_start (GTK_BOX (vbox2), frame2, TRUE, TRUE, 0);
	gtk_container_set_border_width (GTK_CONTAINER (frame2), 1);
	gtk_frame_set_shadow_type (GTK_FRAME (frame2), GTK_SHADOW_NONE);

	hbox4 = gtk_hbox_new (TRUE, 1);
	gtk_widget_show (hbox4);
	gtk_container_add (GTK_CONTAINER (frame2), hbox4);

	vseparator3 = gtk_vseparator_new ();
	gtk_widget_show (vseparator3);
	gtk_box_pack_start (GTK_BOX (hbox4), vseparator3, TRUE, TRUE, 0);

	vseparator2 = gtk_vseparator_new ();
	gtk_widget_show (vseparator2);
	gtk_box_pack_start (GTK_BOX (hbox4), vseparator2, TRUE, TRUE, 0);

	vseparator1 = gtk_vseparator_new ();
	gtk_widget_show (vseparator1);
	gtk_box_pack_start (GTK_BOX (hbox4), vseparator1, TRUE, TRUE, 0);

	button2 = gtk_button_new ();
	gtk_widget_show (button2);
	gtk_box_pack_start (GTK_BOX (vbox2), button2, FALSE, TRUE, 0);
	gtk_container_set_border_width (GTK_CONTAINER (button2), 1);
	gtk_tooltips_set_tip (tooltips, button2,
			      _("Click here to undock this window"), NULL);

	pixmap =
		anjuta_res_get_pixmap_widget (GTK_WIDGET(window), "handle_undock.xpm", FALSE);
	gtk_widget_show (pixmap);
	gtk_container_add (GTK_CONTAINER (button2), pixmap);
	
	/* This is only a test code. -- START */
	label1 = gtk_text_new (NULL, NULL);
	gtk_text_insert (GTK_TEXT(label1), NULL, NULL, NULL,
		"This is where the document manager will sit. :-)", -1);
	gtk_widget_show (label1);
	e_paned_add2(E_PANED (window->priv->hpane), label1);
	
	label1 = gtk_text_new (NULL, NULL);
	gtk_text_insert (GTK_TEXT(label1), NULL, NULL, NULL,
		"This is where the project manager will sit. :-)", -1);
	gtk_widget_show (label1);
	gtk_notebook_append_page (GTK_NOTEBOOK(window->priv->sidebar_notebook),
		label1, gtk_label_new ("Prj"));
	/* This is only a test code. -- END */

/*
	gtk_signal_connect (GTK_OBJECT (window), "focus_in_event",
			    GTK_SIGNAL_FUNC (on_anjuta_window_focus_in_event),
			    window);

	gtk_signal_connect (GTK_OBJECT (window), "delete_event",
			    GTK_SIGNAL_FUNC (on_anjuta_window_delete), window);
	
*/	gtk_signal_connect (GTK_OBJECT (window), "destroy",
			    GTK_SIGNAL_FUNC (anjuta_app_destroy), window);
	
/*	gtk_signal_connect (GTK_OBJECT (window), "switch_page",
			    GTK_SIGNAL_FUNC (on_anjuta_notebook_switch_page),
			    window);
	
	gtk_signal_connect (GTK_OBJECT (button1), "clicked",
			    GTK_SIGNAL_FUNC
			    (on_sidebar_undock_button_clicked), window);
	
	gtk_signal_connect (GTK_OBJECT (button3), "clicked",
			    GTK_SIGNAL_FUNC (on_sidebar_list_hide_button_clicked),
			    window);
	
	gtk_signal_connect (GTK_OBJECT (button4), "clicked",
			    GTK_SIGNAL_FUNC (on_bottombar_hide_button_clicked),
			    window);
	
	gtk_signal_connect (GTK_OBJECT (button2), "clicked",
			    GTK_SIGNAL_FUNC
			    (on_bottombar_undock_button_clicked), window);
*/
}

GtkWidget *
anjuta_app_new (void)
{
	AnjutaApp *window;
	
	window = ANJUTA_APP (gtk_type_new (anjuta_app_get_type ()));

	/* create our menus */	

	return GTK_WIDGET(window);
}

static void
anjuta_app_destroy(GtkWidget *widget, gpointer data)
{
	gtk_main_quit();
}

static void
anjuta_app_about (GtkWidget* widget, gpointer data)
{
	GtkWidget* about;
	about = create_about_gui();
	gtk_widget_show (about);
}

static void
anjuta_app_drag_recv(gchar* filename, gpointer data)
{
	/*anjuta_goto_file_line (filename, 0);*/
}

void 
anjuta_app_add_widget (AnjutaApp *app, GtkWidget *w, 
		      const gchar *name)
{
	GtkWidget *notebook = app->priv->sidebar_notebook;
	GtkWidget *label = gtk_label_new (name);

	gtk_notebook_append_page (GTK_NOTEBOOK (notebook), w, label);
	
	if (g_list_length (GTK_NOTEBOOK (notebook)->children) > 1) {
		gtk_notebook_set_show_tabs (GTK_NOTEBOOK (notebook), TRUE);
	}
	gtk_object_set_data (GTK_OBJECT (w), "AnjutaApp::notebook", notebook);

	gtk_widget_queue_resize (GTK_WIDGET (app));
}

void
anjuta_app_remove_widget (AnjutaApp *app, GtkWidget *w)
{
	gint page_num;
	GtkWidget *notebook;

	notebook = 
		gtk_object_get_data (GTK_OBJECT (w), "AnjutaApp::notebook");
	
	page_num = gtk_notebook_page_num (GTK_NOTEBOOK (notebook), w);
	gtk_notebook_remove_page (GTK_NOTEBOOK (notebook), page_num);
}

#if 0
/*
 * Function: anjuta_app_clear_statusbar()
 * Desc: Clears the Statusbar of a Window
 */

void
anjuta_app_clear_statusbar( AnjutaApp *window )
{
	gnome_appbar_pop( GNOME_APPBAR(window->statusbar) );
}

static void anjuta_child_terminated (int t);
static void anjuta_clear_windows_menu (void);
static void anjuta_fill_windows_menu (void);
static void anjuta_show_text_editor (TextEditor * te);
static void on_anjuta_window_selected (GtkMenuItem * menuitem,
				       gpointer user_data);
void
anjuta_new ()
{
	app = (AnjutaApp *) g_malloc (sizeof (AnjutaApp));
	if (app)
	{
		/* Must declare static, because it will be used forever */
		static FileSelData fsd1 = { N_("Open File"), NULL,
			on_open_filesel_ok_clicked,
			on_open_filesel_cancel_clicked, NULL
		};

		/* Must declare static, because it will be used forever */
		static FileSelData fsd2 = { N_("Save File As"), NULL,
			on_save_as_filesel_ok_clicked,
			on_save_as_filesel_cancel_clicked, NULL
		};
		app->shutdown_in_progress = FALSE;
		app->registered_windows = NULL;
		app->registered_child_processes = NULL;
		app->registered_child_processes_cb = NULL;
		app->registered_child_processes_cb_data = NULL;
		app->text_editor_list = NULL;
		app->current_text_editor = NULL;
		app->cur_job = NULL;
		app->recent_files = NULL;
		app->recent_projects = NULL;
		app->win_width = gdk_screen_width () - 10;
		app->win_height = gdk_screen_height () - 25;
		app->vpaned_height = app->win_height / 2 + 7;
		app->hpaned_width = app->win_width / 4;
		app->in_progress = FALSE;
		app->auto_gtk_update = TRUE;
		app->busy_count = 0;
		app->execution_dir = NULL;
		app->first_time_expose = TRUE;

		create_anjuta_gui (app);
		
		app->dirs = anjuta_dirs_new ();
		app->fileselection = create_fileselection_gui (&fsd1);
		
		/* Set to the current dir */
		fileselection_set_dir (app->fileselection, (gchar *)get_current_dir_name());
		
		app->save_as_fileselection = create_fileselection_gui (&fsd2);
		app->find_replace = find_replace_new ();
		app->find_in_files = find_in_files_new ();
		app->preferences = preferences_new ();
		app->compiler_options = compiler_options_new (app->preferences->props);
		app->src_paths = src_paths_new ();
		app->messages = messages_new ();
		app->project_dbase = project_dbase_new (app->preferences->props);
		app->configurer = configurer_new (app->project_dbase->props);
		app->executer = executer_new (app->project_dbase->props);
		app->command_editor = command_editor_new (app->preferences->props_global,
					app->preferences->props_local, app->project_dbase->props);
		app->tags_manager = tags_manager_new ();
		app->help_system = anjuta_help_new();

		app->widgets.the_client = app->widgets.vpaned;
		app->widgets.hpaned_client = app->widgets.hpaned;
		gtk_container_add (GTK_CONTAINER (app->widgets.hpaned),
				   app->widgets.notebook);
		gtk_notebook_popup_enable (GTK_NOTEBOOK
					   (app->widgets.notebook));
		gtk_box_pack_start (GTK_BOX (app->widgets.mesg_win_container),
				    app->messages->client, TRUE, TRUE, 0);
		gtk_box_pack_start (GTK_BOX
				    (app->widgets.
				     project_dbase_win_container),
				    app->project_dbase->widgets.client, TRUE,
				    TRUE, 0);
		project_dbase_hide (app->project_dbase);
		messages_hide (app->messages);

		gtk_paned_set_position (GTK_PANED (app->widgets.vpaned),
					app->vpaned_height);
		gtk_paned_set_position (GTK_PANED (app->widgets.hpaned),
					app->hpaned_width);
		anjuta_update_title ();
		launcher_init ();
		debugger_init ();

		anjuta_load_yourself (app->preferences->props);
		gtk_widget_set_uposition (app->widgets.window, app->win_pos_x,
					  app->win_pos_y);
		gtk_window_set_default_size (GTK_WINDOW (app->widgets.window),
					     app->win_width, app->win_height);
		main_toolbar_update ();
		extended_toolbar_update ();
		debug_toolbar_update ();
		format_toolbar_update ();
		browser_toolbar_update ();
		anjuta_apply_preferences();
	}
	else
	{
		app = NULL;
		g_error (_("Cannot create application...exiting\n"));
	}
}

void
anjuta_session_restore (GnomeClient* client)
{
	anjuta_load_cmdline_files();
}

TextEditor *
anjuta_append_text_editor (gchar * filename)
{
	GtkWidget *label, *eventbox;
	TextEditor *te, *cur_page;
	gchar *buff;

	cur_page = anjuta_get_current_text_editor ();
	te = text_editor_new (filename, cur_page, app->preferences);
	if (te == NULL) return NULL;
	gtk_signal_disconnect_by_func (GTK_OBJECT (app->widgets.notebook),
				       GTK_SIGNAL_FUNC
				       (on_anjuta_notebook_switch_page),
				       NULL);
	anjuta_set_current_text_editor (te);
	anjuta_clear_windows_menu ();
	app->text_editor_list = g_list_append (app->text_editor_list, te);
	breakpoints_dbase_set_all_in_editor (debugger.breakpoints_dbase, te);
	switch (te->mode)
	{
	case TEXT_EDITOR_PAGED:
		label = gtk_label_new (te->filename);
		gtk_widget_show (label);
		eventbox = gtk_event_box_new ();
		gtk_widget_show (eventbox);
		gtk_notebook_prepend_page (GTK_NOTEBOOK
					   (app->widgets.notebook), eventbox,
					   label);
		gtk_container_add (GTK_CONTAINER (eventbox),
				   te->widgets.client);
	
		/* For your kind info, this same data is also set in */
		/* the function on_text_editor_dock_activated() */
		gtk_object_set_data (GTK_OBJECT (eventbox), "TextEditor", te);

		if (te->full_filename)
			buff =
				g_strdup_printf (_("Anjuta: %s"),
						 te->full_filename);
		else
			buff =
				g_strdup_printf (_("Anjuta: %s"),
						 te->filename);
		gtk_window_set_title (GTK_WINDOW (app->widgets.window), buff);
		g_free (buff);
		gtk_notebook_set_page (GTK_NOTEBOOK (app->widgets.notebook),
		       0);
		break;

	case TEXT_EDITOR_WINDOWED:
		gtk_widget_show (te->widgets.window);
		break;
	}
	anjuta_fill_windows_menu ();
	gtk_signal_connect (GTK_OBJECT (app->widgets.notebook), "switch_page",
			    GTK_SIGNAL_FUNC (on_anjuta_notebook_switch_page),
			    NULL);
	anjuta_set_current_text_editor(te);
	anjuta_grab_text_focus ();
	return te;
}

static void
on_anjuta_window_selected (GtkMenuItem * menuitem, gpointer user_data)
{
	anjuta_show_text_editor ((TextEditor *) user_data);
}

void
anjuta_show ()
{
	PropsID pr;
	gchar* key;

	pr = app->preferences->props;

	gtk_widget_show (app->widgets.window);

	/* Editor stuffs */
	gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
					(app->widgets.menubar.view.
					 editor_linenos), prop_get_int (pr,
									"margin.linenumber.visible",
									0));
	gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
					(app->widgets.menubar.view.
					 editor_markers), prop_get_int (pr,
									"margin.marker.visible",
									0));
	gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
					(app->widgets.menubar.view.
					 editor_folds), prop_get_int (pr,
								      "margin.fold.visible",
								      0));
	gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
					(app->widgets.menubar.view.
					 editor_indentguides),
					prop_get_int (pr,
						      "view.indentation.guides",
						      0));
	gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
					(app->widgets.menubar.view.
					 editor_whitespaces),
					prop_get_int (pr, "view.whitespace",
						      0));
	gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
					(app->widgets.menubar.view.
					 editor_eolchars), prop_get_int (pr,
									 "view.eol",
									 0));


	/* Hide all toolbars, since corrosponding toggle menu items */
	/* (in the view submenu) are all initially off */
	/* We do not resize or set the change in props */
	anjuta_toolbar_set_view (ANJUTA_MAIN_TOOLBAR, FALSE, FALSE, FALSE);
	anjuta_toolbar_set_view (ANJUTA_EXTENDED_TOOLBAR, FALSE, FALSE, FALSE);
	anjuta_toolbar_set_view (ANJUTA_TAGS_TOOLBAR, FALSE, FALSE, FALSE);
	anjuta_toolbar_set_view (ANJUTA_DEBUG_TOOLBAR, FALSE, FALSE, FALSE);
	anjuta_toolbar_set_view (ANJUTA_BROWSER_TOOLBAR, FALSE, FALSE, FALSE);
	anjuta_toolbar_set_view (ANJUTA_FORMAT_TOOLBAR, FALSE, FALSE, FALSE);

	/* Now we are synced with the menu items */
	/* Show/Hide the necessary toolbars */
	key = g_strconcat (ANJUTA_MAIN_TOOLBAR, ".visible", NULL);
	gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
			(app->widgets.menubar. view.main_toolbar),
			prop_get_int (pr, key, 1));
	g_free (key);

	key = g_strconcat (ANJUTA_EXTENDED_TOOLBAR, ".visible", NULL);
	gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
			(app->widgets.menubar. view.extended_toolbar),
			prop_get_int (pr, key, 1));
	g_free (key);

	key = g_strconcat (ANJUTA_TAGS_TOOLBAR, ".visible", NULL);
	gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
			(app->widgets.menubar. view.tags_toolbar),
			prop_get_int (pr, key, 1));
	g_free (key);

	key = g_strconcat (ANJUTA_DEBUG_TOOLBAR, ".visible", NULL);
	gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
			(app->widgets.menubar. view.debug_toolbar),
			prop_get_int (pr, key, 1));
	g_free (key);

	key = g_strconcat (ANJUTA_BROWSER_TOOLBAR, ".visible", NULL);
	gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
			(app->widgets.menubar. view.browser_toolbar),
			prop_get_int (pr, key, 1));
	g_free (key);

	key = g_strconcat (ANJUTA_FORMAT_TOOLBAR, ".visible", NULL);
	gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
			(app->widgets.menubar. view.format_toolbar),
			prop_get_int (pr, key, 1));
	g_free (key);
	
	update_gtk ();
	messages_append (app->messages,
			 _("CVS is not yet implemented. :-(\n"), MESSAGE_CVS);
	
	if (app->dirs->first_time)
	{
		gchar *file;
		app->dirs->first_time = FALSE;
		file = g_strconcat (app->dirs->data, "/welcome.txt", NULL);
		anjuta_info_show_file (file, 500, 435);
		g_free (file);
	}
}

void
anjuta_save_settings ()
{
	gchar *buffer;
	FILE *stream;

	if (!app)
		return;
	buffer =
		g_strconcat (app->dirs->settings, "/session.properties",
			     NULL);
	stream = fopen (buffer, "w");
	g_free (buffer);
	if (stream)
	{
		anjuta_save_yourself (stream);
		fclose (stream);
	}
}

gboolean anjuta_save_yourself (FILE * stream)
{
	GList *node;
	PropsID pr;
	gchar* key;

	pr = app->preferences->props;

	gdk_window_get_root_origin (app->widgets.window->window,
				    &app->win_pos_x, &app->win_pos_y);
	gdk_window_get_size (app->widgets.window->window, &app->win_width,
			     &app->win_height);
	fprintf (stream,
		 _("# * DO NOT EDIT OR RENAME THIS FILE ** Generated by Anjuta **\n"));
	fprintf (stream, "anjuta.version=%s\n", VERSION);
	fprintf (stream, "anjuta.win.pos.x=%d\n", app->win_pos_x);
	fprintf (stream, "anjuta.win.pos.y=%d\n", app->win_pos_y);
	fprintf (stream, "anjuta.win.width=%d\n", app->win_width);
	fprintf (stream, "anjuta.win.height=%d\n", app->win_height);
	fprintf (stream, "anjuta.vpaned.size=%d\n",
		 GTK_PANED (app->widgets.vpaned)->child1_size);
	fprintf (stream, "anjuta.hpaned.size=%d\n",
		 GTK_PANED (app->widgets.hpaned)->child1_size);

	key = g_strconcat (ANJUTA_MAIN_TOOLBAR, ".visible", NULL);
	fprintf (stream, "%s=%d\n", key, prop_get_int (app->preferences->props, key, 1));
	g_free (key);

	key = g_strconcat (ANJUTA_EXTENDED_TOOLBAR, ".visible", NULL);
	fprintf (stream, "%s=%d\n", key, prop_get_int (app->preferences->props, key, 1));
	g_free (key);

	key = g_strconcat (ANJUTA_FORMAT_TOOLBAR, ".visible", NULL);
	fprintf (stream, "%s=%d\n", key, prop_get_int (app->preferences->props, key, 1));
	g_free (key);

	key = g_strconcat (ANJUTA_DEBUG_TOOLBAR, ".visible", NULL);
	fprintf (stream, "%s=%d\n", key, prop_get_int (app->preferences->props, key, 1));
	g_free (key);

	key = g_strconcat (ANJUTA_TAGS_TOOLBAR, ".visible", NULL);
	fprintf (stream, "%s=%d\n", key, prop_get_int (app->preferences->props, key, 1));
	g_free (key);

	key = g_strconcat (ANJUTA_BROWSER_TOOLBAR, ".visible", NULL);
	fprintf (stream, "%s=%d\n", key, prop_get_int (app->preferences->props, key, 1));
	g_free (key);

	fprintf (stream, "view.eol=%d\n", prop_get_int (pr, "view.eol", 0));
	fprintf (stream, "view.indentation.guides=%d\n",
		 prop_get_int (pr, "view.indentation.guides", 0));
	fprintf (stream, "view.whitespace=%d\n",
		 prop_get_int (pr, "view.whitespace", 0));
	fprintf (stream, "view.indentation.whitespace=%d\n",
		 prop_get_int (pr, "view.indentation.whitespace", 0));
	fprintf (stream, "margin.linenumber.visible=%d\n",
		 prop_get_int (pr, "margin.linenumber.visible", 0));
	fprintf (stream, "margin.marker.visible=%d\n",
		 prop_get_int (pr, "margin.marker.visible", 0));
	fprintf (stream, "margin.fold.visible=%d\n",
		 prop_get_int (pr, "margin.fold.visible", 0));

	fprintf (stream, "anjuta.recent.files=");
	node = app->recent_files;
	while (node)
	{
		fprintf (stream, "\\\n%s ", (gchar *) node->data);
		node = g_list_next (node);
	}
	fprintf (stream, "\n\n");

	fprintf (stream, "anjuta.recent.projects=");
	node = app->recent_projects;
	while (node)
	{
		fprintf (stream, "\\\n%s ", (gchar *) node->data);
		node = g_list_next (node);
	}
	fprintf (stream, "\n\n");
	messages_save_yourself (app->messages, stream);
	project_dbase_save_yourself (app->project_dbase, stream);

	compiler_options_save_yourself (app->compiler_options, stream);
	if (app->project_dbase->project_is_open == FALSE)
		compiler_options_save (app->compiler_options, stream);

	command_editor_save_yourself (app->command_editor, stream);
	command_editor_save (app->command_editor, stream);

	src_paths_save_yourself (app->src_paths, stream);
	if (app->project_dbase->project_is_open == FALSE)
		src_paths_save (app->src_paths, stream);

	find_replace_save_yourself (app->find_replace, stream);
	debugger_save_yourself (stream);
	preferences_save_yourself (app->preferences, stream);
	return TRUE;
}

gboolean anjuta_load_yourself (PropsID pr)
{
	gint length;

	app->win_pos_x = prop_get_int (pr, "anjuta.win.pos.x", 10);
	app->win_pos_y = prop_get_int (pr, "anjuta.win.pos.y", 10);
	app->win_width =
		prop_get_int (pr, "anjuta.win.width",
			      gdk_screen_width () - 10);
	app->win_height =
		prop_get_int (pr, "anjuta.win.height",
			      gdk_screen_height () - 25);
	length =
		prop_get_int (pr, "anjuta.vpaned.size",
			      app->win_height / 2 + 7);
	gtk_paned_set_position (GTK_PANED (app->widgets.vpaned), length);
	length = prop_get_int (pr, "anjuta.hpaned.size", app->win_width / 4);
	gtk_paned_set_position (GTK_PANED (app->widgets.hpaned), length);

	glist_strings_free (app->recent_files);
	app->recent_files = glist_from_data (pr, "anjuta.recent.files");
	glist_strings_free (app->recent_projects);
	app->recent_projects = glist_from_data (pr, "anjuta.recent.projects");
	messages_load_yourself (app->messages, pr);
	project_dbase_load_yourself (app->project_dbase, pr);
	preferences_load_yourself (app->preferences, pr);
	compiler_options_load_yourself (app->compiler_options, pr);
	compiler_options_load (app->compiler_options, pr);
	src_paths_load_yourself (app->src_paths, pr);
	src_paths_load (app->src_paths, pr);
	find_replace_load_yourself (app->find_replace, pr);
	debugger_load_yourself (pr);
	return TRUE;
}

void
anjuta_update_title ()
{
	gchar *buff1, *buff2;
	GtkWidget *window;
	TextEditor *te;
	te = anjuta_get_current_text_editor ();
	if (te)
	{
		if (te->mode == TEXT_EDITOR_PAGED)
		{
			window = app->widgets.window;
			main_toolbar_update ();
			extended_toolbar_update ();
		}
		else
			{ window = te->widgets.window;}

		if (text_editor_is_saved (te))
		{
			if (te->full_filename)
			{
				buff1 =
					g_strdup_printf (_("Anjuta: %s(Saved)"),
						 te->full_filename);
			}
			else
			{
				buff1 = g_strdup("");
			}
			buff2 =
				g_strdup_printf (_("Anjuta: %s"),
						 te->filename);
		}
		else
		{
			if (te->full_filename)
			{
				buff1 =
					g_strdup_printf (_("Anjuta: %s(Unsaved)"),
							 te->full_filename);
			}
			else
			{
				buff1 = g_strdup("");
			}
			buff2 =
				g_strdup_printf (_("Anjuta: %s"),
						 te->filename);
		}
		if (te->full_filename)
		{
			gtk_window_set_title (GTK_WINDOW (window), buff1);
			tags_manager_set_filename (app->tags_manager,
						   te->full_filename);
		}
		else
		{ gtk_window_set_title (GTK_WINDOW (window), buff2);}
		
		g_free (buff1);
		g_free (buff2);
	}
	else
	{
		gtk_window_set_title (GTK_WINDOW (app->widgets.window),
				      _("Anjuta: No file"));
	}
}

void
anjuta_app_apply_preferences (void)
{
	TextEditor *te;
	gint i;
	gint no_tag;

	Preferences *pr = app->preferences;
	no_tag = preferences_get_int (pr, EDITOR_TAG_HIDE);
	if (no_tag == TRUE)
	{
		gtk_notebook_set_show_tabs (GTK_NOTEBOOK
					    (app->widgets.notebook), FALSE);
	}
	else
	{
		gchar *tag_pos;

		tag_pos = preferences_get (pr, EDITOR_TAG_POS);

		if (strcmp (tag_pos, "bottom") == 0)
			gtk_notebook_set_tab_pos (GTK_NOTEBOOK
						  (app->widgets.notebook),
						  GTK_POS_BOTTOM);
		else if (strcmp (tag_pos, "left") == 0)
			gtk_notebook_set_tab_pos (GTK_NOTEBOOK
						  (app->widgets.notebook),
						  GTK_POS_LEFT);
		else if (strcmp (tag_pos, "right") == 0)
			gtk_notebook_set_tab_pos (GTK_NOTEBOOK
						  (app->widgets.notebook),
						  GTK_POS_RIGHT);
		else
			gtk_notebook_set_tab_pos (GTK_NOTEBOOK
						  (app->widgets.notebook),
						  GTK_POS_TOP);
		g_free (tag_pos);
		gtk_notebook_set_show_tabs (GTK_NOTEBOOK
					    (app->widgets.notebook), TRUE);
	}
	messages_update (app->messages);
	for (i = 0; i < g_list_length (app->text_editor_list); i++)
	{
		te =	(TextEditor*) (g_list_nth (app->text_editor_list, i)->data);
		text_editor_update_preferences (te);
		anjuta_refresh_breakpoints (te);
	}
}

void
anjuta_refresh_breakpoints (TextEditor* te)
{
	breakpoints_dbase_clear_all_in_editor (debugger.breakpoints_dbase, te);
	breakpoints_dbase_set_all_in_editor (debugger.breakpoints_dbase, te);
}

static void
anjuta_app_destroy (AnjutaApp* app)
{
#if 0				/* From here */	
	gchar *tmp;
	pid_t pid;

	g_return_if_fail (app != NULL);

	/* This will remove all tmp files */
	tmp =
		g_strdup_printf ("rm -f %s/anjuta_*.%ld",
				 app->dirs->tmp, (long) getpid ());
	pid = gnome_execute_shell (app->dirs->home, tmp);
	waitpid (pid, NULL, 0);

/* Is it necessary to free up all the memos on exit? */
/* Assuming that it is not, I am disabling the following */
/* Basicaly, because it is faster */


	if (app->project_dbase->project_is_open)
		project_dbase_close_project (app->project_dbase);
	debugger_stop ();

	gtk_widget_hide (app->widgets.notebook);
	for (i = 0;
	     i <
	     g_list_length (GTK_NOTEBOOK (app->widgets.notebook)->
			    children); i++)
	{
		te = anjuta_get_notebook_text_editor (i);
		gtk_container_remove (GTK_CONTAINER
				      (gtk_notebook_get_nth_page
				       (GTK_NOTEBOOK (app->widgets.
						      notebook), i)),
				      te->widgets.client);
		gtk_container_add (GTK_CONTAINER
				   (te->widgets.client_area),
				   te->widgets.client);}
	app->shutdown_in_progress = TRUE;
	for (i = 0; i < g_list_length (app->text_editor_list); i++)
	{
		te =
			(TextEditor
		      *) (g_list_nth_data (app->text_editor_list, i));
		text_editor_destroy (te);
	}
	if (app->text_editor_list)
		g_list_free (app->text_editor_list);
	for (i = 0; i < g_list_length (app->recent_files); i++)
	{
		g_free (g_list_nth_data (app->recent_files, i));
	}
	if (app->recent_files)
		g_list_free (app->recent_files);
	for (i = 0; i < g_list_length (app->recent_projects); i++)
	{
		g_free (g_list_nth_data (app->recent_projects, i));
	}
	if (app->recent_projects)
		g_list_free (app->recent_projects);
	if (app->fileselection)
		gtk_widget_destroy (app->fileselection);
	if (app->save_as_fileselection)
		gtk_widget_destroy (app->save_as_fileselection);
	if (app->preferences)
		preferences_destroy (app->preferences);
	if (app->compiler_options)
		compiler_options_destroy (app->compiler_options);
	if (app->src_paths)
		src_paths_destroy (app->src_paths);
	if (app->messages)
		messages_destroy (app->messages);
	if (app->project_dbase)
		project_dbase_destroy (app->project_dbase);
	if (app->command_editor)
		command_editor_destroy (app->command_editor);
	if (app->find_replace)
		find_replace_destroy (app->find_replace);
	if (app->find_in_files)
		find_in_files_destroy (app->find_in_files);
	if (app->dirs)
		anjuta_dirs_destroy (app->dirs);
	if (app->executer)
		executer_destroy (app->executer);
	if (app->configurer)
		configurer_destroy (app->configurer);
	if (app->tags_manager)
		tags_manager_destroy (app->tags_manager);
	if (app->execution_dir)
		g_free (app->execution_dir);
	debugger_shutdown ();
	
	string_assign (&app->cur_job, NULL);
	app->text_editor_list = NULL;
	app->fileselection = NULL;
	app->save_as_fileselection = NULL;
	app->preferences = NULL;
	main_menu_unref ();
	app = NULL;
#endif /* To here */

	gtk_main_quit ();
}


gboolean
anjuta_init_progress (gchar * description, gdouble full_value,
		      GnomeAppProgressCancelFunc cancel_cb, gpointer data)
{
	if (app->in_progress)
		return FALSE;
	app->in_progress = TRUE;
	app->progress_value = 0.0;
	app->progress_full_value = full_value;
	app->progress_cancel_cb = cancel_cb;
	app->progress_key =
		gnome_app_progress_timeout (GNOME_APP (app->widgets.window),
					    description,
					    100,
					    on_anjuta_progress_cb,
					    on_anjuta_progress_cancel, data);
	return TRUE;
}

void
anjuta_set_progress (gdouble value)
{
	if (app->in_progress)

		app->progress_value = value / app->progress_full_value;
}

void
anjuta_done_progress (gchar * end_mesg)
{
	if (app->in_progress)
		gnome_app_progress_done (app->progress_key);
	anjuta_status (end_mesg);
	app->in_progress = FALSE;
}

void
anjuta_not_implemented (char *file, guint line)
{
	gchar *mesg =
		g_strdup_printf (N_
				 ("Not yet implemented.\nInsert code at \"%s:%u\""),
				 file, line);
	messagebox (GNOME_MESSAGE_BOX_INFO, _(mesg));
	g_free (mesg);
}

void
anjuta_set_busy ()
{
	GnomeAnimator *led;
	GList *node;
	
	led = GNOME_ANIMATOR (app->widgets.toolbar.main_toolbar.led);
	app->busy_count++;
	if (app->busy_count > 1)
		return;
	if (app_cursor)
		gdk_cursor_destroy (app_cursor);
	gnome_animator_stop (led);
	gnome_animator_goto_frame (led, 1);
	app_cursor = gdk_cursor_new (GDK_WATCH);
	if (app->widgets.window->window)
		gdk_window_set_cursor (app->widgets.window->window, app_cursor);
	node = app->text_editor_list;
	while (node)
	{
		if (((TextEditor*)node->data)->widgets.editor->window)
			scintilla_send_message (SCINTILLA(((TextEditor*)node->data)->widgets.editor),
				SCI_SETCURSOR, SC_CURSORWAIT, 0);
		node = g_list_next (node);
	}
	gdk_flush ();
}

void
anjuta_set_active ()
{
	GList* node;
	
	app->busy_count--;
	if (app->busy_count > 0)
		return;
	app->busy_count = 0;
	if (app_cursor)
		gdk_cursor_destroy (app_cursor);
	app_cursor = gdk_cursor_new (GDK_TOP_LEFT_ARROW);
	gdk_window_set_cursor (app->widgets.window->window, app_cursor);
	node = app->text_editor_list;
	while (node)
	{
		scintilla_send_message (SCINTILLA(((TextEditor*)node->data)->widgets.editor),
			SCI_SETCURSOR, SC_CURSORNORMAL, 0);
		node = g_list_next (node);
	}
	update_led_animator ();
}

void
anjuta_load_cmdline_files ()
{
	GList *node;
	
	/* Open up the command argument files */
	node = command_args;
	while (node)
	{
		switch (get_file_ext_type (node->data))
		{
		case FILE_TYPE_IMAGE:
			break;
		case FILE_TYPE_PROJECT:
			if (!app->project_dbase->project_is_open)
			{
				fileselection_set_filename (app->project_dbase->fileselection_open, node->data);
				project_dbase_load_project (app->project_dbase, TRUE);
			}
			break;
		default:
			anjuta_goto_file_line (node->data, -1);
			break;
		}
		node = g_list_next (node);
	}
	if (command_args)
	{
		glist_strings_free (command_args);
		command_args = NULL;
	}
}

static void
anjuta_app_clear_windows_menu ()
{
	guint count;
	count = g_list_length (app->text_editor_list);
	if (count < 1)
		return;
	gnome_app_remove_menus (GNOME_APP (app->widgets.window),
				"_Windows/<Separator>", count + 1);
}

static void
anjuta_app_fill_windows_menu ()
{
	gint count, i;
	GnomeUIInfo wininfo[] = {
		{
		 GNOME_APP_UI_ITEM, NULL,
		 N_("Activate this to select this window"),
		 on_anjuta_window_selected, NULL, NULL,
		 GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_FORWARD, 0, 0, NULL}
		, GNOMEUIINFO_END
	};
	GnomeUIInfo sep[] = {
		GNOMEUIINFO_SEPARATOR,
		GNOMEUIINFO_END
	};
	count = g_list_length (app->text_editor_list);
	if (count > 0)
	{
		gnome_app_insert_menus (GNOME_APP (app->widgets.window),
					"_Windows/Cl_ose Current window",
					sep);
		for (i = (count - 1); i >= 0; i--)
		{
			TextEditor *te;
			te = g_list_nth_data (app->text_editor_list, i);
			wininfo[0].label = te->filename;
			if (te == NULL)
				continue;
			wininfo[0].user_data = te;
			gnome_app_insert_menus (GNOME_APP
						(app->widgets.window),
						"_Windows/<Separator>",
						wininfo);
		}
	}
}

void
anjuta_app_update_app_status (gboolean set_job, gchar* job_name)
{
	gchar *prj, *edit, *job;
	guint line, col, caret_pos;
	gchar *str;
	gint zoom;
	TextEditor *te;
	GtkWidget *sci;

	te = anjuta_get_current_text_editor ();
	if (te)
	{
		sci = te->widgets.editor;
	}
	else
	{
		sci = NULL;
	}
	prj = project_dbase_get_proj_name (app->project_dbase);
	if (!prj)	prj = g_strdup (_("None"));
	zoom = prop_get_int (app->preferences->props, "text.zoom.factor", 0);
	if (sci)
	{
		caret_pos =
			scintilla_send_message (SCINTILLA (sci),
						SCI_GETCURRENTPOS, 0, 0);
		line =
			scintilla_send_message (SCINTILLA (sci),
						SCI_LINEFROMPOSITION,
						caret_pos, 0);
		col =
			scintilla_send_message (SCINTILLA (sci),
						SCI_GETCOLUMN, caret_pos, 0);
		if (scintilla_send_message
		    (SCINTILLA (sci), SCI_GETOVERTYPE, 0, 0))
		{
			edit = g_strdup (_("OVR"));
		}
		else
		{
			edit = g_strdup (_("INS"));
		}
	}
	else
	{
		line = col = 0;
		edit = g_strdup (_("INS"));
	}
	if (set_job)
		string_assign (&app->cur_job, job_name);

	if (app->cur_job)
		job = g_strdup (app->cur_job);
	else
		job = g_strdup (_("None"));
	str =
		g_strdup_printf(
		_("  Project: %s       Zoom: %d       Line: %04d       Col: %03d       %s       Job: %s"),
		 prj, zoom, line, col, edit, job);
	gnome_appbar_set_default (GNOME_APPBAR (app->widgets.appbar), str);
	g_free (str);
	g_free (prj);
	g_free (edit);
	g_free (job);
}

void
anjuta_app_toolbar_set_view (gchar* toolbar_name, gboolean view, gboolean resize, gboolean set_in_props)
{
	gchar* key;
	GnomeDock* dock;
	GnomeDockItem* item;

	if (set_in_props)
	{
		key = g_strconcat (toolbar_name, ".visible", NULL);
		preferences_set_int (app->preferences, key, view);
		g_free (key);
	}

	item = gnome_app_get_dock_item_by_name (GNOME_APP (app->widgets.window), toolbar_name);

	g_return_if_fail (item != NULL);
	g_return_if_fail (GNOME_IS_DOCK_ITEM (item));

	if (view)
		gtk_widget_show (GTK_WIDGET (item));
	else
		gtk_widget_hide (GTK_WIDGET (item));
	dock = gnome_app_get_dock(GNOME_APP(app->widgets.window));
	g_return_if_fail (dock != NULL);
	if (resize)
		gtk_widget_queue_resize (GTK_WIDGET (dock));
}

#endif
