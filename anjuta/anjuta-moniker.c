/*
 * anjuta-moniker.c
 * Copyright (C) 2000  Kh. Naba Kumar Singh
 * 
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free 
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc., 59 
 * Temple Place, Suite 330, Boston, MA  02111-1307  USA 
 */
#include <bonobo/bonobo-exception.h>
#include <bonobo/bonobo-moniker-util.h>

#include "anjuta-shell.h"
#include "anjuta-moniker.h"

static Bonobo_Unknown
resolve (BonoboMoniker *moniker,
			     const Bonobo_ResolveOptions *options,
			     const CORBA_char *requested_interface,
			     CORBA_Environment *ev)
{
	const char *name = bonobo_moniker_get_name (moniker);
	Bonobo_Unknown ret;
	AnjutaShell *shell;
	
	shell = anjuta_get_shell (name);
	
	if (!strcmp (requested_interface, "IDL:GNOME/Development/Anjuta/Shell:1.0"))
	{
		ret = bonobo_object_dup_ref (BONOBO_OBJREF (shell), ev);
	}
	else if (!strcmp (requested_interface, "IDL:Bonobo/ItemContainer:1.0"))
	{
		ret = bonobo_object_dup_ref (BONOBO_OBJREF (shell->item_container), ev);
	}
	else if (!strcmp (requested_interface, "IDL:Bonobo/EventSource:1.0"))
	{
		ret = bonobo_object_dup_ref (BONOBO_OBJREF (shell->event_source), ev);
	}
	else
	{
		ret = CORBA_OBJECT_NIL;
	}
	return ret;
}

static BonoboObject *
moniker_factory (BonoboGenericFactory *fact,
		 const char *object_id,
		 void *data)
{
	return BONOBO_OBJECT (bonobo_moniker_simple_new ("anjuta:", resolve));
}

void
anjuta_moniker_init (void)
{
	BonoboGenericFactory *factory;
	factory = bonobo_generic_factory_new ("OAFIID:Bonobo_Moniker_anjuta_Factory", moniker_factory, NULL);
}
