/*
 * anjuta-shell.c Copyright (C) 2000  Kh. Naba Kumar Singh
 * 
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free 
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc., 59 
 * Temple Place, Suite 330, Boston, MA  02111-1307  USA 
 */

#include <config.h>
#include <gnome.h>
#include <bonobo.h>

#include "anjuta-app.h"
#include "anjuta-shell.h"
#include <libanjuta/libanjuta.h>

struct _AnjutaShellPriv 
{
	GHashTable *components;
	GHashTable *controls;
	GHashTable *anys;
};

static CORBA_Object create_shell (BonoboObject *object);
static void anjuta_shell_destroy (GtkObject *object);
static void anjuta_shell_class_init (AnjutaShellClass *class);
static void anjuta_shell_init (BonoboObject *object);

static BonoboObjectClass *parent_class;
static POA_GNOME_Anjuta_Shell__epv shell_epv;
static POA_GNOME_Anjuta_Shell__vepv shell_vepv;

static inline AnjutaShell *
shell_from_servant (PortableServer_Servant servant)
{
	return ANJUTA_SHELL (bonobo_object_from_servant (servant));
}

static GHashTable *shells = NULL;
static int shell_count = 0;

/* public */
AnjutaShell *
anjuta_get_shell (const char *id)
{
	return g_hash_table_lookup (shells, id);
}

AnjutaShell *
anjuta_shell_new (AnjutaApp *app)
{
	AnjutaShell *shell;
	GNOME_Anjuta_Shell objref;

	g_return_val_if_fail (app != NULL, NULL);

	shell = gtk_type_new (anjuta_shell_get_type ());
	objref = 
		create_shell (BONOBO_OBJECT (shell));
	if (objref == CORBA_OBJECT_NIL) {
		gtk_object_destroy (GTK_OBJECT (shell));
		return NULL;
	}
    
	gtk_object_ref (GTK_OBJECT (shell));
	gtk_object_sink (GTK_OBJECT (shell));
    
	bonobo_object_construct (BONOBO_OBJECT (shell), objref);
    
	shell->app = app;

	shell->id = g_strdup_printf ("Anjuta%d", shell_count++);
	if (!shells) {
		shells = g_hash_table_new (g_str_hash, g_str_equal);
	}
	
	g_hash_table_insert (shells, shell->id, shell);

	return shell;
}

GtkType 
anjuta_shell_get_type (void)
{
	static GtkType type = 0;
        
	if (!type) {
		GtkTypeInfo info = {
			"IDL:GNOME::Anjuta::Shell:1.0",
			sizeof (AnjutaShell),
			sizeof (AnjutaShellClass),
			(GtkClassInitFunc) anjuta_shell_class_init,
			(GtkObjectInitFunc) anjuta_shell_init,
			NULL,
			NULL,
			(GtkClassInitFunc) NULL
		};

		type = gtk_type_unique (bonobo_object_get_type (), &info);
	}

	return type;
}

/* private */
CORBA_Object
create_shell (BonoboObject *object) 
{
	POA_GNOME_Anjuta_Shell *servant;
	CORBA_Environment ev;
	CORBA_exception_init (&ev);
    
	servant = 
		(POA_GNOME_Anjuta_Shell*)g_new0(BonoboObjectServant, 1);
	servant->vepv = &shell_vepv;
    
	POA_GNOME_Anjuta_Shell__init((PortableServer_Servant) servant, 
						      &ev);
	if (ev._major != CORBA_NO_EXCEPTION) {
		g_free (servant);
		CORBA_exception_free (&ev);
		return CORBA_OBJECT_NIL;
	}
	CORBA_exception_free (&ev);
	return bonobo_object_activate_servant (object, servant);
}

static gboolean
object_hash_remove_func (char *name,
			 Bonobo_Unknown obj,
			 AnjutaShell *shell)
{
	g_warning ("Registered object left over: %s", name);
	g_free (name);
	bonobo_object_release_unref (obj, NULL);

	return TRUE;
}

static gboolean 
control_hash_remove_func (char *name,
			  GtkWidget *w,
			  AnjutaShell *shell)
{
	anjuta_app_remove_widget (shell->app, w);
	g_free (name);
	
	return TRUE;
}

static void
anjuta_shell_destroy (GtkObject *object) 
{
	AnjutaShell *shell = ANJUTA_SHELL (object);
	
	g_hash_table_foreach_remove (shell->priv->controls, 
				     (GHRFunc)control_hash_remove_func,
				     shell);
	g_hash_table_foreach_remove (shell->priv->components,
				     (GHRFunc)object_hash_remove_func,
				     shell);
	g_hash_table_remove (shells, shell->id);
	
	g_free (shell->id);
	g_free (shell->priv);
}


static void
impl_remove_object (PortableServer_Servant servant,
		    const CORBA_char *name,
		    CORBA_Environment *ev)
{
	AnjutaShell *shell = shell_from_servant (servant);
	gchar *key;
	CORBA_Object obj;
	GtkWidget *w;
	BonoboArg *arg;

	arg = bonobo_arg_new (BONOBO_ARG_STRING);
	BONOBO_ARG_SET_STRING (arg, name);
	bonobo_event_source_notify_listeners (shell->event_source,
					      "object_removed",
					      arg, NULL);
	
	if (g_hash_table_lookup_extended (shell->priv->controls, name,
					  (gpointer*)&key, (gpointer *)&w)) {
		g_hash_table_remove (shell->priv->controls, name);
		g_free (key);
		anjuta_app_remove_widget (shell->app, w);
	}

	if (g_hash_table_lookup_extended (shell->priv->components, name, 
					  (gpointer*)&key, (gpointer*)&obj)) {
		bonobo_item_container_remove_by_name (shell->item_container, 
						      name);
		g_hash_table_remove (shell->priv->components, name);
		g_free (key);
		bonobo_object_release_unref (obj, ev);
	}

	bonobo_arg_release (arg);
}

static void
impl_add_object (PortableServer_Servant servant, 
		 Bonobo_Unknown obj,
		 const CORBA_char *name,
		 CORBA_Environment *ev)
{
	AnjutaShell *shell = shell_from_servant (servant);
	Bonobo_Unknown dup;
	BonoboArg *arg;
	BonoboObjectClient *cli;
	
	if (g_hash_table_lookup (shell->priv->controls, name)
	    || g_hash_table_lookup (shell->priv->components, name)) {
		impl_remove_object (servant, name, ev);
	}

	dup = bonobo_object_dup_ref (obj, ev);

	g_hash_table_insert (shell->priv->components, 
			     g_strdup (name), 
			     dup);
	cli = bonobo_object_client_from_corba (obj);

	bonobo_item_container_add (shell->item_container, name, BONOBO_OBJECT (cli));

	arg = bonobo_arg_new (BONOBO_ARG_STRING);
	BONOBO_ARG_SET_STRING (arg, name);
	
	bonobo_event_source_notify_listeners (shell->event_source,
					      "object_added",
 					      arg, NULL);
	bonobo_arg_release (arg);
	
}

static void
impl_add_control (PortableServer_Servant servant, 
		  Bonobo_Control ctrl,
		  const CORBA_char *name,
		  const CORBA_char *title,
		  CORBA_Environment *ev)
{
	AnjutaShell *shell = shell_from_servant (servant);
	GtkWidget *w;

	impl_add_object (servant, ctrl, name, ev);

	/* Add to the windows appropriately */

	w = bonobo_widget_new_control_from_objref (bonobo_object_dup_ref (ctrl, ev), 
						   BONOBO_OBJREF (shell->app->ui_container));	

	gtk_widget_show_all (w);
	
	anjuta_app_add_widget (shell->app, w, title);
	
	g_hash_table_insert (shell->priv->controls,
			     g_strdup (name),
			     w);			      
}

static void
impl_add_property_page (PortableServer_Servant servant, 
			Bonobo_Control ctrl,
			const CORBA_char *name,
			const CORBA_char *title,
			CORBA_Environment *ev)
{
	AnjutaShell *shell = shell_from_servant (servant);

	/* FIXME: Property pages are not implemented yet */

	impl_add_object (servant, ctrl, name, ev);
}

static Bonobo_Unknown
impl_get_object (PortableServer_Servant servant,
		 const CORBA_char *name,
		 CORBA_Environment *ev)
{
	AnjutaShell *shell = shell_from_servant (servant);
	gpointer val;
	CORBA_Object ret;

	if ((val = g_hash_table_lookup (shell->priv->components, name))) {
		ret = bonobo_object_dup_ref ((CORBA_Object)val, NULL);
	} else {
		ret = CORBA_OBJECT_NIL;
	}
	return ret;
}

static void
impl_remove_data (PortableServer_Servant servant,
		  const CORBA_char *name,
		  CORBA_Environment *ev)
{
	AnjutaShell *shell = shell_from_servant (servant);
	char *key;
	CORBA_any *any;
	BonoboArg *arg;
	
	if (g_hash_table_lookup_extended (shell->priv->anys, name, 
					  (gpointer*)&key, 
					  (gpointer*)&any)) {
		g_free (key);
		CORBA_free (any);
		g_hash_table_remove (shell->priv->anys, name);
	}

	arg = bonobo_arg_new (BONOBO_ARG_STRING);
	BONOBO_ARG_SET_STRING (arg, name);
	bonobo_event_source_notify_listeners (shell->event_source,
					      "data_removed",
					      arg, NULL);
	bonobo_arg_release (arg);

}

static void
impl_add_data (PortableServer_Servant servant,
	       const CORBA_any *data,
	       const CORBA_char *name,
	       CORBA_Environment *ev)
{
	BonoboArg *arg;
	AnjutaShell *shell = shell_from_servant (servant);
	CORBA_any *dup = CORBA_any__alloc ();
	CORBA_any__copy (dup, (CORBA_any*)data);
	
	if (g_hash_table_lookup (shell->priv->anys, name)) {
		impl_remove_data (servant, name, ev);
	}
	
	g_hash_table_insert (shell->priv->anys, g_strdup (name), 
			     (CORBA_any*)dup);
	arg = bonobo_arg_new (BONOBO_ARG_STRING);
	BONOBO_ARG_SET_STRING (arg, name);
	bonobo_event_source_notify_listeners (shell->event_source,
					      "data_added",
					      arg, NULL);
	bonobo_arg_release (arg);
} 

static CORBA_any *
impl_get_data (PortableServer_Servant servant,
	       const CORBA_char *name,
	       CORBA_Environment *ev)
{
	AnjutaShell *shell = shell_from_servant (servant);
	CORBA_any *ret = CORBA_any_alloc ();
	CORBA_any *orig;
	
	orig = g_hash_table_lookup (shell->priv->anys, name);
	if (orig) {
		CORBA_any__copy (ret, orig);
	}
	
	return ret;
	
}

static Bonobo_UIContainer 
impl_get_ui_container (PortableServer_Servant servant, 
		       CORBA_Environment *ev)
{
	AnjutaShell *shell = shell_from_servant (servant);

	return bonobo_object_dup_ref (bonobo_object_corba_objref (BONOBO_OBJECT (shell->app->ui_container)), NULL);
}

static void
init_shell_corba_class (void) 
{
	/* EPV */
	shell_epv.addControl = impl_add_control;
	shell_epv.addPropertyPage = impl_add_property_page;
	shell_epv.addObject = impl_add_object;
	shell_epv.removeObject = impl_remove_object;
	shell_epv.getObject = impl_get_object;
	shell_epv.getUIContainer = impl_get_ui_container;
	shell_epv.addData = impl_add_data;
	shell_epv.getData = impl_get_data;
	shell_epv.removeData = impl_remove_data;

	/* VEPV */
	shell_vepv.Bonobo_Unknown_epv = bonobo_object_get_epv ();
	shell_vepv.GNOME_Anjuta_Shell_epv = &shell_epv;
}
        
static void
anjuta_shell_class_init (AnjutaShellClass *class) 
{
	GtkObjectClass *object_class = (GtkObjectClass*) class;
	parent_class = gtk_type_class (bonobo_object_get_type ());
    
	object_class->destroy = anjuta_shell_destroy;

	init_shell_corba_class ();
}

static Bonobo_Unknown
get_object (BonoboItemContainer *item_container, CORBA_char *item_name,
	    CORBA_boolean only_if_exists, CORBA_Environment *ev,
	    gpointer data)
{
	AnjutaShell *shell = ANJUTA_SHELL (data);
	Bonobo_Unknown obj = g_hash_table_lookup (shell->priv->components, 
						  item_name);
	if (obj) {
		return bonobo_object_dup_ref (obj, ev);
	} else {
		return CORBA_OBJECT_NIL;
	}
}

static void
anjuta_shell_init (BonoboObject *object)
{
	AnjutaShell *shell = ANJUTA_SHELL (object);
	shell->priv = g_new0 (AnjutaShellPriv, 1);
	shell->priv->components = g_hash_table_new (g_str_hash, g_str_equal);
	shell->priv->controls = g_hash_table_new (g_str_hash, g_str_equal);
	shell->priv->anys = g_hash_table_new (g_str_hash, g_str_equal);

	shell->event_source = bonobo_event_source_new ();
	bonobo_object_add_interface (object, 
				     BONOBO_OBJECT (shell->event_source));

	shell->item_container = bonobo_item_container_new ();
	gtk_signal_connect (GTK_OBJECT (shell->item_container), "get_object",
			    GTK_SIGNAL_FUNC (get_object), shell);
	bonobo_object_add_interface (object, 
				     BONOBO_OBJECT (shell->item_container));
}
