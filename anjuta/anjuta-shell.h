/*
 * anjuta-shell.h Copyright (C) 2000  Kh. Naba Kumar Singh
 * 
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free 
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc., 59 
 * Temple Place, Suite 330, Boston, MA  02111-1307  USA 
 */

#ifndef _ANJUTA_SHELL_H_
#define _ANJUTA_SHELL_H_

#include <bonobo.h>
#include <bonobo/bonobo-event-source.h>
#include "anjuta-app.h"

BEGIN_GNOME_DECLS
 
typedef struct _AnjutaShell      AnjutaShell;
typedef struct _AnjutaShellClass AnjutaShellClass;
typedef struct _AnjutaShellPriv  AnjutaShellPriv;

#define ANJUTA_SHELL_TYPE        (anjuta_shell_get_type ())
#define ANJUTA_SHELL(o)          (GTK_CHECK_CAST ((o), ANJUTA_SHELL_TYPE, AnjutaShell))
#define ANJUTA_SHELL_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), ANJUTA_SHELL_TYPE, AnjutaShellClass))
#define ANJUTA_IS_SHELL(o)       (GTK_CHECK_TYPE ((o), ANJUTA_SHELL_TYPE))
#define ANJUTA_IS_SHELL_CLASS(k) (GTK_CHECK_CLASS_TYPE ((k), ANJUTA_SHELL_TYPE))

struct _AnjutaShell {
	BonoboObject parent;
	
	AnjutaApp *app;
	BonoboEventSource *event_source;
	BonoboItemContainer *item_container;
	
	char *id;

	AnjutaShellPriv *priv;
};

struct _AnjutaShellClass {
	BonoboObjectClass parent_class;
};

GtkType anjuta_shell_get_type (void);
AnjutaShell *anjuta_shell_new (AnjutaApp *app);

AnjutaShell *anjuta_get_shell (const char *id);

END_GNOME_DECLS

#endif /* _ANJUTA_SHELL_H_ */

