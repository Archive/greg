
static void anjuta_child_terminated (int t);

static void
anjuta_fatal_signal_handler (int t)
{
	g_warning(_("Anjuta: Caught signal %d (%s) !"), t, g_strsignal (t));
	exit(1);
}

static void
anjuta_exit_signal_handler (int t)
{
	g_warning(_("Anjuta: Caught signal %d (%s) !"), t, g_strsignal (t));
	anjuta_clean_exit ();
	exit(1);
}

void
anjuta_connect_kernel_signals ()
{
	signal(SIGSEGV, anjuta_fatal_signal_handler);
	signal(SIGILL, anjuta_fatal_signal_handler);
	signal(SIGABRT, anjuta_fatal_signal_handler);
	signal(SIGSEGV, anjuta_fatal_signal_handler);

	signal(SIGINT, anjuta_exit_signal_handler);
	signal(SIGHUP, anjuta_exit_signal_handler);
	signal(SIGQUIT, anjuta_exit_signal_handler);

	signal (SIGCHLD, anjuta_child_terminated);
}

void
anjuta_register_window (GtkWidget * win)
{
	app->registered_windows =
		g_list_append (app->registered_windows, win);
}

void
anjuta_unregister_window (GtkWidget * win)
{
	app->registered_windows =
		g_list_remove (app->registered_windows, win);
}

void
anjuta_foreach_windows (GFunc cb, gpointer data)
{
	g_list_foreach (app->registered_windows, cb, data);
}

void
anjuta_register_child_process (pid_t pid,
			       void (*ch_terminated) (int s,
						      gpointer d),
			       gpointer data)
{
	if (pid < 1)
		return;
	app->registered_child_processes =
		g_list_append (app->registered_child_processes, (int *) pid);
	app->registered_child_processes_cb =
		g_list_append (app->registered_child_processes_cb,
			       ch_terminated);
	app->registered_child_processes_cb_data =
		g_list_append (app->registered_child_processes_cb_data, data);
}

void
anjuta_unregister_child_process (pid_t pid)
{
	gint index;
	index = g_list_index (app->registered_child_processes, (int *) pid);
	app->registered_child_processes =
		g_list_remove (app->registered_child_processes, (int *) pid);
	app->registered_child_processes_cb =
		g_list_remove (app->registered_child_processes_cb,
			       g_list_nth_data (app->
						registered_child_processes_cb,
						index));
	app->registered_child_processes_cb_data =
		g_list_remove (app->registered_child_processes_cb_data,
			       g_list_nth_data (app->

						registered_child_processes_cb_data,
						index));}

void
anjuta_foreach_child_processes (GFunc cb, gpointer data)
{
	g_list_foreach (app->registered_child_processes, cb, data);
}

void
anjuta_child_terminated (int t)
{
	int status;
	gint index;
	pid_t pid;
	int (*callback) (int st, gpointer d);
	gpointer cb_data;
	pid = waitpid (0, &status, WNOHANG);
	if (pid < 1)
		return;
	index = g_list_index (app->registered_child_processes, (int *) pid);
	if (index < 0)
		return;
	callback =
		g_list_nth_data (app->registered_child_processes_cb, index);
	cb_data = g_list_nth_data (app->registered_child_processes_cb, index);
	if (callback)
		(*callback) (status, cb_data);
	anjuta_unregister_child_process (pid);
}
