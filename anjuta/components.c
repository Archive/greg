/*
 * components.c Copyright (C) 2000  Kh. Naba Kumar Singh
 * 
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free 
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc., 59 
 * Temple Place, Suite 330, Boston, MA  02111-1307  USA 
 */
#include <config.h>

#include <gdl/gdl.h>
#include <liboaf/liboaf.h>

#include <libanjuta/libanjuta.h>

#include "anjuta-shell.h"
#include "components.h"

typedef struct 
{
	char *name;
	Bonobo_PropertyBag component;
} InstalledComponent;

#if 0 /* Disabled */

static char *
get_icon_path_for_component_info (const OAF_ServerInfo *info)
{
        OAF_Property *property;
        const char *shell_component_icon_value;

        /* FIXME: liboaf is not const-safe.  */
        property = oaf_server_info_prop_find ((OAF_ServerInfo *) info,
                                              "gide:shell_component_icon");
        if (property == NULL || property->v._d != OAF_P_STRING)
		return NULL;
	
        shell_component_icon_value = property->v._u.value_string;
	
        if (g_path_is_absolute (shell_component_icon_value))
                return g_strdup (shell_component_icon_value);
	
        else
                return g_concat_dir_and_file (GIDE_IMAGES, shell_component_icon_value);
}

#endif /* Disabled */

static InstalledComponent*
initialize_component (AnjutaShell *shell, OAF_ServerInfo *s)
{
	Bonobo_PropertyBag component = CORBA_OBJECT_NIL;
	
	CORBA_Environment ev;
	InstalledComponent *ret;

	CORBA_exception_init (&ev);

	g_print ("Anjuta: Installing Component: %s\n", 
		 oaf_server_info_prop_lookup (s, "name", NULL));

	component = oaf_activate_from_id (s->iid, 0, NULL, &ev);
	if (ev._major == CORBA_NO_EXCEPTION
		&& !CORBA_Object_is_nil (component, &ev))
	{
		bonobo_property_bag_client_set_value_string (component, 
							     "anjuta-shell", shell->id, &ev);
	}
	else
	{
		if (ev._major != CORBA_NO_EXCEPTION)
		{
			g_print ("Exception activating %s: %s",  
				 oaf_server_info_prop_lookup (s, "name", NULL),
				 ev._repo_id);
		}
		else if (!CORBA_Object_is_nil (component, &ev))
		{
			g_print ("Null object while activating %s\n", 
				 oaf_server_info_prop_lookup (s, "name", NULL));
		}
	}
	
	if (!CORBA_Object_is_nil (component, &ev))
	{
		ret = g_new0 (InstalledComponent, 1);
		ret->name = g_strdup (oaf_server_info_prop_lookup (s, "name", NULL));
		ret->component = component;
	}
	else
	{
		ret = NULL;
	}

	CORBA_exception_free (&ev);

	return ret;
}

void
anjuta_app_components_init (AnjutaApp *app)
{
	AnjutaShell *shell;
	CORBA_Environment ev;
	OAF_ServerInfoList *oaf_result;
	CORBA_char *query;
	
	GList *components = NULL;

	g_return_if_fail (app != NULL);
	g_return_if_fail (ANJUTA_IS_APP (app));
	
	shell = anjuta_shell_new (app);
	gtk_object_set_data (GTK_OBJECT (app), "AnjutaShell", shell);
	
	CORBA_exception_init (&ev);
	query = "repo_ids.has ('IDL:Bonobo/PropertyBag:1.0') AND anjuta:shell_component == 'true'";
	oaf_result = oaf_query (query, NULL, &ev);

	if (ev._major == CORBA_NO_EXCEPTION && oaf_result != NULL) {
		int i;
		if (oaf_result->_length == 0)
		{
			g_warning ("Anjuta: No components found.");
		}
		for (i = 0; i < oaf_result->_length; i++)
		{
			InstalledComponent *c;
			
			OAF_ServerInfo *s = &oaf_result->_buffer[i];
			c = initialize_component (shell, s);
			if (c)
				components = g_list_prepend (components, c);
		}
		CORBA_free (oaf_result);
	} else if (ev._major == CORBA_USER_EXCEPTION) {
		g_warning ("Anjuta: Error querying oaf: Corba user exception.");
	} else {
		g_warning ("Anjuta: Error querying oaf: Corba exception.");
	}
	gtk_object_set_data (GTK_OBJECT (app), "InstalledComponents", components);
}

void 
anjuta_app_components_cleanup (AnjutaApp *app)
{
	CORBA_Environment ev;
	AnjutaShell *shell = gtk_object_get_data (GTK_OBJECT (app), "AnjutaShell");
	GList *components = gtk_object_get_data (GTK_OBJECT (app), "InstalledComponents");
	GList *i;
	
	CORBA_exception_init (&ev);
	for (i = components; i != NULL; i = i->next) {
		InstalledComponent *component = i->data;
		g_print ("Uninstalling component: %s\n", component->name);

		bonobo_property_bag_client_set_value_string (component->component,
							     "anjuta-shell", "", &ev);
		g_free (component->name);
		CORBA_Object_release (component->component, &ev);
		g_free (component);
	}	
	CORBA_exception_free (&ev);
	g_list_free (components);
	bonobo_object_unref (BONOBO_OBJECT (shell));
}
