#ifndef _COMPONENTS_H_
#define _COMP0NENTS_H_

#include "anjuta-app.h"

void anjuta_app_components_init (AnjutaApp *app);
void anjuta_app_components_cleanup (AnjutaApp* app);

#endif
