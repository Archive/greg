/*
    main.c
    Copyright (C) 2000  Kh. Naba Kumar Singh

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gnome.h>
#include <liboaf/liboaf.h>
#include <bonobo.h>

#include <libanjuta/libanjuta.h>
#include "anjuta-app.h"
#include "components.h"
#include "splash.h"

/* One and only one instance of AnjutaApp. */
/* AnjutaApp *app;			*/

gboolean no_splash = 0;
GList* command_args;

/* The static variables used in the poptTable.*/
/* anjuta's option table */
static struct 
poptOption anjuta_options[] = {
	{
	 	NULL, '\0', POPT_ARG_INTL_DOMAIN, PACKAGE,
	 	0, NULL, NULL
	},
	{"no-splash", 's', POPT_ARG_NONE, &no_splash, 0, N_("Don't show the splashscreen"), NULL},
	POPT_AUTOHELP {NULL}
};

static guint
create_application (void)
{
	GtkWidget *app;
	// CORBA_Environment ev;
	
	app = anjuta_app_new ();
	gtk_widget_show (app);
	anjuta_app_components_init(ANJUTA_APP(app));
#if 0
	/* Open files from the command line */
	CORBA_exception_init (&ev);
	docman = GNOME_Development_Environment_Shell_getObject 
		(bonobo_object_corba_objref 
		 (gtk_object_get_data (GTK_OBJECT (main_window), "GideShell")),

	/* Restore session */
	flags = gnome_client_get_flags(client);
	if (flags & GNOME_CLIENT_RESTORED)
		anjuta_session_restore(app, client);
	else
		anjuta_load_cmdline_files(app, command_args);
#endif
	return FALSE;
}

int
main (int argc, char *argv[])
{
/*	GnomeClient *client;
	GnomeClientFlags flags; */
	poptContext context;
	const char** args;
	GList* command_args;
	
	CORBA_ORB orb;
	
/*
	GNOME_Development_Anjuta_DocumentManager docman;
	GNOME_Development_Anjuta_Document document;
*/

#ifdef ENABLE_NLS
	bindtextdomain (PACKAGE, PACKAGE_LOCALE_DIR);
	textdomain (PACKAGE);
#endif
	/* Connect the necessary kernal signals */
	/* anjuta_connect_kernel_signals(); */
	
	/* Process command line options */
	gnomelib_register_popt_table (oaf_popt_options, _("Oaf options"));
	gnome_init_with_popt_table("anjuta", VERSION, argc, argv,
				   anjuta_options, 0, &context);

	/* Set anjuta icon */
	/* gnome_window_icon_set_default_from_file (anjuta_res_get_pixmap_file (ANJUTA_PIXMAP_ICON));*/

#if 0
	/* Session management */
	client = gnome_master_client();
	gtk_signal_connect(GTK_OBJECT(client), "save_yourself",
			   GTK_SIGNAL_FUNC(on_anjuta_session_save_yourself),
			   (gpointer) argv[0]);
	gtk_signal_connect(GTK_OBJECT(client), "die",
			   GTK_SIGNAL_FUNC(on_anjuta_session_die), NULL);
#endif

	/* Get the command line files */
	command_args = NULL;
	args = poptGetArgs(context);
	if (args)
	{
		gint i;
		i = 0;
		while (args[i])
		{
			command_args = g_list_append (command_args, g_strdup(args[i]));
			i++;
		}
	}
	poptFreeContext(context);

	if (!no_splash)
		splash_screen ();
	
	/* initialize CORBA, OAF  and bonobo */
	orb = oaf_init (argc, argv);
	if (!orb)
		g_error ("initializing orb failed");

	if (!bonobo_init (orb, NULL, NULL))
		g_error ("Failure starting Bonobo");
	
	gtk_idle_add ((GtkFunction) create_application, NULL);
	bonobo_main ();
	return 0;
}
