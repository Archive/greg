/* 
    pixmaps.h
    Copyright (C) 2000  Kh. Naba Kumar Singh

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#ifndef _PIXMAPS_H_
#define _PIXMAPS_H_

# define ANJUTA_PIXMAP_SPLASH_SCREEN "anjuta_logo.png"
# define ANJUTA_PIXMAP_NEW_FILE "new_file.xpm"
# define ANJUTA_PIXMAP_OPEN_FILE	"open_file.xpm"
# define ANJUTA_PIXMAP_SAVE_FILE	"save_file.xpm"
# define ANJUTA_PIXMAP_SAVE_AS_FILE	"save_as_file.xpm"
# define ANJUTA_PIXMAP_SAVE_ALL	"save_all.xpm"
# define ANJUTA_PIXMAP_CLOSE_FILE	"close_file.xpm"
# define ANJUTA_PIXMAP_RELOAD_FILE	"reload_file.xpm"
# define ANJUTA_PIXMAP_PROJECT	"project.xpm"
# define ANJUTA_PIXMAP_NEW_PROJECT	"new_project.xpm"
# define ANJUTA_PIXMAP_OPEN_PROJECT	"open_project.xpm"
# define ANJUTA_PIXMAP_SAVE_PROJECT	"save_project.xpm"
# define ANJUTA_PIXMAP_CLOSE_PROJECT	"close_project.xpm"
# define ANJUTA_PIXMAP_SYNTAX	"syntax.xpm"
# define ANJUTA_PIXMAP_ADD	"add.xpm"
# define ANJUTA_PIXMAP_REMOVE	"remove.xpm"
# define ANJUTA_PIXMAP_PAGE_SETUP	"page_setup.xpm"
# define ANJUTA_PIXMAP_PRINT "print.xpm"
# define ANJUTA_PIXMAP_EXIT "exit.xpm"
# define ANJUTA_PIXMAP_UNDO "undo.xpm"
# define ANJUTA_PIXMAP_REDO "redo.xpm"
# define ANJUTA_PIXMAP_CUT "cut.xpm"
# define ANJUTA_PIXMAP_COPY "copy.xpm"
# define ANJUTA_PIXMAP_PASTE "paste.xpm"
# define ANJUTA_PIXMAP_CLEAR "clear.xpm"
# define ANJUTA_PIXMAP_SEARCH "search.xpm"
# define ANJUTA_PIXMAP_FIND "find.xpm"
# define ANJUTA_PIXMAP_FIND_IN_FILES "find_in_files.xpm"
# define ANJUTA_PIXMAP_FIND_REPLACE "find_replace.xpm"
# define ANJUTA_PIXMAP_PREFERENCES "preferences.xpm"
# define ANJUTA_PIXMAP_MESSAGES "messages.xpm"
# define ANJUTA_PIXMAP_PROJECT_LISTING "project.xpm"
# define ANJUTA_PIXMAP_BREAKPOINT "breakpoint.xpm"
# define ANJUTA_PIXMAP_WATCH "watch.xpm"
# define ANJUTA_PIXMAP_REGISTERS "registers.xpm"
# define ANJUTA_PIXMAP_STACK "stack.xpm"
# define ANJUTA_PIXMAP_INSPECT "inspect.xpm"
# define ANJUTA_PIXMAP_FRAME "frame.xpm"
# define ANJUTA_PIXMAP_INTERRUPT "interrupt.xpm"
# define ANJUTA_PIXMAP_DUMP "dump.xpm"
# define ANJUTA_PIXMAP_CONSOLE "console.xpm"
# define ANJUTA_PIXMAP_COMPILE "compile.xpm"
# define ANJUTA_PIXMAP_CONFIGURE "configure.xpm"
# define ANJUTA_PIXMAP_BUILD "build.xpm"
# define ANJUTA_PIXMAP_DEBUG "debug.xpm"
# define ANJUTA_PIXMAP_BUILD_ALL "build_all.xpm"
# define ANJUTA_PIXMAP_BUILD_STOP "stop.xpm"
# define ANJUTA_PIXMAP_STOP "stop.xpm"
# define ANJUTA_PIXMAP_EXECUTE "exec.xpm"
# define ANJUTA_PIXMAP_GREP "grep.xpm"
# define ANJUTA_PIXMAP_COMPARE "compare.xpm"
# define ANJUTA_PIXMAP_DIFFERENCE "difference.xpm"
# define ANJUTA_PIXMAP_FILE_VIEW "file_view.xpm"
# define ANJUTA_PIXMAP_INDENT "indent.xpm"
# define ANJUTA_PIXMAP_FLOW "flow.xpm"
# define ANJUTA_PIXMAP_REFERENCE "reference.xpm"
# define ANJUTA_PIXMAP_TRACE "trace.xpm"
# define ANJUTA_PIXMAP_ARCHIVE "archive.xpm"
# define ANJUTA_PIXMAP_HELP "help.xpm"
# define ANJUTA_PIXMAP_CONTENTS "contents.xpm"
# define ANJUTA_PIXMAP_INDEX "index.xpm"
# define ANJUTA_PIXMAP_MAN_PAGE "man_page.xpm"
# define ANJUTA_PIXMAP_GOTO "goto.xpm"
# define ANJUTA_PIXMAP_DOCK "dock.xpm"
# define ANJUTA_PIXMAP_UNDOCK "undock.xpm"
# define ANJUTA_PIXMAP_CONTINUE "continue.xpm"
# define ANJUTA_PIXMAP_RUN_TO_CURSOR "run_to_cursor.xpm"
# define ANJUTA_PIXMAP_STEP_IN "step_in.xpm"
# define ANJUTA_PIXMAP_STEP_OVER "step_over.xpm"
# define ANJUTA_PIXMAP_STEP_OUT "step_out.xpm"
# define ANJUTA_PIXMAP_DEBUG_STOP "debug_stop.xpm"
# define ANJUTA_PIXMAP_POINTER "pointer.xpm"
# define ANJUTA_PIXMAP_WIZARD "wizard.xpm"

# define ANJUTA_PIXMAP_MINI_BUILD "mini_build.xpm"
# define ANJUTA_PIXMAP_MINI_DEBUG "mini_debug.xpm"
# define ANJUTA_PIXMAP_MINI_FIND "mini_find.xpm"
# define ANJUTA_PIXMAP_MINI_CVS "mini_cvs.xpm"
# define ANJUTA_PIXMAP_MINI_DOCK "mini_dock.xpm"
# define ANJUTA_PIXMAP_MINI_MODULES "mini_modules.xpm"


# define ANJUTA_PIXMAP_FILE_FILE "file_file.xpm"
# define ANJUTA_PIXMAP_FILE_H "file_h.xpm"
# define ANJUTA_PIXMAP_FILE_C "file_c.xpm"
# define ANJUTA_PIXMAP_FILE_CPP "file_cpp.xpm"
# define ANJUTA_PIXMAP_FILE_ASM "file_asm.xpm"
# define ANJUTA_PIXMAP_FILE_PIX "file_pix.xpm"
# define ANJUTA_PIXMAP_FILE_XPM "file_pix.xpm"
# define ANJUTA_PIXMAP_FILE_EXE "file_exe.xpm"
# define ANJUTA_PIXMAP_FILE_UNKOWN "file_unkown.xpm"
# define ANJUTA_PIXMAP_FILE_ARCHIVE "file_archive.xpm"
# define ANJUTA_PIXMAP_FILE_PROJECT "file_project.xpm"

# define ANJUTA_PIXMAP_OFOLDER_BLUE "bfoldo.xpm"
# define ANJUTA_PIXMAP_CFOLDER_BLUE "bfoldc.xpm"

# define ANJUTA_PIXMAP_APPWIZ_GEAR "appwid_gear.xpm"

# define ANJUTA_PIXMAP_APP_COMPONENT "appwid_blank.xpm"
# define ANJUTA_PIXMAP_APP_GENERIC "appwid_console.xpm"
# define ANJUTA_PIXMAP_APP_GTK "appwid_draw.xpm"
# define ANJUTA_PIXMAP_APP_GNOME "appwid_wid.xpm"
# define ANJUTA_PIXMAP_APP_GTKMM "appwid_cd.xpm"
# define ANJUTA_PIXMAP_APP_GNOMEMM "appwid_data.xpm"

# define ANJUTA_PIXMAP_RED_LED "ledred.xpm"
# define ANJUTA_PIXMAP_GREEN_LED "ledgreen.xpm"

# define ANJUTA_PIXMAP_BOOKMARK_TOGGLE "bookmark_toggle.xpm"
# define ANJUTA_PIXMAP_BOOKMARK_FIRST "bookmark_first.xpm"
# define ANJUTA_PIXMAP_BOOKMARK_PREV "bookmark_prev.xpm"
# define ANJUTA_PIXMAP_BOOKMARK_NEXT "bookmark_next.xpm"
# define ANJUTA_PIXMAP_BOOKMARK_LAST "bookmark_last.xpm"

# define ANJUTA_PIXMAP_ERROR_PREV "error_prev.xpm"
# define ANJUTA_PIXMAP_ERROR_NEXT "error_next.xpm"

#define ANJUTA_PIXMAP_FOLD_TOGGLE "fold_toggle.xpm"
#define ANJUTA_PIXMAP_FOLD_CLOSE "fold_close.xpm"
#define ANJUTA_PIXMAP_FOLD_OPEN "fold_open.xpm"

#define ANJUTA_PIXMAP_BLOCK_SELECT "block_select.xpm"
#define ANJUTA_PIXMAP_BLOCK_START "block_start.xpm"
#define ANJUTA_PIXMAP_BLOCK_END "block_end.xpm"

#define ANJUTA_PIXMAP_INDENT_INC "indent_inc.xpm"
#define ANJUTA_PIXMAP_INDENT_DCR "indent_dcr.xpm"
#define ANJUTA_PIXMAP_INDENT_AUTO "indent_auto.xpm"
#define ANJUTA_PIXMAP_AUTOFORMAT_SETTING "indent_set.xpm"

#define ANJUTA_PIXMAP_CALLTIP "calltip.xpm"
#define ANJUTA_PIXMAP_AUTOCOMPLETE "autocomplete.xpm"

#endif
