#ifndef _ANJUTA_DOC_COBJECT_H_
#define _ANJUTA_DOC_COBJECT_H_

#include <bonobo.h>
#include "anjuta-document-manager.h"

BEGIN_GNOME_DECLS

typedef struct _AnjutaDocCobject      AnjutaDocCobject;
typedef struct _AnjutaDocCobjectClass AnjutaDocCobjectClass;
typedef struct _AnjutaDocCobjectPriv  AnjutaDocCobjectPriv;

#define ANJUTA_DOC_COBJECT_TYPE        (anjuta_doc_cobject_get_type ())
#define ANJUTA_DOC_COBJECT(o)          (GTK_CHECK_CAST ((o), ANJUTA_DOC_COBJECT_TYPE, AnjutaDocCobject))
#define ANJUTA_DOC_COBJECT_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), ANJUTA_DOC_COBJECT_TYPE, AnjutaDocCobjectClass))
#define ANJUTA_IS_DOC_COBJECT(o)       (GTK_CHECK_TYPE ((o), ANJUTA_DOC_COBJECT_TYPE))
#define ANJUTA_IS_DOC_COBJECT_CLASS(k) (GTK_CHECK_CLASS_TYPE ((k), ANJUTA_DOC_COBJECT_TYPE))

struct _AnjutaDocCobject {
	BonoboObject parent;
	GtkWidget *doc;
};

struct _AnjutaDocCobjectClass {
	BonoboObjectClass parent_class;
};

GtkType
anjuta_doc_cobject_get_type (void);

/* Corba object creation function */
AnjutaDocCobject*
anjuta_doc_cobject_new (AnjutaDoc* doc);

END_GNOME_DECLS
#endif
