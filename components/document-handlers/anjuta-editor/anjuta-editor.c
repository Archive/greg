/*
 * anjuta_editor.c
 * Copyright (C) 2000  Kh. Naba Kumar Singh
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <sys/wait.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
#include <gnome.h>
#include <errno.h>

#include "global.h"
#include "resources.h"
#include "anjuta.h"

#include "anjuta_editor.h"
#include "anjuta_editor_gui.h"
#include "anjuta_editor_cbs.h"
#include "anjuta_editor_menu.h"
#include "utilities.h"
#include "syntax.h"
#include "launcher.h"

#define GTK
#undef PLAT_GTK
#define PLAT_GTK 1
#include "Scintilla.h"
#include "SciLexer.h"
#include "ScintillaWidget.h"

#include "properties.h"
#include "lexer.h"
#include "aneditor.h"
#include "controls.h"

/* Marker 0 is defined for bookmarks */
#define ANJUTA_EDITOR_LINEMARKER	2

/* Do not define any color with this value */
#define MARKER_PROP_END 0xaaaaaa

/* Some crap in interpreting linenum in with scintilla and anjuta */
#define linenum_anjuta_editor_to_scintilla(x) (x-1)
#define linenum_scintilla_to_anjuta_editor(x) (x+1)

struct _AnjutaEditorPriv
{
	/* Short filename */
	gchar *filename;
	
	/* Editor ID for AnEditor */
	AnEditorID editor_id;
	
	/* Autosave timer ID */
	gint autosave_id;
	gboolean autosave_on;

	/* Timer interval in mins */
	gint autosave_it;

	/* Cool! not the usual freeze/thaw */
	/* Something to stop unecessary signalings */
	gint freeze_count;
	
	/* First time exposer */
	gboolean first_time_expose;
};

/* AnjutaEditor signals */
enum
{
	UPDATE_UI,
	LAST_SIGNAL
};

static gint anjuta_editor_signals[LAST_SIGNAL];

GtkVBoxClass *parent_class;


/* marker fore and back colors */
static glong marker_prop[] = 
{
	SC_MARK_ROUNDRECT, 0x00007f, 0xffff80,	/* Book mark */
	SC_MARK_CIRCLE, 0x00007f, 0xff80ff,		/* Breakpoint mark */
	SC_MARK_SHORTARROW, 0x00007f, 0x00ffff,	/* Line mark */
	MARKER_PROP_END,						/* end */
};

static void initialize_markers (AnjutaEditor* te)
{
	gint i, marker;
	g_return_if_fail (te != NULL);
	g_return_if_fail (ANJUTA_IS_EDITOR (te));
	
	marker = 0;
	for (i = 0;; i += 3)
	{
		if (marker_prop[i] == MARKER_PROP_END)
			break;
		scintilla_send_message (SCINTILLA (te->widgets.editor), SCI_MARKERDEFINE,
			marker, marker_prop[i+0]);
		scintilla_send_message (SCINTILLA (te->widgets.editor), SCI_MARKERSETFORE,
			marker, marker_prop[i+1]);
		scintilla_send_message (SCINTILLA (te->widgets.editor), SCI_MARKERSETBACK,
			marker, marker_prop[i+2]);
		marker++;
	}
}

static void anjuta_editor_init (AnjutaEditor* editor);
static void anjuta_editor_class_init (AnjutaEditor* editor);
static void anjuta_editor_destroy (AnjutaEditor* editor);

GtkType
anjuta_editor_get_type ()
{
	static GtkType type = 0;

	if (!type)
	{
		GtkTypeInfo info = {
			"AnjutaEditor",
			sizeof (AnjutaEditor),
			sizeof (AnjutaEditorClass),
			(GtkClassInitFunc) anjuta_editor_class_init,
			(GtkObjectInitFunc) anjuta_editor_init,
			NULL,
			NULL,
			(GtkClassInitFunc) NULL
		};

		type = gtk_type_unique (gtk_vbox_get_type(), &info);
	}
	return type;
}

static void
anjuta_editor_init (AnjutaEditor* te)
{
	g_return_if_fail (te != NULL);
	g_return_if_fail (ANJUTA_IS_EDITOR (te));
	
	te->priv = g_malloc (sizeof(AnjutaEditorPriv));
	
	te->full_filename = NULL;
	te->modified_time = time (NULL);
	//te->preferences = eo;
	te->force_hilite = TE_LEXER_AUTOMATIC;
	te->current_line = 0;
	te->autosave_on = FALSE;
	te->autosave_it = 10;
	//te->props_base = eo->props;
	
	te->priv->filename = NULL;
	te->priv->freeze_count = 0;
	//te->priv->first_time_expose = TRUE;

	te->editor_id = aneditor_new (prop_get_pointer (te->props_base));
	/*
	aneditor_command (te->editor_id, ANE_SETACCELGROUP,
			  (glong) app->accel_group, 0);
	*/
	create_anjuta_editor_gui (te);
	initialize_markers (te);
}

static void
anjuta_editor_class_init (AnjutaEditorClass* klass)
{
	GtkObjectClass *object_class = (GtkObjectClass *) klass;
	parent_class = gtk_type_class (gtk_vbox_get_type ());

	object_class->destroy = anjuta_editor_destroy;

	anjuta_editor_signals[UPDATE_UI] =
		gtk_signal_new ("update_ui",
				GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (AnjutaEditorClass,
						   update_ui),
				gtk_marshal_NONE__POINTER,
				GTK_TYPE_NONE, 1, GTK_TYPE_STRING);

	gtk_object_class_add_signals (object_class,
				      anjuta_editor_signals,
				      LAST_SIGNAL);
}

static void
anjuta_editor_destroy (AnjutaEditor* editor)
{
}

AnjutaEditor *
anjuta_editor_new (Bonobo_UIContainer ui_container)
{
	AnjutaEditor *te;
	te = gtk_type_new (anjuta_editor_get_type ());
	return GTK_WIDGET (te);
}

void
anjuta_editor_freeze (AnjutaEditor * te)
{
	te->freeze_count++;
}

void
anjuta_editor_thaw (AnjutaEditor * te)
{
	te->freeze_count--;
	if (te->freeze_count < 0)
		te->freeze_count = 0;
}

void
anjuta_editor_set_hilite_type (AnjutaEditor * te)
{
	aneditor_command (te->editor_id, ANE_SETLANGUAGE, te->force_hilite,
			  0);
	aneditor_command (te->editor_id, ANE_SETHILITE,
			  (glong) extract_filename (te->full_filename), 0);
}

void
anjuta_editor_set_zoom_factor (AnjutaEditor * te, gint zfac)
{
	aneditor_command (te->editor_id, ANE_SETZOOM, zfac,  0);
}

void
anjuta_editor_destroy (AnjutaEditor * te)
{
	if (te->editor_id)
		aneditor_destroy (te->editor_id);
}

void
anjuta_editor_undock (AnjutaEditor * te, GtkWidget * container)
{
	if (!te)
		return;
	if (te->mode == ANJUTA_EDITOR_WINDOWED)
		return;
	te->mode = ANJUTA_EDITOR_WINDOWED;
	if (GTK_IS_CONTAINER (container) == FALSE)
		return;
	gtk_container_remove (GTK_CONTAINER (container), te->widgets.client);
	gtk_container_add (GTK_CONTAINER (te->widgets.client_area),
			   te->widgets.client);
	gtk_widget_show (te->widgets.client);
	gtk_widget_show (te->widgets.window);
}

void
anjuta_editor_dock (AnjutaEditor * te, GtkWidget * container)
{
	if (!te)
		return;
	if (te->mode == ANJUTA_EDITOR_PAGED)
		return;
	te->mode = ANJUTA_EDITOR_PAGED;
	gtk_widget_hide (te->widgets.window);
	gtk_container_remove (GTK_CONTAINER (te->widgets.client_area),
			      te->widgets.client);
	gtk_container_add (GTK_CONTAINER (container), te->widgets.client);
	gtk_widget_show (te->widgets.client);
}

glong
anjuta_editor_find (AnjutaEditor * te, gchar * str, gint scope, gboolean forward,
		gboolean regexp, gboolean ignore_case, gboolean whole_word)
{
	glong ret;
	GtkWidget *editor;
	glong flags;

	if (!te) return -1;
	editor = te->widgets.editor;
	
	flags = (ignore_case ? 0 : SCFIND_MATCHCASE)
		| (regexp ? SCFIND_REGEXP : 0)
		| (whole_word ? SCFIND_WHOLEWORD : 0)
		| (forward ? 0 : ANEFIND_REVERSE_FLAG);
		
	switch (scope)
	{
		case ANJUTA_EDITOR_FIND_SCOPE_WHOLE:
				if (forward)
				{
					scintilla_send_message (SCINTILLA (editor), SCI_SETANCHOR, 0, 0);
					scintilla_send_message (SCINTILLA (editor), SCI_SETCURRENTPOS, 0, 0);
				}
				else
				{
					glong length;
					length = scintilla_send_message (SCINTILLA (editor), SCI_GETTEXTLENGTH, 0, 0);
					scintilla_send_message (SCINTILLA (editor), SCI_SETCURRENTPOS, length-1, 0);
					scintilla_send_message (SCINTILLA (editor), SCI_SETANCHOR, length-1, 0);
				}
			break;
		default:
	}
	ret = aneditor_command (te->editor_id, ANE_FIND, flags, (long)str);
	return ret;
}

void
anjuta_editor_replace_selection (AnjutaEditor * te, gchar* r_str)
{
	if (!te) return;
	scintilla_send_message (SCINTILLA(te->widgets.editor), SCI_REPLACESEL, 0, (long)r_str);
}

guint
anjuta_editor_get_total_lines (AnjutaEditor * te)
{
	guint i;
	guint count = 0;
	if (te == NULL)
		return 0;
	if (IS_SCINTILLA (te->widgets.editor) == FALSE)
		return 0;
	for (i = 0;
	     i < scintilla_send_message (SCINTILLA (te->widgets.editor),
					 SCI_GETLENGTH, 0, 0); i++)
	{
		if (scintilla_send_message
		    (SCINTILLA (te->widgets.editor), SCI_GETCHARAT, i,
		     0) == '\n')
			count++;
	}
	return count;
}

guint
anjuta_editor_get_current_lineno (AnjutaEditor * te)
{
	guint count;
	
	g_return_val_if_fail (te != NULL, 0);

	count =	scintilla_send_message (SCINTILLA (te->widgets.editor),
					SCI_GETCURRENTPOS, 0, 0);
	count =	scintilla_send_message (SCINTILLA (te->widgets.editor),
					SCI_LINEFROMPOSITION, count, 0);
	return linenum_scintilla_to_anjuta_editor(count);
}

gboolean
anjuta_editor_goto_point (AnjutaEditor * te, guint point)
{
	g_return_val_if_fail (te != NULL, FALSE);
	g_return_val_if_fail(IS_SCINTILLA (te->widgets.editor) == TRUE, FALSE);

	scintilla_send_message (SCINTILLA (te->widgets.editor), SCI_GOTOPOS,
				point, 0);
	return TRUE;
}

gboolean
anjuta_editor_goto_line (AnjutaEditor * te, guint line, gboolean mark)
{
	gint selpos;
	g_return_val_if_fail (te != NULL, FALSE);
	g_return_val_if_fail(IS_SCINTILLA (te->widgets.editor) == TRUE, FALSE);
	g_return_val_if_fail(line >= 0, FALSE);

	te->current_line = line;
	if (mark) anjuta_editor_set_line_marker (te, line);
	scintilla_send_message (SCINTILLA (te->widgets.editor), SCI_ENSUREVISIBLE,
		linenum_anjuta_editor_to_scintilla (line), 0);
	selpos = scintilla_send_message(SCINTILLA (te->widgets.editor), SCI_POSITIONFROMLINE,
		linenum_anjuta_editor_to_scintilla (line), 0);
	scintilla_send_message (SCINTILLA (te->widgets.editor), SCI_SETSELECTIONSTART, selpos, 0);
	scintilla_send_message (SCINTILLA (te->widgets.editor), SCI_SETSELECTIONEND, selpos, 0);
	
	/* This ensures that we have arround 5 lines visible below the mark */
	scintilla_send_message (SCINTILLA (te->widgets.editor), SCI_GOTOLINE, 
		linenum_anjuta_editor_to_scintilla (line)+5, 0);
	scintilla_send_message (SCINTILLA (te->widgets.editor), SCI_GOTOLINE, 
		linenum_anjuta_editor_to_scintilla (line), 0);
	return TRUE;
}

gint
anjuta_editor_goto_block_start (AnjutaEditor* te)
{
	gint line;
	line = aneditor_command (te->editor_id, ANE_GETBLOCKSTARTLINE, 0, 0);
	if (line >= 0) anjuta_editor_goto_line (te, line, TRUE);
	else gdk_beep();
	return line;
}

gint
anjuta_editor_goto_block_end (AnjutaEditor* te)
{
	gint line;
	line = aneditor_command (te->editor_id, ANE_GETBLOCKENDLINE, 0, 0);
	if (line >= 0) anjuta_editor_goto_line (te, line, TRUE);
	else gdk_beep();
	return line;
}

gint
anjuta_editor_set_marker (AnjutaEditor *te, glong line, gint marker)
{
	g_return_val_if_fail (te != NULL, -1);
	g_return_val_if_fail(IS_SCINTILLA (te->widgets.editor) == TRUE, -1);

	/* Scintilla interpretes line+1 rather than line */
	/* A bug perhaps */
	/* So counter balance it with line-1 */
	/* Using the macros linenum_* */
	return scintilla_send_message (SCINTILLA (te->widgets.editor), SCI_MARKERADD,
		linenum_anjuta_editor_to_scintilla (line), marker);
}

void
anjuta_editor_set_line_marker (AnjutaEditor *te, glong line)
{
	g_return_if_fail (te != NULL);
	g_return_if_fail(IS_SCINTILLA (te->widgets.editor) == TRUE);

	anjuta_delete_all_marker (ANJUTA_EDITOR_LINEMARKER);
	anjuta_editor_set_marker (te, line, ANJUTA_EDITOR_LINEMARKER);
}

static gboolean
load_from_file (AnjutaEditor * te, gchar * fn)
{
	FILE *fp;
	gchar *buffer;
	gint nchars;
	struct stat st;
	size_t size;

	if (stat (fn, &st) != 0)
	{
		g_warning ("Could not stat the file");
		return FALSE;
	}
	size = st.st_size;
	scintilla_send_message (SCINTILLA (te->widgets.editor), SCI_CLEARALL,
				0, 0);
	buffer = g_malloc (size);
	if (buffer == NULL && size != 0)
	{
		/* This is funny in unix, but never hurts */
		g_warning ("This file is too big. Unable to allocate memory");
		return FALSE;
	}
	fp = fopen (fn, "rb");
	if (!fp)
	{
		g_free (buffer);
		return FALSE;
	}
	/* Crude way of loading, but faster */
	nchars = fread (buffer, 1, size, fp);
	if (size != nchars)
		g_warning ("File size and loaded size not matching");
	scintilla_send_message (SCINTILLA (te->widgets.editor), SCI_ADDTEXT,
				nchars, (long) buffer);
	g_free (buffer);
	if (ferror (fp))
	{
		fclose (fp);
		return FALSE;
	}
	fclose (fp);
	return TRUE;
}

#if 0

static gint
strip_trailing_spaces (char *data, int ds, gboolean lastBlock)
{
	gint lastRead;
	gchar *w;
	gint i;

	w = data;
	lastRead = 0;

	for (i = 0; i < ds; i++)
	{
		gchar ch;

		ch = data[i];
		if ((ch == ' ') || (ch == '\t'))
		{
			/* Swallow those spaces */
		}
		else if ((ch == '\r') || (ch == '\n'))
		{
			*w++ = ch;
			lastRead = i + 1;
		}
		else
		{
			while (lastRead < i)
			{
				*w++ = data[lastRead++];
			}
			*w++ = ch;
			lastRead = i + 1;
		}
	}
	/* If a non-final block, then preserve whitespace
	 * at end of block as it may be significant.
	 */
	if (!lastBlock)
	{
		while (lastRead < ds)
		{
			*w++ = data[lastRead++];
		}
	}
	return w - data;
}

#endif

static gboolean
save_to_file (AnjutaEditor * te, gchar * fn)
{
	FILE *fp;
	guint nchars;
	gint strip;
	gchar *data;

	fp = fopen (fn, "wb");
	if (!fp)
		return FALSE;
	strip = prop_get_int (te->props_base, "strip.trailing.spaces", 1);
	nchars =
		scintilla_send_message (SCINTILLA (te->widgets.editor),
					SCI_GETLENGTH, 0, 0);
	data =
		(gchar *) aneditor_command (te->editor_id, ANE_GETTEXTRANGE,
					    0, nchars);
	if (data)
	{
		size_t size;

		size = strlen (data);
		if (size != nchars)
			g_warning
				("Text length and no. of bytes saved is not equal");
		size = fwrite (data, size, 1, fp);
		g_free (data);
	}
	if (ferror (fp))
	{
		fclose (fp);
		return FALSE;
	}
	fclose (fp);
	return TRUE;
}

gboolean
anjuta_editor_load_file (AnjutaEditor * te)
{
	gint tags_update;

	if (te == NULL || te->filename == NULL)
		return FALSE;
	if (IS_SCINTILLA (te->widgets.editor) == FALSE)
		return FALSE;
	anjuta_status (_("Loading File ..."));
	anjuta_editor_freeze (te);
	anjuta_set_busy ();
	te->modified_time = time (NULL);
	if (load_from_file (te, te->full_filename) == FALSE)
	{
		anjuta_system_error (errno, _("Could not load file: %s"), te->full_filename);
		anjuta_set_active ();
		anjuta_editor_thaw (te);
		return FALSE;
	}
	scintilla_send_message (SCINTILLA (te->widgets.editor), SCI_GOTOPOS,
				0, 0);
	tags_update = preferences_get_int (te->preferences, AUTOMATIC_TAGS_UPDATE);
	if (app->project_dbase->project_is_open == FALSE && tags_update)
	{
		tags_manager_update (app->tags_manager, te->full_filename);
	}
	else if (project_dbase_is_file_in_module
		    (app->project_dbase, MODULE_SOURCE, te->full_filename) && tags_update)
	{
		tags_manager_update (app->tags_manager, te->full_filename);
	}
	anjuta_set_active ();
	anjuta_editor_thaw (te);
	scintilla_send_message (SCINTILLA (te->widgets.editor),
				SCI_SETSAVEPOINT, 0, 0);
	scintilla_send_message (SCINTILLA (te->widgets.editor),
				SCI_EMPTYUNDOBUFFER, 0, 0);
	anjuta_editor_set_hilite_type (te);
	if (preferences_get_int (te->preferences, FOLD_ON_OPEN))
	{
		aneditor_command (te->editor_id, ANE_CLOSE_FOLDALL, 0, 0);
	}
	anjuta_status (_("File Loaded Successfully"));
	return TRUE;
}

gboolean
anjuta_editor_save_file (AnjutaEditor * te)
{
	gint tags_update;

	if (te == NULL)
		return FALSE;
	if (IS_SCINTILLA (te->widgets.editor) == FALSE)
		return FALSE;
	anjuta_set_busy ();
	anjuta_editor_freeze (te);
	anjuta_status (_("Saving File ..."));
	if (save_to_file (te, te->full_filename) == FALSE)
	{
		anjuta_editor_thaw (te);
		anjuta_set_active ();
		anjuta_system_error (errno, _("Couldn't save file: %s."), te->full_filename);
		return FALSE;
	}
	else
	{
		te->modified_time = time (NULL);
		tags_update =
			preferences_get_int (te->preferences,
					     AUTOMATIC_TAGS_UPDATE);
		if (app->project_dbase->project_is_open == FALSE
		    && tags_update)
			tags_manager_update (app->tags_manager,
					     te->full_filename);
		else
			if (project_dbase_is_file_in_module
			    (app->project_dbase, MODULE_SOURCE, te->full_filename)
			    && tags_update)
			tags_manager_update (app->tags_manager,
					     te->full_filename);
		/* we need to update UI with the call to scintilla */
		anjuta_editor_thaw (te);
		scintilla_send_message (SCINTILLA (te->widgets.editor),
					SCI_SETSAVEPOINT, 0, 0);
		anjuta_set_active ();
		anjuta_status (_("File Saved successfully"));
		return TRUE;
	}
}

gboolean
anjuta_editor_save_yourself (AnjutaEditor * te, FILE * stream)
{
	return TRUE;
}

gboolean
anjuta_editor_recover_yourself (AnjutaEditor * te, FILE * stream)
{
	return TRUE;
}

void
anjuta_editor_update_preferences (AnjutaEditor * te)
{
	Preferences *pr;
	gboolean auto_save;
	guint auto_save_timer;

	auto_save = preferences_get_int (te->preferences, SAVE_AUTOMATIC);
	auto_save_timer =
		preferences_get_int (te->preferences, AUTOSAVE_TIMER);

	g_return_if_fail (te != NULL);
	pr = te->preferences;

	if (auto_save)
	{
		if (te->autosave_on == TRUE)
		{
			if (auto_save_timer != te->autosave_it)
			{
				gtk_timeout_remove (te->autosave_id);
				te->autosave_id =
					gtk_timeout_add (auto_save_timer *
							 60000,
							 on_anjuta_editor_auto_save,
							 te);
			}
		}
		else
		{
			te->autosave_id =
				gtk_timeout_add (auto_save_timer * 60000,
						 on_anjuta_editor_auto_save,
						 te);
		}
		te->autosave_it = auto_save_timer;
		te->autosave_on = TRUE;
	}
	else
	{
		if (te->autosave_on == TRUE)
			gtk_timeout_remove (te->autosave_id);
		te->autosave_on = FALSE;
	}
	anjuta_editor_set_hilite_type (te);
	anjuta_editor_set_zoom_factor(te, preferences_get_int (te->preferences, "text.zoom.factor"));
}

gboolean
anjuta_editor_check_disk_status (AnjutaEditor * te, const gboolean bForce )
{
	struct stat status;
	time_t t;
	
	gchar *buff;
	if (!te)
		return FALSE;
	if (stat (te->full_filename, &status))
		return TRUE;
	t = time(NULL);
	if (te->modified_time > t || status.st_mtime > t)
	{
		/*
		 * Something is worng with the time stamp. They are refering to the
		 * future or the system clock is wrong.
		 * FIXME: Prompt the user about this inconsistency.
		 */
		return TRUE;
	}
	if (te->modified_time < status.st_mtime)
	{
		if( bForce )
		{
			anjuta_editor_load_file (te);
		} else
		{
			buff =
				g_strdup_printf (_
						 ("WARNING: The file \"%s\" in the disk is more recent "
						  "than\nthe current buffer.\nDo you want to reload it."),
				te->filename);
			messagebox2 (GNOME_MESSAGE_BOX_WARNING, buff,
					 GNOME_STOCK_BUTTON_YES,
					 GNOME_STOCK_BUTTON_NO,
					 GTK_SIGNAL_FUNC
					 (on_anjuta_editor_check_yes_clicked),
					 GTK_SIGNAL_FUNC
					 (on_anjuta_editor_check_no_clicked), te);
			return FALSE;
		}
	}
	return TRUE;
}

void
anjuta_editor_undo (AnjutaEditor * te)
{
	scintilla_send_message (SCINTILLA (te->widgets.editor),
				SCI_UNDO, 0, 0);
}

void
anjuta_editor_redo (AnjutaEditor * te)
{
	scintilla_send_message (SCINTILLA (te->widgets.editor),
				SCI_REDO, 0, 0);
}

void
anjuta_editor_update_controls (AnjutaEditor * te)
{
	gboolean F, P, L, C, S;

	if (te == NULL)
		return;
	S = anjuta_editor_is_saved (te);
	L = launcher_is_busy ();
	P = app->project_dbase->project_is_open;

	switch (get_file_ext_type (te->filename))
	{
	case FILE_TYPE_C:
	case FILE_TYPE_CPP:
	case FILE_TYPE_ASM:
		F = TRUE;
		break;
	default:
		F = FALSE;
	}
	switch (get_file_ext_type (te->filename))
	{
	case FILE_TYPE_C:
	case FILE_TYPE_CPP:
	case FILE_TYPE_HEADER:
		C = TRUE;
		break;
	default:
		C = FALSE;
	}
	gtk_widget_set_sensitive (te->buttons.save, !S);
	gtk_widget_set_sensitive (te->buttons.reload,
				  (te->full_filename != NULL));
	gtk_widget_set_sensitive (te->buttons.compile, F && !L);
	gtk_widget_set_sensitive (te->buttons.build, (F || P) && !L);
}

gboolean
anjuta_editor_is_saved (AnjutaEditor * te)
{
	return !(scintilla_send_message (SCINTILLA (te->widgets.editor),
					 SCI_GETMODIFY, 0, 0));
}

gboolean
anjuta_editor_has_selection (AnjutaEditor * te)
{
	gint from, to;
	from = scintilla_send_message (SCINTILLA (te->widgets.editor),
				       SCI_GETSELECTIONSTART, 0, 0);
	to = scintilla_send_message (SCINTILLA (te->widgets.editor),
				     SCI_GETSELECTIONEND, 0, 0);
	return (from == to) ? FALSE : TRUE;
}

gchar*
anjuta_editor_get_selection (AnjutaEditor * te)
{
	guint from, to;
	struct TextRange tr;

	from = scintilla_send_message (SCINTILLA (te->widgets.editor),
				       SCI_GETSELECTIONSTART, 0, 0);
	to = scintilla_send_message (SCINTILLA (te->widgets.editor),
				     SCI_GETSELECTIONEND, 0, 0);
	if (from == to)
		return NULL;
	tr.chrg.cpMin = MIN(from, to);
	tr.chrg.cpMax = MAX(from, to);
	tr.lpstrText = g_malloc (sizeof(gchar)*(tr.chrg.cpMax-tr.chrg.cpMin)+5);
	scintilla_send_message (SCINTILLA(te->widgets.editor), SCI_GETTEXTRANGE, 0, (long)(&tr));
	return tr.lpstrText;
}

void
anjuta_editor_autoformat (AnjutaEditor * te)
{
	gchar *cmd, *file, *fopts, *shell;
	pid_t pid;
	int status;

	if (anjuta_is_installed ("indent", TRUE) == FALSE)
		return;
	if (preferences_get_int (app->preferences, AUTOFORMAT_DISABLE))
	{
		anjuta_warning (_("Autoformat is disablde. If you want, enable in Preferences."));
		return;
	}
	if (te == NULL)
		return;

	anjuta_status (_("Auto formating ..."));
	anjuta_set_busy ();
	anjuta_editor_freeze (te);
	file = get_a_tmp_file ();
	if (save_to_file (te, file) == FALSE)
	{
		remove (file);
		g_free (file);
		anjuta_editor_thaw (te);
		anjuta_set_active ();
		anjuta_warning (_("Error in auto formating ..."));
		return;
	}

	fopts = preferences_get_format_opts (te->preferences);
	cmd = g_strconcat ("indent ", fopts, " ", file, NULL);
	g_free (fopts);

	/* This does not work, because SIGCHILD is not emitted.
	 * pid = gnome_execute_shell(app->dirs->tmp, cmd);
	 * So using fork instead.
	 */
	shell = gnome_util_user_shell ();
	if ((pid = fork ()) == 0)
	{
		execlp (shell, shell, "-c", cmd, NULL);
		g_error (_("Cannot execute command shell"));
	}
	g_free (cmd);

	waitpid (pid, &status, 0);
	scintilla_send_message (SCINTILLA (te->widgets.editor),
				SCI_GETCURRENTPOS, 0, 0);
	scintilla_send_message (SCINTILLA (te->widgets.editor),
				SCI_BEGINUNDOACTION, 0, 0);
	if (load_from_file (te, file) == FALSE)
	{
		anjuta_warning (_("Error in auto formating ..."));
	}
	else
	{
		anjuta_editor_set_hilite_type (te);
		gtk_widget_queue_draw (te->widgets.editor);
		anjuta_status (_("Auto formating completed"));
	}
	remove (file);
	g_free (file);
	anjuta_set_active ();
	anjuta_editor_thaw (te);
	scintilla_send_message (SCINTILLA (te->widgets.editor),
				SCI_ENDUNDOACTION, 0, 0);
	main_toolbar_update ();
	update_main_menubar ();
	anjuta_refresh_breakpoints(te);
}

gboolean
anjuta_editor_is_marker_set (AnjutaEditor* te, gint line, gint marker)
{
	gint state;

	g_return_val_if_fail (te != NULL, FALSE);
	g_return_val_if_fail (line >= 0, FALSE);
	g_return_val_if_fail (marker < 32, FALSE);
	
	state = scintilla_send_message (SCINTILLA(te->widgets.editor),
		SCI_MARKERGET, linenum_anjuta_editor_to_scintilla (line), 0);
	return ((state & (1 << marker)));
}

void
anjuta_editor_delete_marker (AnjutaEditor* te, gint line, gint marker)
{
	g_return_if_fail (te != NULL);
	g_return_if_fail (line >= 0);
	g_return_if_fail (marker < 32);
	
	scintilla_send_message (SCINTILLA(te->widgets.editor),
		SCI_MARKERDELETE, linenum_anjuta_editor_to_scintilla (line), marker);
}
gint
anjuta_editor_line_from_handle (AnjutaEditor* te, gint marker_handle)
{
	gint line;
	
	g_return_val_if_fail (te != NULL, -1);
	
	line = scintilla_send_message (SCINTILLA(te->widgets.editor),
		SCI_MARKERLINEFROMHANDLE, marker_handle, 0);
	
	return linenum_scintilla_to_anjuta_editor (line);
}

gint anjuta_editor_get_bookmark_line( AnjutaEditor* te, const gint nLineStart )
{
	return aneditor_command (te->editor_id, ANE_GETBOOKMARK_POS, nLineStart, 0 );
}


gint anjuta_editor_get_num_bookmarks(AnjutaEditor* te)
{
	gint	nLineNo = -1 ;
	gint	nMarkers = 0 ;
	
	g_return_val_if_fail (te != NULL, 0 );

	while( ( nLineNo = anjuta_editor_get_bookmark_line( te, nLineNo ) ) >= 0 )
	{
		//printf( "Line %d\n", nLineNo );
		nMarkers++;
	}
	//printf( "out Line %d\n", nLineNo );
	return nMarkers ;
}
