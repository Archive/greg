/*
 * anjuta_editor.h
 * Copyright (C) 2001  Kh. Naba Kumar Singh
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#ifndef _ANJUTA_EDITOR_H_
#define _ANJUTA_EDITOR_H_

#include "global.h"
#include "anjuta_editor_menu.h"
#include "preferences.h"
#include "aneditor.h"

BEGIN_GNOME_DECLS

#define ANJUTA_EDITOR_FIND_SCOPE_WHOLE 1
#define ANJUTA_EDITOR_FIND_SCOPE_CURRENT 2
#define ANJUTA_EDITOR_FIND_SCOPE_SELECTION 3

typedef struct _AnjutaEditor AnjutaEditor;
typedef struct _AnjutaEditorClass AnjutaEditorClass;
typedef struct _AnjutaEditorPriv AnjutaEditorPriv;

#define ANJUTA_EDITOR_TYPE        (anjuta_editor_get_type ())
#define ANJUTA_EDITOR(o)          (GTK_CHECK_CAST ((o), ANJUTA_EDITOR_TYPE, AnjutaEditor))
#define ANJUTA_EDITOR_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), ANJUTA_EDITOR_TYPE, AnjutaEditorClass))
#define ANJUTA_IS_EDITOR(o)       (GTK_CHECK_TYPE ((o), ANJUTA_EDITOR_TYPE))
#define ANJUTA_IS_EDITOR_CLASS(k) (GTK_CHECK_CLASS_TYPE ((k), ANJUTA_EDITOR_TYPE))

/*
enum _AnjutaEditorMode
{
	ANJUTA_EDITOR_PAGED,
	ANJUTA_EDITOR_WINDOWED
};

struct _AnjutaEditorGeom
{
	guint x, y, width, height;
};

struct _AnjutaEditorButtons
{
	GtkWidget *new;
	GtkWidget *open;
	GtkWidget *save;
	GtkWidget *reload;
	GtkWidget *cut;
	GtkWidget *copy;
	GtkWidget *paste;
	GtkWidget *find;
	GtkWidget *replace;
	GtkWidget *compile;
	GtkWidget *build;
	GtkWidget *print;
	GtkWidget *attach;
};

struct _AnjutaEditorGui
{
	GtkWidget *window;
	GtkWidget *client_area;
	GtkWidget *client;
	GtkWidget *line_label;
	GtkWidget *editor;
};
*/

struct _AnjutaEditor
{
	GtkVBox parent;

	/* filename with full path */
	gchar *full_filename;
	
	/* Last save time */
	time_t modified_time;
	
	/* hilite style */
	gint hilite_style;

	/* Current line number */
	glong current_line;

	//Preferences *preferences;

	/* Properties set ID in the preferences */
	//gint props_base;

	/* Private data */
	AnjutaEditorPriv* priv;
};

struct _AnjutaEditorClass
{
	GtkVBoxClass parent_class;
	void (*update_ui) (AnjutaEditor *docman);
};

//void create_anjuta_editor_gui (AnjutaEditor * te);

GtkType
anjuta_editor_get_type (void);

GtkWidget*
anjuta_editor_new ();

void anjuta_editor_freeze (AnjutaEditor * te);

void anjuta_editor_thaw (AnjutaEditor * te);

void anjuta_editor_set_hilite_type (AnjutaEditor * te);

void anjuta_editor_set_zoom_factor (AnjutaEditor * te, gint zfac);

void anjuta_editor_show (AnjutaEditor * te);

void anjuta_editor_hide (AnjutaEditor * te);

void anjuta_editor_destroy (AnjutaEditor * te);

void anjuta_editor_undo (AnjutaEditor * te);
void anjuta_editor_redo (AnjutaEditor * te);

glong
anjuta_editor_find (AnjutaEditor * te, gchar * str, gint scope, gboolean forward,
		  gboolean regexp, gboolean ignore_case, gboolean whole_word);

void anjuta_editor_replace_selection (AnjutaEditor * te, gchar * r_str);

guint anjuta_editor_get_total_lines (AnjutaEditor * te);
guint anjuta_editor_get_current_lineno (AnjutaEditor * te);
gchar* anjuta_editor_get_selection (AnjutaEditor * te);

gboolean anjuta_editor_goto_point (AnjutaEditor * te, guint num);
gboolean anjuta_editor_goto_line (AnjutaEditor * te, guint num, gboolean mark);
gint anjuta_editor_goto_block_start (AnjutaEditor* te);
gint anjuta_editor_goto_block_end (AnjutaEditor* te);

void anjuta_editor_set_line_marker (AnjutaEditor * te, glong line);
gint anjuta_editor_set_marker (AnjutaEditor * te, glong line, gint marker);

gboolean anjuta_editor_load_file (AnjutaEditor * te);
gboolean anjuta_editor_save_file (AnjutaEditor * te);

gboolean anjuta_editor_is_saved (AnjutaEditor * te);
gboolean anjuta_editor_has_selection (AnjutaEditor * te);

void anjuta_editor_update_preferences (AnjutaEditor * te);
void anjuta_editor_update_controls (AnjutaEditor * te);

void anjuta_editor_dock (AnjutaEditor * te, GtkWidget * container);
void anjuta_editor_undock (AnjutaEditor * te, GtkWidget * container);

gboolean anjuta_editor_save_yourself (AnjutaEditor * te, FILE * stream);
gboolean anjuta_editor_recover_yourself (AnjutaEditor * te, FILE * stream);

/*gboolean anjuta_editor_check_disk_status (AnjutaEditor * te);*/
gboolean anjuta_editor_check_disk_status (AnjutaEditor * te, const gboolean bForce );

void anjuta_editor_autoformat (AnjutaEditor * te);
gboolean anjuta_editor_is_marker_set (AnjutaEditor* te, gint line, gint marker);
void anjuta_editor_delete_marker (AnjutaEditor* te, gint line, gint marker);
gint anjuta_editor_line_from_handle (AnjutaEditor* te, gint marker_handle);
gint anjuta_editor_get_bookmark_line( AnjutaEditor* te, const gint nLineStart );
gint anjuta_editor_get_num_bookmarks(AnjutaEditor* te);


END_GNOME_DECLS

#endif
