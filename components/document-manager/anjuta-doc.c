/*
    anjuta_doc.c
    Copyright (C) 2000  Kh. Naba Kumar Singh

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <bonobo.h>
#include <bonobo/bonobo-stream-memory.h>
#include <fcntl.h>
#include <liboaf/liboaf.h>
#include <libgnomevfs/gnome-vfs-mime.h>
#include <libgnomevfs/gnome-vfs-mime-info.h>
#include <libgnomevfs/gnome-vfs-mime-handlers.h>

#include "anjuta-doc.h"
enum ComponentType
{
	VIEWER,
	EDITABLE,
	LAUNCHER
};

typedef struct
{
	gchar* name;
	gchar* iid;
	guint menu_item;
	enum ComponentType type;
	gboolean need_terminal;
} AvailableComponent;

static void anjuta_doc_class_init(AnjutaDocClass* klass);
static void anjuta_doc_init(AnjutaDoc* doc);
static void anjuta_doc_destroy(GtkWidget* doc, gpointer data);

static gboolean load_file (AnjutaDoc *document, const char *filename);
static void unload_file (AnjutaDoc *doc);

static void load_mime (AnjutaDoc *doc, const char *mime);
static void unload_mime (AnjutaDoc *doc);

static void update_viewers (AnjutaDoc *document);
static void unload_viewers (AnjutaDoc *document);

static void activate_component (AnjutaDoc *document, gint component_index);
static void unload_component (AnjutaDoc *document);

static void change_component (AnjutaDoc *document, gint component_index);
static void load_file_into_control (AnjutaDoc *document);
static void save_file_from_control (AnjutaDoc *document);

static BonoboStream* get_working_copy (AnjutaDoc *doc);
static void set_working_copy (AnjutaDoc *doc, BonoboStream* stream);

static gboolean create_editor_widget (AnjutaDoc *document, 
				      AvailableComponent *v);
static gint choose_default_component (AnjutaDoc *document);

static void update_option_menu (AnjutaDoc *doc, AvailableComponent *component);
static GtkWidget* build_component_menu (AnjutaDoc *doc);
static GSList* get_lang_list (void);

static void switch_component_activated_cb (GtkWidget *widget, AnjutaDoc *doc);

//static void set_default_clicked_cb (GtkWidget *btn, AnjutaDoc *document);

enum
{
	MODIFIED,
	READONLY,
	CHANGED,
	LAST_SIGNAL
};

static gint
anjuta_doc_signals[LAST_SIGNAL] = { 0 };

guint
anjuta_doc_get_type()
{
	static guint document_type = 0;

	if (!document_type) {
		GtkTypeInfo document_info = {
			"AnjutaDoc",
			sizeof (AnjutaDoc),
			sizeof (AnjutaDocClass),
			(GtkClassInitFunc) anjuta_doc_class_init,
			(GtkObjectInitFunc) anjuta_doc_init,
			(GtkArgSetFunc) NULL,
			(GtkArgGetFunc) NULL
		};
		document_type = gtk_type_unique (gtk_vbox_get_type (),
						 &document_info);
	}
	return document_type;
}

static void
anjuta_doc_class_init(AnjutaDocClass* klass)
{
	GtkObjectClass *object_class;
	
	object_class = (GtkObjectClass*) klass;

	anjuta_doc_signals[MODIFIED]
		= gtk_signal_new ("modified",
				  GTK_RUN_FIRST,
				  object_class->type,
				  GTK_SIGNAL_OFFSET (AnjutaDocClass,
						     modified),
				  gtk_marshal_NONE__NONE,
				  GTK_TYPE_NONE, 0);

	anjuta_doc_signals[READONLY]
		= gtk_signal_new ("readonly",
				  GTK_RUN_FIRST,
				  object_class->type,
				  GTK_SIGNAL_OFFSET (AnjutaDocClass,
						     readonly),
				  gtk_marshal_NONE__NONE,
				  GTK_TYPE_NONE, 0);

	anjuta_doc_signals[CHANGED]
		= gtk_signal_new ("changed",
				  GTK_RUN_FIRST,
				  object_class->type,
				  GTK_SIGNAL_OFFSET (AnjutaDocClass,
						     changed),
				  gtk_marshal_NONE__NONE,
				  GTK_TYPE_NONE, 0);

    gtk_object_class_add_signals (object_class,
				       anjuta_doc_signals,
				       LAST_SIGNAL);

	 object_class->destroy = (GtkSignalFunc) anjuta_doc_destroy;
}


static void
anjuta_doc_init (AnjutaDoc* doc)
{
	/* Finish up object construction */
	gtk_object_default_construct( GTK_OBJECT(doc) );

	doc->modified = FALSE;
	doc->readonly = FALSE;
	doc->components = NULL;
	doc->ui_container = NULL;
	doc->control_frame = NULL;
	doc->bonobo_widget = NULL;
	
	doc->current_component_index = -1;
	doc->option_menu = gtk_option_menu_new();
	gtk_widget_show (doc->option_menu);
	//gtk_box_pack_end (GTK_BOX(doc), doc->option_menu, FALSE, FALSE, 0);
	doc->file_loaded = FALSE;
	doc->filename = NULL;
	doc->mime_type = NULL;
}

/* New Empty document */
GtkWidget*
anjuta_doc_new (Bonobo_UIContainer UI_container)
{
	CORBA_Environment ev;
	AnjutaDoc* doc;
	//BonoboWindow* win;
	
	doc = gtk_type_new (anjuta_doc_get_type());
	doc->ui_container = UI_container;
	//doc->option_control = bonobo_control_new (option_menu);
	
	//win = bonobo_container_get_win(UI_container);
	//bonobo_window_add_popup(win, doc->option_menu, "/menu/View");
	CORBA_exception_init (&ev);
	Bonobo_Unknown_ref (UI_container, &ev);
	//bonobo_ui_component_object_set ( , "/Toolbar", BONOBO_OBJREF(doc->option_control), &ev);
	CORBA_exception_free (&ev);

	return GTK_WIDGET(doc);
}

/*
 * New document from filename. Mime will be determined and
 * appropriate component loaded.
 */
GtkWidget*
anjuta_doc_new_with_file (Bonobo_UIContainer UI_container, gchar* filename)
{
	GtkWidget* doc;
	const gchar* mime = gnome_vfs_get_file_mime_type (filename, NULL, FALSE);
	doc = anjuta_doc_new_with_mime (UI_container, mime);
	load_file (ANJUTA_DOC(doc), filename);
	return doc;
}

/*
 * New Empty document. Mime will be set for this document
 */
GtkWidget*
anjuta_doc_new_with_mime (Bonobo_UIContainer UI_container, const gchar* mime)
{
	GtkWidget* doc;
	doc = anjuta_doc_new (UI_container);
	load_mime(ANJUTA_DOC(doc), mime);
	return doc;
}

static void
anjuta_doc_destroy (GtkWidget *widget, gpointer data)
{
	AnjutaDoc *document;
	CORBA_Environment ev;
	
	g_return_if_fail (widget != NULL);
	g_return_if_fail (GTK_IS_WIDGET (widget));

	document = ANJUTA_DOC (widget);

	CORBA_exception_init (&ev);
	
	Bonobo_Unknown_unref (document->ui_container, &ev);

	CORBA_exception_free (&ev);
	
	if (document->file_loaded) {
		unload_file (document);
	} else {
		unload_component (document);
	}
	//bonobo_object_unref (document->docobj);
}

/* Returns the bonobo control associated with the component */
BonoboObjectClient*
anjuta_doc_get_control (AnjutaDoc* doc)
{
	BonoboObjectClient *cli;
	g_return_val_if_fail(doc != NULL, NULL);
	cli = bonobo_widget_get_server (BONOBO_WIDGET (doc->bonobo_widget));
	return cli;
}

/*
 * Load file. Mime will be determined and
 * appropriate component loaded if follow_mime is true.
 * otherwise, the document mime will be use. An error will
 * be produced if the file mime doen't match with the doc's mime
 * and follow_mime is true (No file will be loaded). 
 */
gboolean
anjuta_doc_load_file (AnjutaDoc* doc, const gchar* filename, gboolean follow_mime)
{
	g_return_val_if_fail (doc != NULL, FALSE);
	g_return_val_if_fail (filename != NULL, FALSE);
	//g_return_val_if_fail ((ANJUTA_IS_DOC(doc)), FALSE);
	
	printf ("In anjuta_doc_load_file\n");
	return load_file (doc, filename);
}

/*
 * Save file. If follow_mime is true, then the file mime will be
 * tested against the doc mime. Error will be produced if doesn't
 * match. No save will occur. Pass filename = NULL, if the original
 * has to be saved.
 */
gboolean
anjuta_doc_save_file (AnjutaDoc* doc, const gchar* filename, gboolean follow_mime)
{
	return TRUE;
}

/* Readonly and modified status */
void anjuta_doc_set_readonly (AnjutaDoc* doc, gboolean readonly)
{
	g_return_if_fail (doc != NULL);
	doc->readonly = readonly;
	if (readonly) doc->modified = FALSE;
}

gboolean anjuta_doc_get_readonly (AnjutaDoc* doc)
{
	g_return_val_if_fail (doc != NULL, TRUE);
	return doc->readonly;
}

void anjuta_doc_set_modified (AnjutaDoc* doc, gboolean modified)
{
	g_return_if_fail (doc != NULL);
	doc->modified = modified;
}

gboolean anjuta_doc_get_modified (AnjutaDoc* doc)
{
	g_return_val_if_fail (doc != NULL, FALSE);
	return doc->modified;
}

static GList*
get_available_components (const char *mime_type)
{
	/* FIXME: This should cache, since we're likely to do a lot of queries
	 * only only a few different file types */

	CORBA_Environment ev;
	OAF_ServerInfoList *oaf_result;
	CORBA_char *query;
	GList *ret = NULL;
	GList *apps = NULL;
	char *generic;
	char *p;
	GList* node;
	GSList *langs = get_lang_list ();
	
	printf ("In get_available_components\n");

	generic = g_strdup (mime_type);
	p = strchr (generic, '/');
	g_assert (p);
	*(++p) = '*';
	*(++p) = 0;

	CORBA_exception_init (&ev);
	query = g_strdup_printf (
		"repo_ids.has ('IDL:Bonobo/Control:1.0') "
		"AND (bonobo:supported_mime_types.has ('%s') "
		"OR bonobo:supported_mime_types.has ('%s')) "
		"AND (repo_ids.has ('IDL:Bonobo/PersistFile:1.0') "
		"OR repo_ids.has ('IDL:Bonobo/PersistStream:1.0'))",
		mime_type, generic);
	
	oaf_result = oaf_query (query, NULL, &ev);

	g_free (generic);	       
	g_free (query);
	
	if (ev._major == CORBA_NO_EXCEPTION && oaf_result != NULL && oaf_result->_length >= 1) {
		int i;

		for (i = 0; i < oaf_result->_length; i++) {
			OAF_Property *prop;
			AvailableComponent *v = g_new (AvailableComponent, 1);
			OAF_ServerInfo *s = &oaf_result->_buffer[i];
			v->name = g_strdup (oaf_server_info_prop_lookup (s, "name",
									 langs));
			v->iid = g_strdup (s->iid);

			prop = oaf_server_info_prop_find (s, 
							  "bonobo:editable");
			if(prop)
			{
				if (prop->v._u.value_boolean)
					v->type = EDITABLE;
				else
					v->type = VIEWER;
			}
			else
			{
				v->type = VIEWER;
			}
			ret = g_list_prepend (ret, v);
		}
	}
	
	if (oaf_result != NULL) {
		CORBA_free (oaf_result);
	}
	CORBA_exception_free (&ev);
	
	apps = gnome_vfs_mime_get_all_applications (mime_type);
	node = apps;
	while (node)
	{
		GnomeVFSMimeApplication *a = node->data;
		AvailableComponent *v = g_new (AvailableComponent, 1);
		v->name = g_strdup (a->name);
		v->iid = g_strdup (a->command);
		v->need_terminal = a->requires_terminal;
		v->type = LAUNCHER;
		ret = g_list_prepend (ret, v);
		node = g_list_next (node);
	}
	gnome_vfs_mime_application_list_free (apps);
	return ret;
}

static void
destroy_available_components (GList *components)
{
	GList *i;
	printf ("In destroy_available_componets\n");
	for (i  = components; i != NULL; i = i->next) {
		AvailableComponent *c = i->data;
		g_free (c->name);
		g_free (c->iid);
		g_free (c);
	}
	g_list_free (components);
}

static gboolean
load_file (AnjutaDoc *document, const char *filename)
{
	const char *mime_type;
	printf ("In load_file\n");

	if (document->file_loaded)
		unload_file (document);

	document->filename = g_strdup (filename);

	mime_type = gnome_vfs_get_file_mime_type (filename, NULL, FALSE);

	load_mime (document, mime_type);

	if (document->components) {
//		activate_component (document, 
//				    choose_default_component (document));
		load_file_into_control (document);
	}

	document->file_loaded = TRUE;

//	gtk_signal_emit (GTK_OBJECT (document),
//			 doc_signals[DOC_SOURCE_SIGNAL],
//			 filename);

	return TRUE;
}

static void
unload_file (AnjutaDoc *document)
{
	printf ("In unload_file\n");
	unload_component (document);

	unload_mime (document);

	if (document->filename) {
		g_free (document->filename);
		document->filename = NULL;
	}

	if (document->saved_stream) {
		bonobo_object_unref (BONOBO_OBJECT (document->saved_stream));
		document->saved_stream = FALSE;
	}
	document->file_loaded = FALSE;
}

static void
load_mime (AnjutaDoc *doc, const char *mime_type)
{
	gint default_comp;	
	printf ("In load_mime\n");
	if (doc->mime_type) {
		unload_mime (doc);
	}
	doc->mime_type = g_strdup (mime_type);
	update_viewers (doc);
	default_comp = choose_default_component(doc);
	if (default_comp < 0) return;
	activate_component(doc, default_comp);
}

static void
unload_mime (AnjutaDoc *doc)
{
	printf ("In unload_mime\n");

	if (doc->mime_type) {
		unload_viewers (doc);
		g_free (doc->mime_type);
		doc->mime_type = NULL;
	}
}

static void
unload_viewers (AnjutaDoc *doc)
{
	printf ("In unload_viewers\n");
	if (!doc->components)
		return;

	destroy_available_components (doc->components);
	doc->components = NULL;
	gtk_option_menu_remove_menu (GTK_OPTION_MENU (doc->option_menu));
}

static void
launch_command (gchar *fname, char *cmd, gboolean run_in_terminal)
{
	char **argv;
	int i = 0;

	if (run_in_terminal) {
		argv = g_new0 (char *, 5);
		argv[i++] = "gnome-terminal";
		argv[i++] = "--command";
		argv[i++] = g_strdup_printf ("%s %s", 
					     cmd, fname);
		
	} else {
		argv = g_new0 (char*, 3);
		argv[i++] = cmd;
		argv[i++] = fname;
	}
	gnome_execute_async (g_get_home_dir (), i, argv);
	
	if (run_in_terminal) {
		g_free (argv[2]);
	}
	g_free (argv);
}

static void
update_viewers (AnjutaDoc* doc)
{
	GtkWidget *menu;
	printf ("In update_viewers\n");
	if (doc->components) {
		unload_viewers (doc);
	}

	doc->components = 
		get_available_components (doc->mime_type);
	menu = build_component_menu (doc);
	gtk_widget_show_all (menu);
	gtk_option_menu_set_menu (GTK_OPTION_MENU (doc->option_menu),
				  menu);
}

static void
unload_component (AnjutaDoc *doc)
{
	CORBA_Environment ev;
	CORBA_exception_init (&ev);

	printf ("In unload_component\n");
	gtk_container_remove (GTK_CONTAINER (doc),
			      doc->bonobo_widget);
	doc->bonobo_widget = NULL;
	doc->current_component_index = -1;

	if (!CORBA_Object_is_nil (doc->persist_file, &ev)) {
		Bonobo_Unknown_unref (doc->persist_file, &ev);
		CORBA_Object_release (doc->persist_file, &ev);
		doc->persist_file = CORBA_OBJECT_NIL;
	}
	if (!CORBA_Object_is_nil (doc->persist_stream, &ev)) {
		Bonobo_Unknown_unref (doc->persist_stream, &ev);
		CORBA_Object_release (doc->persist_stream, &ev);
		doc->persist_stream = CORBA_OBJECT_NIL;
	}
	
	CORBA_exception_free (&ev);
}

static void
change_component (AnjutaDoc *doc, gint component_index)
{
	BonoboStream *tmp = NULL;
	printf ("In change_component\n");

	g_return_if_fail (component_index >= 0);

	if (doc->current_component_index == component_index) {
		return;
	}
	
	tmp = get_working_copy (doc);
	activate_component (doc, component_index);
	set_working_copy (doc, tmp);

	if (tmp) {
		bonobo_object_unref (BONOBO_OBJECT (tmp));
	}
}

static void
activate_component (AnjutaDoc *doc, gint component_index) 
{
	CORBA_Environment ev;
	AvailableComponent *component;
	printf ("In activate_componet\n");
	
	g_return_if_fail (component_index >= 0);
	CORBA_exception_init (&ev);
	
	if (doc->current_component_index >=  0) {
		unload_component (doc);
	}
	component = g_list_nth_data (doc->components, component_index);
	create_editor_widget (doc, component);
	
	if (doc->bonobo_widget) {
		BonoboObjectClient *cli;
		
		cli = bonobo_widget_get_server (BONOBO_WIDGET (doc->bonobo_widget));
		doc->persist_file = bonobo_object_client_query_interface (cli, "IDL:Bonobo/PersistFile:1.0", NULL);
		
		doc->persist_stream = bonobo_object_client_query_interface (cli, "IDL:Bonobo/PersistStream:1.0", NULL);
		
		doc->control_frame = bonobo_widget_get_control_frame (BONOBO_WIDGET (doc->bonobo_widget));
			      
		if (!doc->control_frame) {
			g_error ("could not get control frame for the component");
		}
		doc->current_component_index = component_index;
	}

	update_option_menu (doc, component);
	
	CORBA_exception_free (&ev);
}

static void
update_option_menu (AnjutaDoc *doc, AvailableComponent *component)
{
	gtk_option_menu_set_history (GTK_OPTION_MENU (doc->option_menu),
				     component->menu_item);
}

static BonoboStream *
get_working_copy (AnjutaDoc *doc)
{
	BonoboStream *ret = NULL;
	AvailableComponent* component;
	CORBA_Environment ev;
	
	CORBA_exception_init (&ev);

	component = g_list_nth_data (doc->components, doc->current_component_index);
	if (component->type != EDITABLE && doc->saved_stream)
	{
		ret = doc->saved_stream;
		doc->saved_stream = NULL;
	}
	else if (component->type == EDITABLE
		   && !CORBA_Object_is_nil (doc->persist_stream, &ev)) {
 		ret = bonobo_stream_mem_create (NULL, 0, 
						FALSE, TRUE);
		Bonobo_PersistStream_save (doc->persist_stream,
					   bonobo_object_corba_objref (BONOBO_OBJECT (ret)),
					   doc->mime_type,
					   &ev);
		if (ev._major != CORBA_NO_EXCEPTION) {
			bonobo_object_unref (BONOBO_OBJECT (ret));
			ret = NULL;
		}
	}

	if (!ret) {
		/* Prompt to save */
 	}

	if (ret) {
		Bonobo_Stream_seek (bonobo_object_corba_objref (BONOBO_OBJECT (ret)),
				    0, Bonobo_Stream_SEEK_SET, &ev);
	}
	
	CORBA_exception_free (&ev);
	return ret;
}

static void
set_working_copy (AnjutaDoc *doc, BonoboStream *stream)
{
	CORBA_Environment ev;
	AvailableComponent* component;
	
	CORBA_exception_init (&ev);

	if (stream && !CORBA_Object_is_nil (doc->persist_stream, &ev)) {
		/* Both components support Bonobo::PersistStream */
		Bonobo_PersistStream_load (doc->persist_stream, 
					   bonobo_object_corba_objref (BONOBO_OBJECT (stream)),
					   doc->mime_type,
					   &ev);
	} else if (stream) {
		/* The first component supported Bonobo::PersistStream, 
		 * but the second one does not.
		 * In this case, the user was not prompted to save
		 * because the previous control supported PersistStream and
		 * its contents were stored to memory. */

		/* FIXME: Offer to save the memory stream */
		load_file_into_control (doc);
	} else {
		load_file_into_control (doc);
	}

	component = g_list_nth_data (doc->components, doc->current_component_index);
	if (!(component->type == EDITABLE) && stream) {
		bonobo_object_ref (BONOBO_OBJECT (stream));
		doc->saved_stream = stream;
	}

	CORBA_exception_free (&ev);
}

static gint
choose_default_component (AnjutaDoc *doc)
{
	OAF_ServerInfo *server_info;
	gint ret = -1;
	printf ("In choose_default_component\n");
	
	g_return_val_if_fail (doc->components != NULL, -1);

	/* First try to match the gnome-vfs default component */
	server_info = gnome_vfs_mime_get_default_component (doc->mime_type);
	if (server_info)
	{
		int i;
		for (i = 0; i < g_list_length (doc->components); i++)
		{
			AvailableComponent *component = g_list_nth_data(doc->components, i);
			if (strcmp (component->iid, server_info->iid) == 0)
			{
				ret = i;
				break;
			}
		}
		CORBA_free (server_info);
	}
	
	/* Then try to find an editable component */
	if (ret < 0)
	{
		int i;
		for (i = 0; i < g_list_length (doc->components); i++)
		{
			AvailableComponent *component = g_list_nth_data(doc->components, i);
			if (component->type == EDITABLE)
			{
				ret = i;
				break;
			}
		}
	}

	/* Then just pick the first one */
	if (ret < 0) {
		ret = 0;
	}
	return ret;
}

static void
save_file (const gchar *filename,
			   Bonobo_PersistFile pfile)
{
	CORBA_Environment ev;
	CORBA_exception_init (&ev);
	Bonobo_PersistFile_save (pfile, filename, &ev);
	if (ev._major != CORBA_NO_EXCEPTION)
		g_warning ("Cannot save.");
	CORBA_exception_free (&ev);
}
static void 
load_file_into_control (AnjutaDoc *document) 
{
	CORBA_Environment ev;

	CORBA_exception_init (&ev);
	printf ("In load_file_into_control\n");

	if (CORBA_Object_is_nil(document->persist_file, &ev)) {
		if (CORBA_Object_is_nil (document->persist_stream, &ev)) {
			g_warning ("The loaded component supports neither Bonobo::PersistFile nor Bonobo::PersistStream");
		} else {
			BonoboStream *stream;
			stream = bonobo_stream_open ("fs", document->filename, Bonobo_Storage_READ, O_RDONLY);
			Bonobo_PersistStream_load (document->persist_stream, bonobo_object_corba_objref (BONOBO_OBJECT (stream)), document->mime_type, &ev);
			bonobo_object_unref (BONOBO_OBJECT (stream));
		}
	} else {
		Bonobo_PersistFile_load (document->persist_file, 
					 document->filename, &ev);
	} 
	CORBA_exception_free (&ev);
}

static void
save_file_from_control (AnjutaDoc *document)
{
	 CORBA_Environment ev;
	 
	 CORBA_exception_init (&ev);
	 
	printf ("In save_file_from_control\n");
	 if (CORBA_Object_is_nil (document->persist_file, &ev)) {
		  if (CORBA_Object_is_nil (document->persist_stream, 
					   &ev)) {
			   g_warning ("The loaded component supports neither Bonobo::PersistFile nor Bonobo::PersistStream");
		  } else {
			   BonoboStream *stream;
			   stream = bonobo_stream_open ("fs", 
							document->filename,
							Bonobo_Storage_WRITE, 
							O_WRONLY);
			   Bonobo_PersistStream_save (document->persist_stream,
						      bonobo_object_corba_objref (BONOBO_OBJECT (stream)),
						      document->mime_type,
						      &ev);
			   bonobo_object_unref (BONOBO_OBJECT (stream));
		  }
	 } else {
		  Bonobo_PersistFile_save (document->persist_file,
					   document->filename, &ev);
	 }
}
static GtkWidget *
build_component_menu (AnjutaDoc *doc)
{
	GtkWidget *menu;
	GtkWidget *item;
	int i;
	printf ("In create_component_menu\n");

	menu = gtk_menu_new ();
	for (i = 0; i < g_list_length (doc->components); i++)
	{
		AvailableComponent *v = g_list_nth_data (doc->components, i);
		gchar *text;
		switch (v->type)
		{
			case EDITABLE:
				text = g_strdup_printf (_("Edit with %s"), v->name);
				break;
			case VIEWER:
				text = g_strdup_printf (_("View with %s"), v->name);
				break;
			case LAUNCHER:
				text = g_strdup_printf (_("Launch %s"), v->name);
				break;
			default:
				g_warning ("Undefined case statement");
				text = g_strdup_printf (_("Unknown component"));
		}
		item = gtk_menu_item_new_with_label (text);
		g_free (text);
		
		/* Rude, but it works :-) */
		/* Add 1 to take care of the NULL condition */
		gtk_object_set_data (GTK_OBJECT (item), "AvailableComponent", (gpointer)(i+1)); 
		
		gtk_signal_connect (GTK_OBJECT (item), "activate", 
				    GTK_SIGNAL_FUNC (switch_component_activated_cb), doc);
		gtk_menu_append (GTK_MENU (menu), item);
		v->menu_item = i;
	}
	return menu;
}

static void
switch_component_activated_cb (GtkWidget *widget, AnjutaDoc *doc)
{
	gint comp_index;
	AvailableComponent* comp;
	
	/* We get the back the actual component index by subtracting 1 */
	comp_index = (gint)gtk_object_get_data (GTK_OBJECT (widget), "AvailableComponent") -1;
	
	if (comp_index <0) return;
		
	comp = g_list_nth_data (doc->components, comp_index);
	if (comp) {
		switch (comp->type)
		{
			case EDITABLE:
			case VIEWER:
				change_component (doc, comp_index);
				break;
			case LAUNCHER:
				launch_command (doc->filename, comp->iid, comp->need_terminal);
				break;
			default:
				g_warning ("Undefined case statement.");
		}
	}
}

static GSList *
get_lang_list (void)
{
        GSList *retval;
        char *lang;
        char * equal_char;

        retval = NULL;
		printf ("In get_lang_list\n");

        lang = g_getenv ("LANGUAGE");

        if (!lang) {
                lang = g_getenv ("LANG");
        }


        if (lang) {
                equal_char = strchr (lang, '=');
                if (equal_char != NULL) {
                        lang = equal_char + 1;
                }

                retval = g_slist_prepend (retval, lang);
        }
        
        return retval;
}

static gboolean
create_editor_widget (AnjutaDoc*document, AvailableComponent *component)
{
	CORBA_Environment ev;
	gboolean ret = FALSE;
	CORBA_exception_init (&ev);
	printf ("In create_editor_widget\n");

	document->bonobo_widget = bonobo_widget_new_control (component->iid, 
							     document->ui_container);

	if (document->bonobo_widget) {		
		gtk_container_add (GTK_CONTAINER (document), 
				   document->bonobo_widget);
		gtk_widget_show (document->bonobo_widget);
		gtk_widget_grab_focus (document->bonobo_widget);
		ret = TRUE;
	} else {
		ret = FALSE;
	}
	CORBA_exception_free (&ev);
	return ret;
}

#if 0
void
split_view (AnjutaDoc* doc)
{
	BonoboViewFrame *view_frame;
	GtkWidget *view_widget;
	GtkWidget *box,*l;

	view_frame = bonobo_client_site_new_view_full (
		buffer->client_site,
		bonobo_object_corba_objref (BONOBO_OBJECT (frame->container)),
		FALSE, TRUE);

	if (!view_frame) {
		g_warning ("create view failed");
		return;
	}

	gtk_object_set_data (GTK_OBJECT(view_frame), "gshell:buffer", buffer);
	frame->view_list = g_list_append (frame->view_list, view_frame);

	gtk_signal_connect (GTK_OBJECT (view_frame), "system_exception",
			    view_system_exception_cb, buffer);
	gtk_signal_connect (GTK_OBJECT (view_frame), "user_activate",
			    GTK_SIGNAL_FUNC (user_activate_request_cb), frame);
	gtk_signal_connect (GTK_OBJECT (view_frame), "activated",
			    GTK_SIGNAL_FUNC (view_activated_cb), frame);

	view_widget = bonobo_view_frame_get_wrapper (view_frame);
	
	box = gtk_vbox_new (FALSE,0);
	gtk_object_set_data (GTK_OBJECT(box), "gshell:view_frame", view_frame);

	l = gtk_label_new (buffer->name);
	gtk_box_pack_start (GTK_BOX (box), l, FALSE, FALSE, 0);
	gtk_box_pack_start (GTK_BOX (box), view_widget, TRUE, TRUE, 0);

	gtk_box_pack_end (GTK_BOX (frame->vbox), box, TRUE, TRUE, 0);

	if (pos>=0) gtk_box_reorder_child (GTK_BOX (frame->vbox), box, pos);

	gtk_widget_show_all (box);

	user_activate_request_cb (view_frame, frame);

	buffer_create_zoomable_frame (buffer, frame, view_frame);
}
#endif
