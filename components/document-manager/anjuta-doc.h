/*
    anjuta_doc.h
    Copyright (C) 2000  Kh. Naba Kumar Singh

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef _ANJUTA_DOC_H_
#define _ANJUTA_DOC_H_

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <bonobo.h>

#define ANJUTA_DOC(o) (GTK_CHECK_CAST((o), anjuta_doc_get_type(), AnjutaDoc))
#define ANJUTA_DOC_CLASS(k) (GTK_CHECK_CLASS_CAST((k), anjuta_doc_get_type(), AnjutaDoc))
#define ANJUTA_IS_DOC(o) (GTK_CHECK_TYPE((o), anjuta_doc_get_type())

typedef struct _AnjutaDoc AnjutaDoc;
typedef struct _AnjutaDocClass AnjutaDocClass;

struct _AnjutaDoc
{
	/* Public */
	GtkVBox parent;
	
	Bonobo_UIContainer ui_container;
	BonoboControlFrame *control_frame;
	GtkWidget *bonobo_widget;
	
	Bonobo_PersistFile persist_file;
	Bonobo_PersistStream persist_stream;
	
	gboolean file_loaded;
	char *filename;
	char *mime_type;
	gboolean modified;
	gboolean readonly;
	
	/* Private */
	GList* components;
	gint current_component_index;
	BonoboStream *saved_stream;
	GtkWidget* option_menu;
};

struct _AnjutaDocClass
{
	GtkVBoxClass klass;
	
	void (* modified) (AnjutaDoc *doc, gboolean modified);
	void (* readonly) (AnjutaDoc *document, gboolean readonly);
    void (* changed) (AnjutaDoc *doc, gint change_type);
};

guint
anjuta_doc_get_type(void);

/* New Empty document */
GtkWidget*
anjuta_doc_new (Bonobo_UIContainer UI_container);

/*
 * New document from filename. Mime will be determined and
 * appropriate component loaded.
 */
GtkWidget*
anjuta_doc_new_with_file (Bonobo_UIContainer UI_conatainer, gchar* filename);

/*
 * New Empty document. Mime will be set for this document
 */
GtkWidget*
anjuta_doc_new_with_mime (Bonobo_UIContainer UI_conatainer, const gchar* mime);

/* Returns the bonobo control associated with the component */
BonoboObjectClient*
anjuta_doc_get_control (AnjutaDoc* doc);

/*
 * Load file. Mime will be determined and
 * appropriate component loaded if follow_mime is true.
 * otherwise, the document mime will be use. An error will
 * be produced if the file mime doen't match with the doc's mime
 * and follow_mime is true (No file will be loaded). 
 */
gboolean
anjuta_doc_load_file (AnjutaDoc* doc, const gchar* filename, gboolean follow_mime);

/*
 * Save file. If follow_mime is true, then the file mime will be
 * tested against the doc mime. Error will be produced if doesn't
 * match. No save will occur.
 */
gboolean
anjuta_doc_save_file (AnjutaDoc* doc, const gchar* filename, gboolean follow_mime);

/* Readonly and modified status */
void anjuta_doc_set_readonly (AnjutaDoc* doc, gboolean readonly);
gboolean anjuta_doc_get_readonly (AnjutaDoc* doc);
void anjuta_doc_set_modified (AnjutaDoc* doc, gboolean modified);
gboolean anjuta_doc_get_modified (AnjutaDoc* doc);

#endif /* ANJUTA_DOC_H */
