/*
 * anjuta-docman-cobject.c
 * Copyright (C) 2000  Kh. Naba Kumar Singh
 * 
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free 
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc., 59 
 * Temple Place, Suite 330, Boston, MA  02111-1307  USA 
 */

#include <libanjuta/libanjuta.h>
#include "anjuta-docman-cobject.h"
#include "anjuta-document-manager.h"

static BonoboObjectClass *parent_class;
static POA_GNOME_Anjuta_DocumentManager__epv adm_epv;
static POA_GNOME_Anjuta_DocumentManager__vepv adm_vepv;

/* Function prototypes */
static void anjuta_document_manager_cobject_init(AnjutaDocumentManagerCobject* cobj);
static void anjuta_document_manager_cobject_class_init(AnjutaDocumentManagerCobjectClass* cobj_class);
static void anjuta_document_manager_cobject_destroy(AnjutaDocumentManagerCobject* cobj);

/* Public Functions */
GtkType
anjuta_document_manager_cobject_get_type ()
{
	static GtkType type = 0;

	if (!type)
	{
		GtkTypeInfo info = {
			"AnjutaDocumentManagerCobject",
			sizeof (AnjutaDocumentManagerCobject),
			sizeof (AnjutaDocumentManagerCobjectClass),
			(GtkClassInitFunc) anjuta_document_manager_cobject_class_init,
			(GtkObjectInitFunc) anjuta_document_manager_cobject_init,
			NULL,
			NULL,
			(GtkClassInitFunc) NULL
		};

		type = gtk_type_unique (bonobo_object_get_type(), &info);
	}

	return type;

}

/* Corba object creation function */
AnjutaDocumentManagerCobject*
anjuta_document_manager_cobject_new (AnjutaDocumentManager* docman)
{
	POA_GNOME_Anjuta_DocumentManager *servant;
	AnjutaDocumentManagerCobject *docm_obj;
	GNOME_Anjuta_DocumentManager objref;
	CORBA_Environment ev;

	g_return_val_if_fail (docman != NULL, NULL);
	g_return_val_if_fail (ANJUTA_IS_DOCUMENT_MANAGER(docman), NULL);
	
	docm_obj = gtk_type_new (anjuta_document_manager_cobject_get_type ());
	
	servant = (POA_GNOME_Anjuta_DocumentManager*) g_new0(BonoboObjectServant, 1);
	servant->vepv = &adm_vepv;
	
	CORBA_exception_init (&ev);
	POA_GNOME_Anjuta_DocumentManager__init((PortableServer_Servant) servant, &ev);
	
	if (ev._major != CORBA_NO_EXCEPTION) {
		g_free (servant);
		CORBA_exception_free (&ev);
		gtk_object_destroy (GTK_OBJECT (docm_obj));
		return NULL;
	}
	
	CORBA_exception_free (&ev);
	objref = bonobo_object_activate_servant (BONOBO_OBJECT(docm_obj), servant);

	gtk_object_ref (GTK_OBJECT (docman));
	docm_obj->docm = GTK_WIDGET (docman);
	
	bonobo_object_construct (BONOBO_OBJECT (docm_obj), objref);
	
	return docm_obj;
}

/* Private Functions */
static void
anjuta_document_manager_cobject_init(AnjutaDocumentManagerCobject* cobj)
{
	/* Doesn't seem to have anything to do right now */
}

static void 
anjuta_document_manager_cobject_destroy(AnjutaDocumentManagerCobject* cobj)
{
	g_return_if_fail (cobj != NULL);
	g_return_if_fail (ANJUTA_IS_DOCUMENT_MANAGER_COBJECT(cobj));
	
	gtk_object_unref (GTK_OBJECT (cobj->docm));
}

static void
impl_add_new_doc (PortableServer_Servant servant, 
		CORBA_Environment *ev)
{
	AnjutaDocumentManagerCobject *cobj;
	
	cobj = ANJUTA_DOCUMENT_MANAGER_COBJECT(bonobo_object_from_servant (servant));
	
	printf ("Trying to add a new doc\n");
	/* Fixme: Is it save to type cast CORBA_char to gchar? */
	anjuta_document_manager_add_new_doc (ANJUTA_DOCUMENT_MANAGER(cobj->docm));
}

static void
impl_goto_file_line (PortableServer_Servant servant, 
		const CORBA_char *filename, CORBA_long line,
		CORBA_Environment *ev)
{
	AnjutaDocumentManagerCobject *cobj;
	
	cobj = ANJUTA_DOCUMENT_MANAGER_COBJECT(bonobo_object_from_servant (servant));
	
	/* Fixme: Is it save to type cast CORBA_char to gchar? */
	//anjuta_document_manager_goto_file_line (ANJUTA_DOCUMENT_MANAGER(cobj->docm), (gchar*)filename, line);
}

static void
impl_goto_file_line_mark (PortableServer_Servant servant, 
		const CORBA_char *filename, CORBA_long line,
		CORBA_Environment *ev)
{
	AnjutaDocumentManagerCobject *cobj;
	
	cobj = ANJUTA_DOCUMENT_MANAGER_COBJECT(bonobo_object_from_servant (servant));
	
	/* Fixme: Is it save to type cast CORBA_char to gchar? */
	//anjuta_document_manager_goto_file_line_mark (ANJUTA_DOCUMENT_MANAGER(cobj->docm), (gchar*)filename, line);
}

static void
anjuta_document_manager_cobject_class_init(AnjutaDocumentManagerCobjectClass* cobj_class)
{
	GtkObjectClass *object_class = (GtkObjectClass*) cobj_class;
	parent_class = gtk_type_class (bonobo_object_get_type ());
    
	object_class->destroy = anjuta_document_manager_cobject_destroy;
    	
	/* EPV initialization */
	adm_epv.addNewDoc = impl_add_new_doc;
	adm_epv.gotoFileLine = impl_goto_file_line;
	adm_epv.gotoFileLineMark = impl_goto_file_line_mark;

	/* VEPV initialization */
	adm_vepv.Bonobo_Unknown_epv = bonobo_object_get_epv ();
	adm_vepv.GNOME_Anjuta_DocumentManager_epv = &adm_epv;
}
