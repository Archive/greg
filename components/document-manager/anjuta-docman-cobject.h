#ifndef _ANJUTA_DOCUMENT_MANAGER_COBJECT_H_
#define _ANJUTA_DOCUMENT_MANAGER_COBJECT_H_

#include <bonobo.h>
#include "anjuta-document-manager.h"

BEGIN_GNOME_DECLS

typedef struct _AnjutaDocumentManagerCobject      AnjutaDocumentManagerCobject;
typedef struct _AnjutaDocumentManagerCobjectClass AnjutaDocumentManagerCobjectClass;
typedef struct _AnjutaDocumentManagerCobjectPriv  AnjutaDocumentManagerCobjectPriv;

struct _AnjutaDocumentManager;

#define ANJUTA_DOCUMENT_MANAGER_COBJECT_TYPE        (anjuta_document_manager_cobject_get_type ())
#define ANJUTA_DOCUMENT_MANAGER_COBJECT(o)          (GTK_CHECK_CAST ((o), ANJUTA_DOCUMENT_MANAGER_COBJECT_TYPE, AnjutaDocumentManagerCobject))
#define ANJUTA_DOCUMENT_MANAGER_COBJECT_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), ANJUTA_DOCUMENT_MANAGER_COBJECT_TYPE, AnjutaDocumentManagerCobjectClass))
#define ANJUTA_IS_DOCUMENT_MANAGER_COBJECT(o)       (GTK_CHECK_TYPE ((o), ANJUTA_DOCUMENT_MANAGER_COBJECT_TYPE))
#define ANJUTA_IS_DOCUMENT_MANAGER_COBJECT_CLASS(k) (GTK_CHECK_CLASS_TYPE ((k), ANJUTA_DOCUMENT_MANAGER_COBJECT_TYPE))

struct _AnjutaDocumentManagerCobject {
	BonoboObject parent;
	GtkWidget *docm;
};

struct _AnjutaDocumentManagerCobjectClass {
	BonoboObjectClass parent_class;
};

GtkType
anjuta_document_manager_cobject_get_type (void);

/* Corba object creation function */
AnjutaDocumentManagerCobject*
anjuta_document_manager_cobject_new (AnjutaDocumentManager* docm);

END_GNOME_DECLS
#endif
