/*
 * anjuta-docman-control.h
 * Copyright (C) 2000  Kh. Naba Kumar Singh
 * 
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free 
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc., 59 
 * Temple Place, Suite 330, Boston, MA  02111-1307  USA 
 */

#include <config.h>
#include <gnome.h>
#include <bonobo.h>
#include <liboaf/liboaf.h>
#include <bonobo/bonobo-shlib-factory.h>

#include <libanjuta/libanjuta.h>

#include "anjuta-docman-cobject.h"
#include "anjuta-document-manager.h"

#define COMPONENT_FACTORY_IID "OAFIID:GNOME_Anjuta_DocumentManager_Factory"
#define COMPONENT_CONTROL_IID "OAFIID:GNOME_Anjuta_DocumentManager_Control"

static BonoboGenericFactory *factory;
static gint active_controls = 0;

enum
{
	ARG_TABSIZE,
	ARG_AUTOSAVE
};

static void on_file_new (GtkWidget* w, BonoboControl* control);
static void on_file_open (GtkWidget* w, BonoboControl* control);
static void on_file_reload (GtkWidget* w, BonoboControl* control);
static void on_file_save (GtkWidget* w, BonoboControl* control);
static void on_file_save_as (GtkWidget* w, BonoboControl* control);
static void on_file_save_all (GtkWidget* w, BonoboControl* control);
static void on_file_close (GtkWidget* w, BonoboControl* control);
static void on_file_close_all (GtkWidget* w, BonoboControl* control);

static BonoboUIVerb verbs[] = 
{
	BONOBO_UI_UNSAFE_VERB ("FileNew", on_file_new),
	BONOBO_UI_UNSAFE_VERB ("FileOpen", on_file_open),
	BONOBO_UI_UNSAFE_VERB ("FileReload", on_file_reload),
	BONOBO_UI_UNSAFE_VERB ("FileSave", on_file_save),
	BONOBO_UI_UNSAFE_VERB ("FileSaveAs", on_file_save_as),
	BONOBO_UI_UNSAFE_VERB ("FileSaveAll", on_file_save_all),
	BONOBO_UI_UNSAFE_VERB ("FileClose", on_file_close),
	BONOBO_UI_UNSAFE_VERB ("FileCloseAll", on_file_close_all),
	BONOBO_UI_VERB_END
};

static void on_file_new(GtkWidget* w, BonoboControl* control)
{
	AnjutaDocumentManager* docm = ANJUTA_DOCUMENT_MANAGER(
		gtk_object_get_data (GTK_OBJECT(control),
		"DocumentManager"));
	anjuta_document_manager_add_new_doc (docm);
}

static void on_file_open(GtkWidget* w, BonoboControl* control)
{
}

static void on_file_reload(GtkWidget* w, BonoboControl* control)
{
}

static void on_file_save(GtkWidget* w, BonoboControl* control)
{
}

static void on_file_save_as(GtkWidget* w, BonoboControl* control)
{
}

static void on_file_save_all(GtkWidget* w, BonoboControl* control)
{
}

static void on_file_close(GtkWidget* w, BonoboControl* control)
{
}

static void on_file_close_all(GtkWidget* w, BonoboControl* control)
{
}

static void
get_prop (BonoboPropertyBag * bag, BonoboArg * arg, guint arg_id, 
	  CORBA_Environment *ev, gpointer data)
{
	AnjutaDocumentManager* docm = data;
	
	switch (arg_id) {
		case ARG_TABSIZE:
			BONOBO_ARG_SET_INT (arg, docm->tabsize);
			break;
		case ARG_AUTOSAVE:
			BONOBO_ARG_SET_BOOLEAN (arg, docm->autosave);
			break;
		default:
			g_warning ("Unknow property queried with the control");
			break;
	}
}

static void
set_prop (BonoboPropertyBag * bag, const BonoboArg * arg, guint arg_id, 
	  CORBA_Environment *ev, gpointer data)
{
	AnjutaDocumentManager* docm = data;
	
	switch (arg_id) {
		case ARG_TABSIZE:
			docm->tabsize = BONOBO_ARG_GET_INT (arg);
			break;
		case ARG_AUTOSAVE:
			docm->autosave = BONOBO_ARG_GET_BOOLEAN (arg);
			break;
		default:
			g_warning ("Unknow property set in the control");
	}
}

static void
on_control_set_frame (BonoboControl *control,
	      gpointer data)
{
	Bonobo_UIContainer uic;
	BonoboUIComponent *component;
	AnjutaDocumentManager* docm;

	if (bonobo_control_get_control_frame (control) == CORBA_OBJECT_NIL)
		return;
	
	docm = gtk_object_get_data (GTK_OBJECT(control), "DocumentManager");

	uic = bonobo_control_get_remote_ui_container (control);
	component = bonobo_control_get_ui_component (control);
	bonobo_ui_component_set_container (component, uic);
	docm->uic = uic;

	/* Setup anjuta document manager control ui.*/
	bonobo_ui_component_add_verb_list_with_data (component, verbs, control);

	bonobo_ui_util_set_ui (component, GNOME_DATADIR,
			       "anjuta-document-manager.xml",
			       "Anjuta Document manager");
}

static void
on_control_destroy (GtkObject *control, AnjutaDocumentManager *docm)
{
	active_controls --;

	if (active_controls)
		return;

	g_print ("Anjuta document manager: factory now shutting down\n");
	bonobo_object_unref (BONOBO_OBJECT (factory));
	gtk_main_quit ();
}

/* PropertyBag */
static void
property_bag_init(BonoboControl* control)
{
	BonoboPropertyBag  *pb;
	BonoboArg  *arg;
	GtkWidget* docm;
	
	docm = gtk_object_get_data(GTK_OBJECT(control), "DocumentManager");
	
	pb = bonobo_property_bag_new (get_prop, set_prop, docm);
	bonobo_control_set_properties (control, pb);

	arg = bonobo_arg_new (BONOBO_ARG_BOOLEAN);
	BONOBO_ARG_SET_BOOLEAN (arg, TRUE);

	bonobo_property_bag_add (pb, "AutoSave", ARG_AUTOSAVE,
				 BONOBO_ARG_BOOLEAN, arg,
				 "Auto save donuments", 0);

	CORBA_free (arg);
	
	arg = bonobo_arg_new (BONOBO_ARG_INT);
	BONOBO_ARG_SET_INT (arg, 8);

	bonobo_property_bag_add (pb, "TabSize", ARG_TABSIZE,
				 BONOBO_ARG_INT, arg,
				 "Editor Tab size", 0);
	CORBA_free (arg);

	bonobo_object_unref (BONOBO_OBJECT (pb));
}

static BonoboObject *
control_factory (BonoboGenericFactory *factory, gpointer closure)
{
	BonoboControl *control;
	GtkWidget *docm;
	AnjutaDocumentManagerCobject *docm_obj;
	
	active_controls++;

	/* We don't have container now. We will set it later.
	 * when the control fram is set
	 */
	docm = anjuta_document_manager_new(NULL);
	gtk_widget_show (docm);
	
	control = bonobo_control_new (docm);
	gtk_object_set_data (GTK_OBJECT (control), "DocumentManager", docm);
	
	docm_obj = anjuta_document_manager_cobject_new (
			ANJUTA_DOCUMENT_MANAGER(docm));
	
	bonobo_object_add_interface (BONOBO_OBJECT (control), 
				     BONOBO_OBJECT (docm_obj));

	if (!control){
		gtk_widget_unref (docm);
		return NULL;
	} else {
		property_bag_init(control);
		
		/* UI initialization takes place when the control frame is set */
		gtk_signal_connect (GTK_OBJECT (control), "set_frame",
					GTK_SIGNAL_FUNC (on_control_set_frame), docm);
	
		gtk_signal_connect (GTK_OBJECT (control), "destroy",
					GTK_SIGNAL_FUNC (on_control_destroy), docm);
		
		return BONOBO_OBJECT (control);
	}
}

int
main (int argc, char **argv)
{
	CORBA_ORB orb;

	/* Initialize the i18n support */
	bindtextdomain(PACKAGE, PACKAGE_LOCALE_DIR);
	textdomain(PACKAGE);
	
	/* intialize gnome */
	gnome_init_with_popt_table ("GNOME_Anjuta_DocumentManagerFactory",  "0.0",
				     argc, argv, oaf_popt_options, 0, NULL);

	/* initialize CORBA, OAF  and bonobo */
	orb = oaf_init (argc,argv);
	if (!orb)
		g_error ("initializing orb failed");
	if (!bonobo_init (orb, NULL, NULL))
		g_error ("could not initialize Bonobo");

	factory = bonobo_generic_factory_new (COMPONENT_FACTORY_IID,
					      control_factory,
					      NULL);
	if (factory == NULL)
		g_error ("I could not register the Anjuta Document Manager Factory.");

	bonobo_main ();

	return 0;
}
