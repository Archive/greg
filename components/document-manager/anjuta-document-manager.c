/*
 * anjuta-document-manager.c
 * Copyright (C) 2000  Kh. Naba Kumar Singh
 * 
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free 
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc., 59 
 * Temple Place, Suite 330, Boston, MA  02111-1307  USA 
 */

#include <gnome.h>
#include <libanjuta/libanjuta.h>

#include "anjuta-document-manager.h"

static void anjuta_document_manager_destroy (GtkObject * object);
static void anjuta_document_manager_class_init (AnjutaDocumentManagerClass *
						class);
static void anjuta_document_manager_init (GtkObject * obj);

enum
{
	DOCUMENT_CHANGED,
	LAST_SIGNAL
};

static gint anjuta_document_manager_signals[LAST_SIGNAL];
GtkNotebookClass *parent_class;

/* public routines */
GtkWidget *
anjuta_document_manager_new (Bonobo_UIContainer uic)
{
	AnjutaDocumentManager *docm;
	docm = gtk_type_new (anjuta_document_manager_get_type ());
	docm->uic = uic;
	return GTK_WIDGET (docm);
}

GtkType
anjuta_document_manager_get_type (void)
{
	static GtkType type = 0;

	if (!type)
	{
		GtkTypeInfo info = {
			"AnjutaDocumentManager",
			sizeof (AnjutaDocumentManager),
			sizeof (AnjutaDocumentManagerClass),
			(GtkClassInitFunc) anjuta_document_manager_class_init,
			(GtkObjectInitFunc) anjuta_document_manager_init,
			NULL,
			NULL,
			(GtkClassInitFunc) NULL
		};

		type = gtk_type_unique (gtk_notebook_get_type (), &info);
	}

	return type;
}

static void
anjuta_document_manager_class_init (AnjutaDocumentManagerClass * klass)
{
	GtkObjectClass *object_class = (GtkObjectClass *) klass;
	parent_class = gtk_type_class (gtk_notebook_get_type ());

	object_class->destroy = anjuta_document_manager_destroy;

	anjuta_document_manager_signals[DOCUMENT_CHANGED] =
		gtk_signal_new ("document_changed",
				GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (AnjutaDocumentManagerClass,
						   document_changed),
				gtk_marshal_NONE__POINTER,
				GTK_TYPE_NONE, 1, GTK_TYPE_STRING);

	gtk_object_class_add_signals (object_class,
				      anjuta_document_manager_signals,
				      LAST_SIGNAL);
}

static void
anjuta_document_manager_init (GtkObject * obj)
{
	AnjutaDocumentManager *docm = ANJUTA_DOCUMENT_MANAGER (obj);

	gtk_notebook_set_scrollable (GTK_NOTEBOOK (docm), TRUE);
}

static void
anjuta_document_manager_destroy (GtkObject * obj)
{
	//AnjutaDocumentManager *docm = ANJUTA_DOCUMENT_MANAGER (obj);
	GTK_OBJECT_CLASS (parent_class)->destroy (obj);
}

GtkWidget*
anjuta_document_manager_add_new_doc (AnjutaDocumentManager * docm)
{
	GtkWidget* doc;
	GtkWidget* label;
	
	g_return_val_if_fail (docm != NULL, NULL);
	g_return_val_if_fail (ANJUTA_IS_DOCUMENT_MANAGER(docm), NULL);
	g_return_val_if_fail (docm->uic != NULL, NULL);
	
	printf ("New doc about to be created");
	doc = anjuta_doc_new_with_mime (docm->uic, "text/plain");
	printf ("New doc created");
	gtk_widget_show(doc);
	
	label = gtk_label_new ("Newfile");
	gtk_widget_show(label);
	
	gtk_notebook_append_page (GTK_NOTEBOOK(docm), doc, label);
	
	return doc;
}

void
anjuta_document_manager_remove_current_doc (AnjutaDocumentManager * docm)
{
	AnjutaDoc *doc = anjuta_document_manager_get_current_doc (docm);
	anjuta_document_manager_remove_doc (docm, doc);
}

void
anjuta_document_manager_remove_doc (AnjutaDocumentManager * docm,
				    AnjutaDoc * doc)
{
#if 0
	GtkWidget *submenu;
	GtkWidget *wid;

	gint nb_cur_page_num;

	if (te == NULL)
		return;

	gtk_signal_disconnect_by_func (GTK_OBJECT (app->widgets.notebook),
				       GTK_SIGNAL_FUNC
				       (on_notebook_switch_page), NULL);
	// anjuta_clear_windows_menu ();

	if (doc->full_filename != NULL)
	{
		gint max_recent_files;

		max_recent_files =
			preferences_get_int (app->preferences,
					     MAXIMUM_RECENT_FILES);
		app->recent_files =
			update_string_list (app->recent_files,
					    te->full_filename,
					    max_recent_files);
		submenu =
			create_submenu (_("Recent Files "), app->recent_files,
					GTK_SIGNAL_FUNC
					(on_recent_files_menu_item_activate));
		gtk_menu_item_set_submenu (GTK_MENU_ITEM
					   (app->widgets.menubar.
					    file.recent_files), submenu);
	}
	breakpoints_dbase_clear_all_in_editor (debugger.breakpoints_dbase,
					       te);
	switch (doc->mode)
	{
	case ANJUTA_DOCUMENT_PAGED:
		wid = te->widgets.client->parent;
		nb_cur_page_num =
			gtk_notebook_page_num (GTK_NOTEBOOK
					       (app->widgets.notebook), wid);
		if (GTK_IS_CONTAINER (wid))
			gtk_container_remove (GTK_CONTAINER (wid),
					      te->widgets.client);
		gtk_notebook_remove_page (GTK_NOTEBOOK
					  (app->widgets.notebook),
					  nb_cur_page_num);
		gtk_container_add (GTK_CONTAINER (te->widgets.client_area),
				   te->widgets.client);
		app->text_editor_list =
			g_list_remove (app->text_editor_list, te);
		/* This is called to set the next active document */
		if (GTK_NOTEBOOK (app->widgets.notebook)->children == NULL)
		{
			anjuta_set_current_text_editor (NULL);
		}
		else
		{
			on_anjuta_window_focus_in_event (NULL, NULL, NULL);
		}
		break;

	case TEXT_EDITOR_WINDOWED:
		app->text_editor_list =
			g_list_remove (app->text_editor_list, te);
		break;
	}
	if (te->full_filename)
	{
		if (app->project_dbase->project_is_open == FALSE)
			tags_manager_remove (app->tags_manager,
					     te->full_filename);
	}
	anjuta_fill_windows_menu ();
	text_editor_destroy (te);
	gtk_signal_connect (GTK_OBJECT (app->widgets.notebook), "switch_page",
			    GTK_SIGNAL_FUNC (on_anjuta_notebook_switch_page),
			    NULL);
#endif
}

AnjutaDoc*
anjuta_document_manager_get_current_doc (AnjutaDocumentManager * docm)
{
	g_return_val_if_fail (docm != NULL, NULL);
	return docm->current_document;
}

void
anjuta_document_manager_set_current_doc (AnjutaDocumentManager * docm)
{
#if 0
	app->current_text_editor = te;
	main_toolbar_update ();
	extended_toolbar_update ();
	format_toolbar_update ();
	browser_toolbar_update ();
	anjuta_update_title ();
	anjuta_update_app_status (FALSE, NULL);
	if (te == NULL)
	{
		chdir (app->dirs->home);
		return;
	}
	if (te->full_filename)
	{
		gchar *dir;
		dir = g_dirname (te->full_filename);
		chdir (dir);
		g_free (dir);
		return;
	}
	return;
#endif
}

#if 0

TextEditor *
anjuta_get_notebook_text_editor (gint page_num)
{
	GtkWidget *page;
	TextEditor *te;

	page =
		gtk_notebook_get_nth_page (GTK_NOTEBOOK
					   (app->widgets.notebook), page_num);
	te = gtk_object_get_data (GTK_OBJECT (page), "TextEditor");
	return te;
}

GtkWidget *
anjuta_get_current_text ()
{
	TextEditor *t = anjuta_get_current_text_editor ();
	if (t)
		return t->widgets.editor;
	return NULL;
}

gchar *
anjuta_get_current_selection ()
{
	gchar *chars;
	TextEditor *te;

	te = anjuta_get_current_text_editor ();
	if (te == NULL)
		return NULL;

	chars = text_editor_get_selection (te);
	return chars;
}

void
anjuta_goto_file_line (gchar * fname, guint lineno)
{
	anjuta_goto_file_line_mark (fname, lineno, TRUE);
}

void
anjuta_goto_file_line_mark (gchar * fname, guint lineno, gboolean mark)
{
	gchar *fn;
	GList *node;

	TextEditor *te;

	g_return_if_fail (fname != NULL);
	fn = anjuta_get_full_filename (fname);
	g_return_if_fail (fname != NULL);
	node = app->text_editor_list;
	while (node)
	{
		te = (TextEditor *) node->data;
		if (te->full_filename == NULL)
		{
			node = g_list_next (node);
			continue;
		}
		if (strcmp (fn, te->full_filename) == 0)
		{
			if (lineno >= 0)
				text_editor_goto_line (te, lineno, mark);
			anjuta_show_text_editor (te);
			anjuta_grab_text_focus ();
			g_free (fn);
			return;
		}
		node = g_list_next (node);
	}
	te = anjuta_append_text_editor (fn);
	if (lineno >= 0 && te)
		text_editor_goto_line (te, lineno, mark);
	g_free (fn);
	return;
}

static void
anjuta_show_text_editor (TextEditor * te)
{
	GtkWidget *page;
	GList *node;
	gint i;

	if (te == NULL)
		return;
	if (te->mode == TEXT_EDITOR_WINDOWED)
	{
		gdk_window_raise (te->widgets.window->window);
		anjuta_set_current_text_editor (te);
		return;
	}
	node = GTK_NOTEBOOK (app->widgets.notebook)->children;
	i = 0;
	while (node)
	{
		TextEditor *t;

		page =
			gtk_notebook_get_nth_page (GTK_NOTEBOOK
						   (app->widgets.notebook),
						   i);
		t = gtk_object_get_data (GTK_OBJECT (page), "TextEditor");
		if (t == te && GTK_IS_EVENT_BOX (page) && t)
		{
			gint page_num;
			page_num =
				gtk_notebook_page_num (GTK_NOTEBOOK
						       (app->
							widgets.notebook),
						       page);
			gtk_notebook_set_page (GTK_NOTEBOOK
					       (app->widgets.notebook),
					       page_num);
			anjuta_set_current_text_editor (te);
			return;
		}
		i++;
		node = g_list_next (node);
	}
}

void
anjuta_set_file_properties (gchar * file)
{
	gchar *full_filename, *dir, *filename, *ext;
	PropsID props;

	g_return_if_fail (file != NULL);
	full_filename = g_strdup (file);
	props = app->preferences->props;
	dir = g_dirname (full_filename);
	filename = extract_filename (full_filename);
	ext = get_file_extension (filename);
	prop_set_with_key (props, CURRENT_FULL_FILENAME, "");
	prop_set_with_key (props, CURRENT_FILE_DIRECTORY, "");
	prop_set_with_key (props, CURRENT_FILENAME_WITH_EXT, "");
	prop_set_with_key (props, CURRENT_FILE_EXTENSION, "");

	if (full_filename)
		prop_set_with_key (props, CURRENT_FULL_FILENAME_WITH_EXT,
				   full_filename);
	if (dir)
		prop_set_with_key (props, CURRENT_FILE_DIRECTORY, dir);
	if (filename)
		prop_set_with_key (props, CURRENT_FILENAME_WITH_EXT,
				   filename);
	if (ext)
	{
		prop_set_with_key (props, CURRENT_FILE_EXTENSION, ext);
		*(--ext) = '\0';
		/* Ext has been removed */
	}
	if (filename)
		prop_set_with_key (props, CURRENT_FILENAME, filename);
	if (full_filename)
		prop_set_with_key (props, CURRENT_FULL_FILENAME,
				   full_filename);
	if (dir)
		g_free (dir);
	if (full_filename)
		g_free (full_filename);
}

void
anjuta_open_file (gchar * fname)
{
	Preferences *pref;
	pid_t pid;
	gchar *cmd;
	GList *list;

	g_return_if_fail (fname != NULL);
	pref = app->preferences;
	anjuta_set_file_properties (fname);
	cmd =
		command_editor_get_command_file (app->command_editor,
						 COMMAND_OPEN_FILE, fname);
	list = glist_from_string (cmd);
	g_free (cmd);
	if (list == NULL)
	{
		anjuta_goto_file_line (fname, -1);
		return;
	}
	if (anjuta_is_installed ((gchar *) list->data, TRUE))
	{
		gchar **av, *dir;
		guint length;
		gint i;

		length = g_list_length (list);
		av = g_malloc (sizeof (gchar *) * length);
		for (i = 0; i < length; i++)
		{
			av[i] = g_list_nth_data (list, i);
		}
		dir =
			prop_get_new_expand (pref->props,
					     CURRENT_FILE_DIRECTORY,
					     app->dirs->home);
		chdir (dir);
		pid = gnome_execute_async (dir, length, av);
		g_free (dir);
		glist_strings_free (list);
		return;
	}
	anjuta_goto_file_line (fname, -1);
}

void
anjuta_view_file (gchar * fname)
{
	Preferences *pref;
	pid_t pid;
	gchar *cmd;
	GList *list;

	g_return_if_fail (fname != NULL);
	pref = app->preferences;
	anjuta_set_file_properties (fname);
	cmd =
		command_editor_get_command_file (app->command_editor,
						 COMMAND_VIEW_FILE, fname);
	list = glist_from_string (cmd);
	g_free (cmd);
	if (list == NULL)
	{
		anjuta_goto_file_line (fname, -1);
		return;
	}
	if (anjuta_is_installed ((gchar *) list->data, TRUE))
	{
		gchar **av, *dir;
		guint length;
		gint i;

		length = g_list_length (list);
		av = g_malloc (sizeof (gchar *) * length);
		for (i = 0; i < length; i++)
		{
			av[i] = g_list_nth_data (list, i);
		}
		dir =
			prop_get_new_expand (pref->props,
					     CURRENT_FILE_DIRECTORY,
					     app->dirs->home);
		pid = gnome_execute_async (dir, length, av);
		g_free (dir);
		glist_strings_free (list);
		return;
	}
	anjuta_goto_file_line (fname, -1);
}

gchar *
anjuta_get_full_filename (gchar * fn)
{
	gchar *cur_dir, *dummy;
	GList *list;
	gchar *text;
	gint i;
	if (!fn)
		return NULL;
	if (fn[0] == '/')
		return g_strdup (fn);

	if (app->execution_dir)
	{
		dummy = g_strconcat (app->execution_dir, "/", fn, NULL);
		if (file_is_regular (dummy) == TRUE)
			return dummy;
		g_free (dummy);
	}
	cur_dir = g_get_current_dir ();
	if (app->project_dbase->project_is_open)
	{
		gchar *src_dir;

		src_dir =
			project_dbase_get_module_dir (app->project_dbase,
						      MODULE_SOURCE);
		dummy = g_strconcat (src_dir, "/", fn, NULL);
		g_free (src_dir);
		if (file_is_regular (dummy) == TRUE)
		{
			g_free (cur_dir);
			return dummy;
		}
		g_free (dummy);
	}
	else
	{
		dummy = g_strconcat (cur_dir, "/", fn, NULL);
		if (file_is_regular (dummy) == TRUE)
		{
			g_free (cur_dir);
			return dummy;
		}
		g_free (dummy);
	}

	list = GTK_CLIST (app->src_paths->widgets.src_clist)->row_list;
	for (i = 0; i < g_list_length (list); i++)
	{
		gtk_clist_get_text (GTK_CLIST
				    (app->src_paths->widgets.src_clist), i, 0,
				    &text);
		if (app->project_dbase->project_is_open)
			dummy =
				g_strconcat (app->project_dbase->top_proj_dir,
					     "/", text, "/", fn, NULL);
		else
			dummy =
				g_strconcat (cur_dir, "/", text, "/", fn,
					     NULL);
		if (file_is_regular (dummy) == TRUE)
		{
			g_free (cur_dir);
			return dummy;
		}
		g_free (dummy);
	}
	dummy = g_strconcat (cur_dir, "/", fn, NULL);

	g_free (cur_dir);
	return dummy;
}

gint
anjuta_get_file_property (gchar * key, gchar * filename, gint default_value)
{
	gint value;
	gchar *value_str;

	g_return_val_if_fail (filename != NULL, default_value);
	g_return_val_if_fail (strlen (filename) != 0, default_value);

	value_str =
		prop_get_new_expand (app->preferences->props, key, filename);
	if (!value_str)
		return default_value;
	value = atoi (value_str);
	return value;
}

void
anjuta_grab_text_focus ()
{
	GtkWidget *text;
	text = GTK_WIDGET (anjuta_get_current_text ());
	g_return_if_fail (text != NULL);
	scintilla_send_message (SCINTILLA (text), SCI_GRABFOCUS, 0, 0);
}

void
anjuta_delete_all_marker (gint marker)
{
	GList *node;
	node = app->text_editor_list;
	while (node)
	{
		TextEditor *te;
		te = node->data;
		scintilla_send_message (SCINTILLA (te->widgets.editor),
					SCI_MARKERDELETEALL, marker, 0);
		node = g_list_next (node);
	}
}

#endif
