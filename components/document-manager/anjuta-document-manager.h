/*
 * anjuta-document-manager.h
 * Copyright (C) 2000  Kh. Naba Kumar Singh
 * 
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free 
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc., 59 
 * Temple Place, Suite 330, Boston, MA  02111-1307  USA 
 */

#ifndef _ANJUTA_DOCUMENT_MANAGER_H_
#define _ANJUTA_DOCUMENT_MANAGER_H_

#include <gnome.h>
#include "anjuta-doc.h"
#include "notebook.h"

BEGIN_GNOME_DECLS typedef struct _AnjutaDocumentManager AnjutaDocumentManager;
typedef struct _AnjutaDocumentManagerClass AnjutaDocumentManagerClass;
typedef struct _AnjutaDocumentManagerPriv AnjutaDocumentManagerPriv;

#define ANJUTA_DOCUMENT_MANAGER_TYPE        (anjuta_document_manager_get_type ())
#define ANJUTA_DOCUMENT_MANAGER(o)          (GTK_CHECK_CAST ((o), ANJUTA_DOCUMENT_MANAGER_TYPE, AnjutaDocumentManager))
#define ANJUTA_DOCUMENT_MANAGER_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), ANJUTA_DOCUMENT_MANAGER_TYPE, AnjutaDocumentManagerClass))
#define ANJUTA_IS_DOCUMENT_MANAGER(o)       (GTK_CHECK_TYPE ((o), ANJUTA_DOCUMENT_MANAGER_TYPE))
#define ANJUTA_IS_DOCUMENT_MANAGER_CLASS(k) (GTK_CHECK_CLASS_TYPE ((k), ANJUTA_DOCUMENT_MANAGER_TYPE))

struct _AnjutaDocumentManager
{
	GtkNotebook notebook;
	Bonobo_UIContainer uic;

	GList *documents;
	AnjutaDoc *current_document;

	gboolean autosave;
	gint tabsize;

	AnjutaDocumentManagerPriv *priv;
};

struct _AnjutaDocumentManagerClass
{
	GtkNotebookClass klass;
	void (*document_changed) (AnjutaDocumentManager * docman,
				  AnjutaDoc * document);
};

GtkWidget *anjuta_document_manager_new (Bonobo_UIContainer uic);

GtkType anjuta_document_manager_get_type (void);

GtkWidget *anjuta_document_manager_add_new_doc (
						AnjutaDocumentManager *dom);

GtkWidget *anjuta_document_manager_goto_file_line (
						AnjutaDocumentManager *docm,
						gchar * filename,
						gint line);

GtkWidget *anjuta_document_manager_goto_file_line_mark (
						AnjutaDocumentManager *docm,
						gchar * filename,
						gint line);

void
anjuta_document_manager_remove_current_doc (AnjutaDocumentManager * docm);

void
anjuta_document_manager_remove_doc (AnjutaDocumentManager * docm,
				    AnjutaDoc * doc);

AnjutaDoc *anjuta_document_manager_get_current_doc (AnjutaDocumentManager *
						    docm);

void anjuta_document_manager_set_current_doc (AnjutaDocumentManager * docm);

#endif /* _ANJUTA_DOCUMENT_MANAGER_H_ */
