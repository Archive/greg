/*
 * test-anjuta-docman.h
 * Copyright (C) 2000  Kh. Naba Kumar Singh
 * 
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free 
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc., 59 
 * Temple Place, Suite 330, Boston, MA  02111-1307  USA 
 */

#include <gnome.h>
#include <bonobo.h>
#include <libanjuta/libanjuta.h>

#include "anjuta-doc.h"

#define ANJUTA_DOCUMENT_MANAGER_CONTROL_IID \
	"OAFIID:GNOME_Anjuta_DocumentManager_Control"

static void
exit_cb (GtkWidget *widget,
	 gpointer data)
{
	gtk_main_quit ();
}

static guint
add_doc(gpointer data)
{
	CORBA_Environment ev;
	GNOME_Anjuta_DocumentManager docman;
	BonoboWidget* control;

	printf ("Trying to add a document with data: 1\n");
	
	control = data;
	g_return_val_if_fail (control != NULL, FALSE);
	g_return_val_if_fail (BONOBO_IS_WIDGET(control), FALSE);
	printf ("Trying to add a document with data: 2\n");
	
	CORBA_exception_init (&ev);
	
	/* Get the docman interface */
	docman = (GNOME_Anjuta_DocumentManager) bonobo_object_client_query_interface
		(bonobo_widget_get_server (control), "IDL:GNOME/Anjuta/DocumentManager:1.0", &ev);

	g_return_val_if_fail (docman != CORBA_OBJECT_NIL, FALSE);
	GNOME_Anjuta_DocumentManager_addNewDoc (docman, &ev);
	//GNOME_Anjuta_DocumentManager_gotoFileLine (docman, "test_anjuta-docman.c", 0, &ev);
	
	/* Release the doc man interface */
	bonobo_object_release_unref (docman, &ev);
	
	CORBA_exception_free (&ev);
	return FALSE;
}

static BonoboUIVerb verbs [] = {
	BONOBO_UI_UNSAFE_VERB ("FileExit", exit_cb),
	BONOBO_UI_VERB_END
};

static guint
create_container (void)
{
	GtkWidget *win;
	GtkWindow *window;
	GtkWidget *control;
	
	BonoboUIComponent *component;
	BonoboUIContainer *container;
	Bonobo_UIContainer ui_container;

	win = bonobo_window_new ("test-anjuta-document-manager",
				 "Anjuta Document Manager Test");
	window = GTK_WINDOW (win);
	gtk_window_set_default_size (window, 600, 440);
	gtk_window_set_policy (window, TRUE, TRUE, FALSE);
	gtk_signal_connect (GTK_OBJECT (win), "delete_event", exit_cb, NULL);


	container = bonobo_ui_container_new ();
	bonobo_ui_container_set_win (container, BONOBO_WINDOW (win));
	ui_container = bonobo_object_corba_objref (BONOBO_OBJECT (container));
	
	component = bonobo_ui_component_new ("test-anjuta-document-manager");
	bonobo_ui_component_set_container (component, ui_container);
    
	control = bonobo_widget_new_control (
		ANJUTA_DOCUMENT_MANAGER_CONTROL_IID,
		bonobo_object_corba_objref (BONOBO_OBJECT (container)));
	
	//bonobo_ui_component_add_verb_list_with_data (component, verbs, control);
	//bonobo_ui_component_set_translate (component, "/", ui, NULL);
	//bonobo_ui_util_set_ui (component, GNOME_DATADIR,
	//	"anjuta-document-manager.xml", "anjua-document-manager");

	if (control == NULL)
		g_error ("Cannot get `%s'.", ANJUTA_DOCUMENT_MANAGER_CONTROL_IID);

	bonobo_window_set_contents (BONOBO_WINDOW (win), control);
	gtk_widget_show_all (GTK_WIDGET (window));
	
	//add_doc (NULL, control);
	gtk_timeout_add (2000, (GtkFunction) add_doc, control);

	return FALSE;
}

int
main (int argc, char **argv)
{
	CORBA_ORB orb;
	
	/* intialize gnome */
	gnome_init_with_popt_table ("test-anjuta-docman",  "0.0",
				     argc, argv, oaf_popt_options, 0, NULL);

	/* initialize CORBA, OAF  and bonobo */
	orb = oaf_init (argc,argv);
	if (!orb)
		g_error ("initializing orb failed");
	if (!bonobo_init (orb, NULL, NULL))
		g_error ("could not initialize Bonobo");

	gtk_idle_add ((GtkFunction) create_container, NULL);

	bonobo_main ();

	return 0;
}
