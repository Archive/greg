/*
 * anjuta-doc-cobject.c
 * Copyright (C) 2000  Kh. Naba Kumar Singh
 * 
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free 
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc., 59 
 * Temple Place, Suite 330, Boston, MA  02111-1307  USA 
 */

#include <libanjuta/libanjuta.h>
#include "anjuta-doc-cobject.h"
#include "anjuta-doc.h"

static BonoboObjectClass *parent_class;
static POA_GNOME_Anjuta_Doc__epv ad_epv;
static POA_GNOME_Anjuta_Doc__vepv ad_vepv;

/* Function prototypes */
static void anjuta_doc_cobject_init(AnjutaDocCobject* cobj);
static void anjuta_doc_cobject_class_init(AnjutaDocCobjectClass* cobj_class);
static void anjuta_doc_cobject_destroy(AnjutaDocCobject* cobj);

/* Public Functions */
GtkType
anjuta_doc_cobject_get_type ()
{
	static GtkType type = 0;

	if (!type)
	{
		GtkTypeInfo info = {
			"AnjutaDocCobject",
			sizeof (AnjutaDocCobject),
			sizeof (AnjutaDocCobjectClass),
			(GtkClassInitFunc) anjuta_doc_cobject_class_init,
			(GtkObjectInitFunc) anjuta_doc_cobject_init,
			NULL,
			NULL,
			(GtkClassInitFunc) NULL
		};

		type = gtk_type_unique (bonobo_object_get_type(), &info);
	}
	return type;
}

/* Corba object creation function */
AnjutaDocCobject*
anjuta_doc_cobject_new (AnjutaDoc* doc)
{
	POA_GNOME_Anjuta_Doc *servant;
	AnjutaDocCobject *doc_obj;
	GNOME_Anjuta_Doc objref;
	CORBA_Environment ev;

	g_return_val_if_fail (doc != NULL, NULL);
	g_return_val_if_fail (ANJUTA_IS_DOC(doc), NULL);
	
	doc_obj = gtk_type_new (anjuta_doc_cobject_get_type ());
	
	servant = (POA_GNOME_Anjuta_Doc*) g_new0(BonoboObjectServant, 1);
	servant->vepv = &adm_vepv;
	
	CORBA_exception_init (&ev);
	POA_GNOME_Anjuta_Doc__init((PortableServer_Servant) servant, &ev);
	
	if (ev._major != CORBA_NO_EXCEPTION) {
		g_free (servant);
		CORBA_exception_free (&ev);
		gtk_object_destroy (GTK_OBJECT (doc_obj));
		return NULL;
	}
	
	CORBA_exception_free (&ev);
	objref = bonobo_object_activate_servant (BONOBO_OBJECT(doc_obj), servant);

	gtk_object_ref (GTK_OBJECT (doc));
	doc_obj->doc = doc;
	
	bonobo_object_construct (BONOBO_OBJECT (doc_obj), objref);
	
	return doc_obj;
}

/* Private Functions */
static void
anjuta_doc_cobject_init(AnjutaDocCobject* cobj)
{
	/* Doesn't seem to have anything to do right now */
}

static void 
anjuta_doc_cobject_destroy(AnjutaDocCobject* cobj)
{
	g_return_if_fail (cobj != NULL);
	g_return_if_fail (ANJUTA_IS_DOC_COBJECT(cobj));
	
	gtk_object_unref (cobj->doc);
}

static void
impl_goto_line (PortableServer_Servant servant, 
		CORBA_int line,
		CORBA_Environment *ev)
{
	AnjutaDocCobject *cobj;
	
	cobj = ANJUTA_DOC_COBJECT(bonobo_object_from_servant (servant));
	anjuta_doc_goto_line (ANJUTA_DOC(cobj->doc), line);
}

static void
impl_goto_file_line_mark (PortableServer_Servant servant, 
		const CORBA_char *filename, CORBA_int line,
		CORBA_Environment *ev)
{
	AnjutaDocCobject *cobj;
	
	cobj = ANJUTA_DOC_COBJECT(bonobo_object_from_servant (servant));
	anjuta_doc_goto_line_mark (ANJUTA_DOC(cobj->doc), line);
}

static CORBA_char*
impl_get_filename (PortableServer_Servant servant, 
		CORBA_Environment *ev)
{
	AnjutaDocCobject *cobj;
	
	cobj = ANJUTA_DOC_COBJECT(bonobo_object_from_servant (servant));
	return (CORBA_char*) anjuta_doc_get_filename (ANJUTA_DOC(cobj->doc));
}

static CORBA_char*
impl_get_mime_type (PortableServer_Servant servant, 
		CORBA_Environment *ev)
{
	AnjutaDocCobject *cobj;
	
	cobj = ANJUTA_DOC_COBJECT(bonobo_object_from_servant (servant));
	return (CORBA_char*) anjuta_doc_get_mime_type (ANJUTA_DOC(cobj->doc));
}

static CORBA_boolean
impl_is_saved (PortableServer_Servant servant, 
		CORBA_Environment *ev)
{
	AnjutaDocCobject *cobj;
	
	cobj = ANJUTA_DOC_COBJECT(bonobo_object_from_servant (servant));
	return (CORBA_boolean) anjuta_doc_is_saved (ANJUTA_DOC(cobj->doc));
}

static void
impl_set_file (PortableServer_Servant servant, 
		const CORBA_char* filename,
		CORBA_Environment *ev)
{
	AnjutaDocCobject *cobj;
	
	cobj = ANJUTA_DOC_COBJECT(bonobo_object_from_servant (servant));
	anjuta_doc_set_filename (ANJUTA_DOC(cobj->doc), (gchar*)filename);
}

static CORBA_Bonobo_control
impl_get_handler_control (PortableServer_Servant servant, 
		CORBA_Environment *ev)
{
	AnjutaDocCobject *cobj;
	
	cobj = ANJUTA_DOC_COBJECT(bonobo_object_from_servant (servant));
	return anjuta_doc_get_handler_control (ANJUTA_DOC(cobj->doc));
}

static CORBA_Bonobo_Unknown
impl_get_handler_interface (PortableServer_Servant servant, 
		CORBA_Environment *ev)
{
	AnjutaDocCobject *cobj;
	
	cobj = ANJUTA_DOC_COBJECT(bonobo_object_from_servant (servant));
	return anjuta_doc_get_handler_interface (ANJUTA_DOC(cobj->doc));
}

static void
anjuta_doc_cobject_class_init(AnjutaDocCobjectClass* cobj_class)
{
	GtkObjectClass *object_class = (GtkObjectClass*) cobj_class;
	parent_class = gtk_type_class (bonobo_object_get_type ());
    
	object_class->destroy = anjuta_doc_cobject_destroy;
    	
	/* EPV initialization */
	ad_epv.gotoFileLine = impl_goto_line;
	ad_epv.gotoFileLineMark = impl_goto_line_mark;
	ad_epv.getFilename = impl_get_filename;
	ad_epv.getMimeType = impl_get_mime_type;
	ad->epv.setFile = impl_set_file;
	ad->epv.getHandlerControl = impl_get_handler_control;
	ad->epv.getHandlerInterface = impl_get_handler_interface;

	/* VEPV initialization */
	ad_vepv.Bonobo_Unknown_epv = bonobo_object_get_epv ();
	ad_vepv.GNOME_Anjuta_Doc_epv = &adm_epv;
}
