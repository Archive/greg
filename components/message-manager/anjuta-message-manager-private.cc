/*
    anjuta-message-manager-private.cc
    Copyright (C) 2000, 2001  Kh. Naba Kumar Singh, Johannes Schmid

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include "anjuta-message-manager-private.h"
#include "libanjuta/libanjuta.h"

AnjutaMessageWindow::AnjutaMessageWindow(AnjutaMessageManager* amm, string type, GdkPixmap* pixmap)
{
	msg_list = gtk_clist_new(1);
	scrolled_win = gtk_scrolled_window_new(NULL, NULL);
	gtk_widget_ref(msg_list);
	gtk_widget_ref(scrolled_win);
	gtk_widget_show(msg_list);
	gtk_widget_show(scrolled_win);
	
	gtk_container_add(GTK_CONTAINER(scrolled_win), msg_list);
	GtkWidget* label = gtk_label_new(type.c_str());
	gtk_widget_ref(label);
	gtk_widget_show(label);
	gtk_notebook_append_page(GTK_NOTEBOOK(amm->intern->notebook), scrolled_win, label);
	
	m_cur_line = 0;
	m_type_name = type;
	m_parent = amm;
	amm->intern->last_page++;
	m_page_num = amm->intern->last_page;
	amm->intern->msg_windows.push_back(this);
	add_button_to_dock(amm, this, pixmap);
}

const string& AnjutaMessageWindow::get_type() const
{
	return m_type_name;
}

const vector<string>& AnjutaMessageWindow::get_messages() const
{
	return m_messages;
}
	
void AnjutaMessageWindow::add_to_buffer(char c)
{
	m_line_buffer += c;
}

void AnjutaMessageWindow::append_buffer()
{
	gtk_clist_freeze (GTK_CLIST(msg_list));

	GtkAdjustment* adj = gtk_scrolled_window_get_vadjustment (GTK_SCROLLED_WINDOW
						     (scrolled_win));

	/* If the scrollbar is almost at the end, */
	/* then only we scroll to the end */
	bool update_adj = false;
	if (adj->value > 0.95 * (adj->upper-adj->page_size) || adj->value == 0)
		update_adj = true;
	
	string message = m_line_buffer;
	m_line_buffer = string();
	char * msg = new char[message.length() + 1];
	strcpy(msg, message.c_str());
	gtk_clist_append(GTK_CLIST(msg_list), &msg);
	m_messages.push_back(message);
	/*
	int dummy_int;
	char* dummy_fn;
	if (parse_error_line(message.c_str(), dummy_fn, dummy_int))
	{
		gtk_clist_set_foreground(GTK_CLIST(msg_list), m_messages.size() - 1, m_parent->intern->color_red);
	}
	delete dummy_fn
	*/
	delete msg;
	gtk_clist_thaw(GTK_CLIST(msg_list));
	if (update_adj) 
		gtk_adjustment_set_value (adj, adj->upper - adj->page_size);
}
		

int AnjutaMessageWindow::get_page() const
{
	return m_page_num;
}

void AnjutaMessageWindow::set_cur_line(int line)
{
	m_cur_line = line;
}

int AnjutaMessageWindow::get_cur_line() const
{
	return m_cur_line;
}
		
void AnjutaMessageWindow::activate()
{
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(button), true);
	gtk_notebook_set_page(GTK_NOTEBOOK(m_parent->intern->notebook), m_page_num);
	m_parent->intern->cur_msg_win = this;
}

void AnjutaMessageWindow::deactivate()
{
	gtk_signal_disconnect_by_func (GTK_OBJECT (button),
					       GTK_SIGNAL_FUNC(on_dock_button_clicked), m_parent);
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(button), false);
	gtk_signal_connect (GTK_OBJECT (button), "clicked",
				    GTK_SIGNAL_FUNC (on_dock_button_clicked), m_parent);
}
	
AnjutaMessageManager* AnjutaMessageWindow::get_parent() const
{
	return m_parent;
}