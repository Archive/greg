/*
    anjuta-message-manager-private.h
    Copyright (C) 2000, 2001  Kh. Naba Kumar Singh, Johannes Schmid

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef _ANJUTA_MESSAGE_MANAGER_PRIVATE
#define _ANJUTA_MESSAGE_MANAGER_PRIVATE

#include <string>
#include <vector>

#include "anjuta-message-manager.h"

using std::vector;
using std::string;

class AnjutaMessageWindow;

struct AnjutaMessageManagerPrivate
{
	vector<AnjutaMessageWindow*> msg_windows;
	AnjutaMessageWindow* cur_msg_win;
	
	gint last_page;
	
	GtkWidget* box;
	GtkWidget* eventbox;
	GtkWidget* dock;
	GtkWidget* dock_item;
	GtkWidget* notebook;
	GtkWidget* toolbar;
	
	GdkColor color_red;
	GdkColor color_green;
	GdkColor color_blue;
	GdkColor color_white;
	GdkColor color_black;
};

class AnjutaMessageWindow
{
	public:
		AnjutaMessageWindow(AnjutaMessageManager* amm, string type, GdkPixmap* pixmap); 
		const string& get_type() const;
		const vector<string>& get_messages() const;
	
		void add_to_buffer(char c);
		void append_buffer();
		
		int get_page() const;
		void set_cur_line(int line);
		int get_cur_line() const;
		
		void activate();
		void deactivate(); 
	
		AnjutaMessageManager* get_parent() const;
	
		GtkWidget* frame;
		GtkWidget* button;
		GtkWidget* msg_list;
	private:
		AnjutaMessageManager* m_parent;
	
		string m_type_name;
		vector<string> m_messages;
		string m_line_buffer;
		gint m_cur_line;
		gint m_page_num;
	
		GtkWidget* scrolled_win;
};

void add_button_to_dock(AnjutaMessageManager* amm, AnjutaMessageWindow* msg_win, GdkPixmap* pixmap);
void remove_button_from_dock(AnjutaMessageManager* amm, AnjutaMessageWindow* msg_win);


#endif