/*
    anjuta-message-manager.cc
    Copyright (C) 2000, 2001  Kh. Naba Kumar Singh, Johannes Schmid

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include "anjuta-message-manager.h"
#include "anjuta-message-manager-private.h"

// SINGALS

enum {
	MESSAGE_CLICKED,
	SIGNALS_END
};
static guint anjuta_message_manager_signals [SIGNALS_END] = { 0 };

typedef void (*GtkSignal_NONE__STRING_STRING)(
			GtkObject*, gchar*, gchar* 
);

void gtk_marshal_NONE__STRING_STRING(GtkObject*    object,
					   GtkSignalFunc func,
					   gpointer      func_data,
                       GtkArg*       args)
{
	GtkSignal_NONE__STRING_STRING rfunc;
	rfunc = (GtkSignal_NONE__STRING_STRING)func;
	(*rfunc)(object, GTK_VALUE_STRING(args[0]), GTK_VALUE_STRING(args[1]));
}

GtkWindowClass *parent_class;

GtkWidget *
anjuta_message_manager_new ()
{
	AnjutaMessageManager *amm;
	amm = reinterpret_cast<AnjutaMessageManager*>(gtk_type_new (anjuta_message_manager_get_type ()));
	return GTK_WIDGET (amm);
}

GtkType
anjuta_message_manager_get_type (void)
{
	static GtkType type = 0;

	if (!type)
	{
#ifdef GTK2
		static const GTypeInfo info = {
			sizeof (AnjutaMessageManagerClass),
			(GInitFunc) anjuta_message_manager_init,
			NULL,           /* base_finalize */
			(GClassInitFunc) anjuta_message_manager_class_init,
			NULL,           /* class_finalize */
			NULL,           /* class_data */
			sizeof (AnjutaMessageManager),
			16,             /* n_preallocs */
			(GInstanceInitFunc) NULL,
		};
		type = g_type_register_static (gtk_window_get_type (), "GtkTest", &info, 0);
#else
		static const GtkTypeInfo info = {
			"AnjutaMessageManager",
			sizeof (AnjutaMessageManager),
			sizeof (AnjutaMessageManagerClass),
			(GtkClassInitFunc) anjuta_message_manager_class_init,
			(GtkObjectInitFunc) anjuta_message_manager_init,
			NULL,
			NULL,
			(GtkClassInitFunc) NULL
		};
		type = gtk_type_unique (gtk_window_get_type (), &info);
#endif
	}
	return type;
}

static void
anjuta_message_manager_class_init (AnjutaMessageManagerClass * klass)
{
	GtkObjectClass *object_class = reinterpret_cast<GtkObjectClass*>(klass);
	parent_class = reinterpret_cast<GtkWindowClass*>(gtk_type_class (gtk_window_get_type ()));

	// Signals
	anjuta_message_manager_signals[MESSAGE_CLICKED] =
		gtk_signal_new ("message_clicked", GTK_RUN_FIRST,
				 object_class -> type,
				 GTK_SIGNAL_OFFSET (AnjutaMessageManagerClass, message_clicked),
				 gtk_marshal_NONE__STRING_STRING,
				 GTK_TYPE_NONE, 2, GTK_TYPE_STRING, GTK_TYPE_STRING);
#define GTK12 // Delete this if using GTK 2.0 (not tested)
#ifdef GTK12
	gtk_object_class_add_signals (object_class, anjuta_message_manager_signals,
				       SIGNALS_END);
#endif
	
	object_class->destroy = anjuta_message_manager_destroy;
}

static void
anjuta_message_manager_init (GtkObject * obj)
{
	// Init Class
	AnjutaMessageManager *amm = ANJUTA_MESSAGE_MANAGER (obj);
	amm->intern = new AnjutaMessageManagerPrivate;
	amm->intern->last_page = -1;
	amm->intern->cur_msg_win = 0;
	
	// Colors
	amm->intern->color_red.pixel = 16;
	amm->intern->color_red.red = (guint16) - 1;
	amm->intern->color_red.green = 0;
	amm->intern->color_red.blue = 0;
	amm->intern->color_green.pixel = 16;
	amm->intern->color_green.red = 0;
	amm->intern->color_green.green = (guint16) - 1;
	amm->intern->color_green.blue = 0;
	amm->intern->color_blue.pixel = 16;
	amm->intern->color_blue.red = 0;
	amm->intern->color_blue.green = 0;
	amm->intern->color_blue.blue = (guint16) - 1;
	amm->intern->color_black.pixel = 16;
	amm->intern->color_black.red = 0;
	amm->intern->color_black.green = 0;
	amm->intern->color_black.blue = 0;	
	amm->intern->color_white.pixel = 16;
	amm->intern->color_white.red = (guint16) - 1;
	amm->intern->color_white.green = (guint16) - 1;
	amm->intern->color_white.blue = (guint16) - 1;
	
	// Init Window
	gtk_widget_set_usize (GTK_WIDGET(amm), 200, 70);
	gtk_window_set_title (GTK_WINDOW (amm), _("Messages"));
	gtk_window_set_wmclass (GTK_WINDOW (amm), "messages", "Anjuta");
	
	// Create Widgets
	amm->intern->box = gtk_hbox_new(false, 0);
	amm->intern->eventbox = gtk_event_box_new();
	amm->intern->notebook = gtk_notebook_new();
	amm->intern->dock = gnome_dock_new();
	amm->intern->toolbar = gtk_toolbar_new(GTK_ORIENTATION_VERTICAL, GTK_TOOLBAR_ICONS);
	amm->intern->dock_item = gnome_dock_item_new("dock", GNOME_DOCK_ITEM_BEH_EXCLUSIVE);
	gtk_widget_show(GTK_WIDGET(amm));
	gtk_widget_show(amm->intern->dock);
	gtk_widget_show(amm->intern->dock_item);
	gtk_widget_show(amm->intern->toolbar);
	gtk_widget_show(amm->intern->notebook);
	gtk_widget_show(amm->intern->box);
	gtk_widget_show(amm->intern->eventbox);
	
	// Hide Notebook tabs
	gtk_notebook_set_show_tabs(GTK_NOTEBOOK(amm->intern->notebook), false);
	
	// Create Dock/Toolbar
	gnome_dock_add_item(GNOME_DOCK(amm->intern->dock), GNOME_DOCK_ITEM(amm->intern->dock_item),
						GNOME_DOCK_LEFT, 1, 0, 0, 0);
	gtk_container_add(GTK_CONTAINER(amm->intern->dock_item), amm->intern->toolbar);
	gnome_dock_set_client_area (GNOME_DOCK (amm->intern->dock), amm->intern->box);
	gtk_toolbar_set_button_relief (GTK_TOOLBAR (amm->intern->toolbar), GTK_RELIEF_NONE);
	gtk_toolbar_set_space_style (GTK_TOOLBAR (amm->intern->toolbar), GTK_TOOLBAR_SPACE_LINE);
	
	// Add Widgets to UI
	gtk_box_pack_start(GTK_BOX(amm->intern->box), amm->intern->notebook, true, true ,0);
	gtk_container_add(GTK_CONTAINER(amm->intern->eventbox), amm->intern->dock);
	gtk_container_add(GTK_CONTAINER(amm), amm->intern->eventbox);
}

static void
anjuta_message_manager_destroy (GtkObject * obj)
{
	AnjutaMessageManager* amm = ANJUTA_MESSAGE_MANAGER(obj);
	typedef vector<AnjutaMessageWindow*>::iterator I;
	for (I cur_win = amm->intern->msg_windows.begin(); cur_win != amm->intern->msg_windows.end(); cur_win++)
	{
		delete *cur_win;
	}
	delete amm->intern;
	GTK_OBJECT_CLASS (parent_class)->destroy (obj);
}

// Creates a new Notebook page for message of a new type if it not already exists
gboolean anjuta_message_manager_add_type(AnjutaMessageManager* amm, gchar* type_name, GdkPixmap* pixmap)
{
	string type = type_name;
	typedef vector<AnjutaMessageWindow*>::const_iterator CI;
	for (CI cur_win = amm->intern->msg_windows.begin();
		cur_win != amm->intern->msg_windows.end();
		cur_win++)
	{
		if ((*cur_win)->get_type() == type)
			return false;
	}
	AnjutaMessageWindow* window = new AnjutaMessageWindow(amm, type, pixmap);
	if (amm->intern->last_page == 0)
		amm->intern->msg_windows.back()->activate();
	
	return true; 
}

// Adds a string to a message_window and shows it when a newline appears
gboolean anjuta_message_manager_append(AnjutaMessageManager* amm, gchar* msg_string, gchar* type_name)
{
	string type = type_name;
	string msg = msg_string;
	vector<AnjutaMessageWindow*>::iterator cur_win;	
	
	if (!amm->intern->msg_windows.empty())
	{
		bool found = false;
		for (cur_win = amm->intern->msg_windows.begin();
			cur_win != amm->intern->msg_windows.end();
			cur_win++)
		{
			if ((*cur_win)->get_type() == type)
			{
				found = true;
				break;
			}
		}
		if (!found)
			return false;
	}
	else
	{
		return false;
	}
	// TODO: Truncate messages
	for (string::iterator c = msg.begin(); c != msg.end(); c++)
	{
		if (*c == '\n')
		{
			(*cur_win)->append_buffer();
		}
		else
		{
			(*cur_win)->add_to_buffer(*c);
		}
	}
	return true;
}

// Sent a "message_click" event as if the next message was clicked
void anjuta_message_manager_next(AnjutaMessageManager* amm)
{
	AnjutaMessageWindow* win = amm->intern->cur_msg_win;
	g_return_if_fail(win != NULL);
	if (win->get_cur_line() < win->get_messages().size())
	{
		gtk_signal_emit(GTK_OBJECT(amm), anjuta_message_manager_signals[MESSAGE_CLICKED],
							win->get_type().c_str(), win->get_messages()[win->get_cur_line() + 1].c_str());
		gtk_clist_select_row(GTK_CLIST(win->msg_list), win->get_cur_line() + 1, 0);
	}
}

// Sent a "message_click" event as if the previous message was clicked
void anjuta_message_manager_previous(AnjutaMessageManager* amm)
{
	AnjutaMessageWindow* win = amm->intern->cur_msg_win;
	g_return_if_fail(win != NULL);
	if (win->get_cur_line() < 0)
	{
		gtk_signal_emit(GTK_OBJECT(amm), anjuta_message_manager_signals[MESSAGE_CLICKED],
							win->get_type().c_str(), win->get_messages()[win->get_cur_line() - 1].c_str());
		gtk_clist_select_row(GTK_CLIST(win->msg_list), win->get_cur_line() - 1, 0);
	}
}

// Add a button to the dock to allow users to change the message type shown
void add_button_to_dock(AnjutaMessageManager* amm, AnjutaMessageWindow* msg_win, GdkPixmap* pixmap)
{
	string label = msg_win->get_type();
	
	msg_win->frame = gtk_frame_new (NULL);
	gtk_toolbar_append_widget (GTK_TOOLBAR (amm->intern->toolbar),
				   msg_win->frame, label.c_str(), NULL);
	gtk_widget_set_usize (msg_win->frame, 80, -1);

	msg_win->button = gtk_toggle_button_new ();
	gtk_widget_show (msg_win->button);
	gtk_button_set_relief (GTK_BUTTON (msg_win->button), GTK_RELIEF_NONE);
	gtk_container_add (GTK_CONTAINER (msg_win->frame), GTK_WIDGET(msg_win->button));
	
	GtkWidget* pix_lab = gtk_hbox_new(false, false);
	GtkWidget* button_label = gtk_label_new(label.c_str());
	//GtkWidget* button_pixmap = gtk_pixmap_new(pixmap, NULL);
	gtk_widget_show(button_label);
	//gtk_widget_show(button_pixmap);
	gtk_widget_ref(button_label);
	//gtk_widget_ref(button_pixmap);
	//gtk_box_pack_start(GTK_BOX(pix_lab), button_pixmap, false, false, 1);
	gtk_container_add(GTK_CONTAINER(pix_lab), button_label);
	
	gtk_widget_show (pix_lab);
	gtk_widget_show (msg_win->frame);
	
	gtk_signal_connect(GTK_OBJECT(msg_win->button), "clicked", GTK_SIGNAL_FUNC(on_dock_button_clicked),
						amm);
	gtk_container_add (GTK_CONTAINER (msg_win->button), pix_lab);
}
	
// Removes a button from the dock (untested)
void remove_button_from_dock(AnjutaMessageManager* amm, AnjutaMessageWindow* msg_win)
{
	gtk_container_remove(GTK_CONTAINER(amm->intern->toolbar), msg_win->frame);
}

// Called when a button is clicked. Changes the view to the message type
void on_dock_button_clicked(GtkToggleButton* button, gpointer data)
{
	g_return_if_fail(data != NULL);
	
	AnjutaMessageManager* amm = ANJUTA_MESSAGE_MANAGER(data);
	
	GList* button_children = gtk_container_children(GTK_CONTAINER(button));
	GtkWidget* box = GTK_WIDGET(g_list_first(button_children)->data);
	GList* box_children = gtk_container_children(GTK_CONTAINER(box));
	GtkWidget* label = GTK_WIDGET(g_list_first(box_children)->data);
	
	char* type_name;
	gtk_label_get(GTK_LABEL(label), &type_name);
	
	for (vector<AnjutaMessageWindow*>::iterator cur_win = amm->intern->msg_windows.begin(); 
				cur_win != amm->intern->msg_windows.end(); cur_win++)
	{
		if ((*cur_win)->get_type() != string(type_name))
		{
			(*cur_win)->deactivate();
		}
		else
		{
			(*cur_win)->activate();
		}
	}
	return;
}

// Called when a message in the list is double clicked. Emits a the "message_clicked" signal
void on_msg_double_clicked(GtkCList* list, gint row, gint column, GdkEvent* event, 
							gpointer data)
{
	if (event == NULL)
		return;
	if (event->type == GDK_BUTTON_PRESS && ((GdkEventButton *)event)->button == 1)
	{
		AnjutaMessageWindow* win = reinterpret_cast<AnjutaMessageWindow*>(data);
		win->set_cur_line(row);
		return;
	}
	if (event->type != GDK_2BUTTON_PRESS)
		return;
	if (((GdkEventButton *) event)->button != 1)
		return;
	
	AnjutaMessageWindow* win = reinterpret_cast<AnjutaMessageWindow*>(data);
	gtk_signal_emit(GTK_OBJECT(win->get_parent()), anjuta_message_manager_signals[MESSAGE_CLICKED],
							win->get_type().c_str(), win->get_messages()[row].c_str());
	return;
}


int main()
{
	gtk_init(0, 0);
	
	AnjutaMessageManager* amm = ANJUTA_MESSAGE_MANAGER(anjuta_message_manager_new());
	anjuta_message_manager_add_type(amm, "Test1", NULL);
	anjuta_message_manager_add_type(amm, "Test2", NULL);
	anjuta_message_manager_append(amm, "Yes\n", "Test1");
	anjuta_message_manager_append(amm, "No\n", "Test2");
	
	gtk_widget_show(GTK_WIDGET(amm));
	
	gtk_main();
	return 0;
};
