/*
    anjuta-message-manager.h
    Copyright (C) 2000, 2001  Kh. Naba Kumar Singh, Johannes Schmid

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <gtk/gtk.h>
#include <gnome.h>

#ifndef _ANJUTA_MESSAGE_MANAGER
#define _ANJUTA_MESSAGE_MANAGER

#ifdef __cplusplus
extern "C" 
{
#endif

typedef struct _AnjutaMessageManager AnjutaMessageManager;
typedef struct _AnjutaMessageManagerClass AnjutaMessageManagerClass;
	
#define ANJUTA_MESSAGE_MANAGER_TYPE        (anjuta_message_manager_get_type ())
#define ANJUTA_MESSAGE_MANAGER(o)          (GTK_CHECK_CAST ((o), ANJUTA_MESSAGE_MANAGER_TYPE, AnjutaMessageManager))
#define ANJUTA_MESSAGE_MANAGER_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), ANJUTA_MESSAGE_MANAGER_TYPE, AnjutaMessageManagerClass))
#define ANJUTA_IS_MESSAGE_MANAGER(o)       (GTK_CHECK_TYPE ((o), ANJUTA_MESSAGE_MANAGER_TYPE))
#define ANJUTA_IS_MESSAGE_MANAGER_CLASS(k) (GTK_CHECK_CLASS_TYPE ((k), ANJUTA_MESSAGE_MANAGER_TYPE))

struct AnjutaMessageManagerPrivate;

struct _AnjutaMessageManager
{
	GtkWindow parent;
	AnjutaMessageManagerPrivate* intern;
};

struct _AnjutaMessageManagerClass
{
	GtkWindowClass klass;
	
	// Signals
	void (*message_clicked) (AnjutaMessageManager *amm, gchar* type, gchar* message);
};

// Public functions
GtkWidget* anjuta_message_manager_new();
GtkType anjuta_message_manager_get_type();

gboolean anjuta_message_manager_add_type(AnjutaMessageManager* amm, gchar* type_name, GdkPixmap* pixmap);
gboolean anjuta_message_manager_append(AnjutaMessageManager* amm, gchar* msg_string, gchar* type_name);

void anjuta_message_manager_next(AnjutaMessageManager* amm);
void anjuta_message_manager_previous(AnjutaMessageManager* amm);


// Intern functions
static void anjuta_message_manager_destroy (GtkObject * object);
static void anjuta_message_manager_class_init (AnjutaMessageManagerClass *klass);
static void anjuta_message_manager_init (GtkObject * obj);

// Callbacks
void on_dock_button_clicked(GtkToggleButton* button, gpointer data);
void on_msg_double_clicked(GtkCList* list, gint row, gint column, GdkEvent* event, gpointer msg_win);

#ifdef __cplusplus
};
#endif

#endif