/*
 * anjuta-component.c
 * Copyright (C) 2000  Kh. Naba Kumar Singh
 * 
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free 
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc., 59 
 * Temple Place, Suite 330, Boston, MA  02111-1307  USA 
 */
#include <config.h>
#include <gnome.h>
#include <bonobo.h>

#include "anjuta-component.h"

struct _AnjutaComponentPriv
{
	AnjutaComponentInitFunc init_fn;
	AnjutaComponentDestroyFunc destroy_fn;
};

enum
{
	ARG_NONE,
	ARG_SHELL,
	ARG_LAST
};

static void anjuta_component_destroy (GtkObject * object);
static void anjuta_component_class_init (AnjutaComponentClass * class);
static void anjuta_component_init (GtkObject * object);
static void set_prop (BonoboPropertyBag * bag,
		      const BonoboArg * arg,
		      guint arg_id,
		      CORBA_Environment * ev, gpointer user_data);
static void get_prop (BonoboPropertyBag * bag,
		      BonoboArg * arg,
		      guint arg_id,
		      CORBA_Environment * ev, gpointer user_data);

static GtkObjectClass *parent_class;


/* Public */
AnjutaComponent *
anjuta_component_new (AnjutaComponentInitFunc init_fn,
		      AnjutaComponentDestroyFunc destroy_fn, gpointer data)
{
	AnjutaComponent *component;

	g_return_val_if_fail (init_fn != NULL, NULL);

	component = gtk_type_new (anjuta_component_get_type ());
	gtk_object_ref (GTK_OBJECT (component));
	gtk_object_sink (GTK_OBJECT (component));

	component->props = bonobo_property_bag_new (get_prop,
						    set_prop, component);
	bonobo_property_bag_add (component->props, "anjuta-shell", ARG_SHELL,
				 BONOBO_ARG_STRING, NULL,
				 "The ID of the shell that owns the component.",
				 BONOBO_PROPERTY_WRITEABLE);

	component->priv->init_fn = init_fn;
	component->priv->destroy_fn = destroy_fn;
	component->data = data;

	return component;
}

GtkType
anjuta_component_get_type (void)
{
	static GtkType type = 0;

	if (!type)
	{
		GtkTypeInfo info = {
			"AnjutaComponent",
			sizeof (AnjutaComponent),
			sizeof (AnjutaComponentClass),
			(GtkClassInitFunc) anjuta_component_class_init,
			(GtkObjectInitFunc) anjuta_component_init,
			NULL,
			NULL,
			(GtkClassInitFunc) NULL
		};

		type = gtk_type_unique (gtk_object_get_type (), &info);
	}

	return type;
}

/* Private */
static void
anjuta_component_destroy (GtkObject * object)
{
	CORBA_Environment ev;
	AnjutaComponent *component = ANJUTA_COMPONENT (object);

	g_free (component->priv);

	CORBA_exception_init (&ev);
	Bonobo_Unknown_unref (component->shell, &ev);
	Bonobo_Unknown_unref (component->ui_container, &ev);
	CORBA_exception_free (&ev);

}

static void
anjuta_component_class_init (AnjutaComponentClass * class)
{
	GtkObjectClass *object_class = (GtkObjectClass *) class;
	parent_class = gtk_type_class (gtk_object_get_type ());

	object_class->destroy = anjuta_component_destroy;
}

static void
anjuta_component_init (GtkObject * object)
{
	AnjutaComponent *component = ANJUTA_COMPONENT (object);
	component->priv = g_new0 (AnjutaComponentPriv, 1);
}

static void
set_prop (BonoboPropertyBag * bag,
	  const BonoboArg * arg,
	  guint arg_id, CORBA_Environment * ev, gpointer user_data)
{
	AnjutaComponent *component = ANJUTA_COMPONENT (user_data);

	switch (arg_id)
	{
	case ARG_SHELL:
	{
		char *val = BONOBO_ARG_GET_STRING (arg);

		if (val[0])
		{
			char *val = BONOBO_ARG_GET_STRING (arg);

			char *moniker_string = g_strdup_printf ("anjuta:%s",
								val);
			component->shell = bonobo_get_object (moniker_string,
							      "IDL:GNOME/Anjuta/Shell:1.0",
							      ev);
			component->shell_id = g_strdup (val);
			component->ui_container =
				GNOME_Anjuta_Shell_getUIContainer
				(component->shell, ev);

			component->priv->init_fn (component, component->data);
		}
		else
		{
			component->priv->destroy_fn (component,
						     component->data);

			g_free (component->shell_id);
			bonobo_object_release_unref (component->shell, ev);
			bonobo_object_release_unref (component->ui_container,
						     ev);
		}
		break;
	}
	default:
		g_assert_not_reached ();
	}
}

static void
get_prop (BonoboPropertyBag * bag,
	  BonoboArg * arg,
	  guint arg_id, CORBA_Environment * ev, gpointer user_data)
{

}
