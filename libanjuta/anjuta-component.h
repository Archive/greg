/*
 * anjuta-component.h
 * Copyright (C) 2000  Kh. Naba Kumar Singh
 * 
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free 
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc., 59 
 * Temple Place, Suite 330, Boston, MA  02111-1307  USA 
 */
#ifndef _ANJUTA_COMPONENT_H
#define _ANJUTA_COMPONENT_H

#include <bonobo.h>
#include <libanjuta/libanjuta.h>

BEGIN_GNOME_DECLS

typedef struct _AnjutaComponent AnjutaComponent;
typedef struct _AnjutaComponentClass AnjutaComponentClass;
typedef struct _AnjutaComponentPriv AnjutaComponentPriv;

#define ANJUTA_COMPONENT_TYPE        (anjuta_component_get_type ())
#define ANJUTA_COMPONENT(o)          (GTK_CHECK_CAST ((o), \
										ANJUTA_COMPONENT_TYPE, AnjutaComponent))
#define ANJUTA_COMPONENT_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), \
										ANJUTA_COMPONENT_TYPE, AnjutaComponentClass))
#define ANJUTA_IS_COMPONENT(o)       (GTK_CHECK_TYPE ((o), ANJUTA_COMPONENT_TYPE))
#define ANJUTA_IS_COMPONENT_CLASS(k) (GTK_CHECK_CLASS_TYPE ((k), ANJUTA_COMPONENT_TYPE))

typedef gboolean (*AnjutaComponentInitFunc) (AnjutaComponent * component,
					     gpointer closure);
typedef void (*AnjutaComponentDestroyFunc) (AnjutaComponent * component,
					    gpointer closure);

struct _AnjutaComponent
{
	GtkObject object;

	GNOME_Anjuta_Shell shell;
	gchar *shell_id;
	Bonobo_UIContainer ui_container;

	gpointer data;
	BonoboPropertyBag *props;

	AnjutaComponentPriv *priv;
};

struct _AnjutaComponentClass
{
	BonoboObjectClass parent_class;
};

GtkType anjuta_component_get_type (void);

AnjutaComponent *anjuta_component_new (AnjutaComponentInitFunc init_fn,
				       AnjutaComponentDestroyFunc cleanup_fn,
				       gpointer data);

END_GNOME_DECLS
#endif
